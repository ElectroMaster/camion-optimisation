import numpy as np
import matplotlib.pyplot as plt
import matplotlib.colors as mplcolors
from mpl_toolkits.mplot3d.art3d import Poly3DCollection, Line3DCollection
from matplotlib.patches import Circle, PathPatch
import mpl_toolkits.mplot3d.art3d as art3d
import logging
logging.basicConfig()
log = logging.getLogger("forme")
log.setLevel(logging.DEBUG)

# because we have rotation here, we cannot judge equal directly use ==
# if gap<ALMOST_EQUAL_GAP, they are equal.
ALMOST_EQUAL_GAP = 1e-3

def random_mpl_color_rgb():
    return (np.random.uniform(), np.random.uniform(), np.random.uniform())


def random_mpl_color_rgba(alpha=0.25):
    rgba_tup = random_mpl_color_rgb() + (alpha,)
    return rgba_tup


def random_mpl_color_rgb_rgba(alpha=0.25):
    """
    generate a pair of rgb and rgba color, both of them have same R G B value.
    """
    rgb_tup = random_mpl_color_rgb()
    rgba_tup = rgb_tup + (alpha,)
    return rgb_tup, rgba_tup

def getLWHByBoiteVertices(vertices):
    """

    :param vertices: vertices coordinates array
    :return: (center_x, center_y, center_z), L, W, H
    """
    min_x = np.min(vertices[:, 0])
    min_y = np.min(vertices[:, 1])
    min_z = np.min(vertices[:, 2])
    max_x = np.max(vertices[:, 0])
    max_y = np.max(vertices[:, 1])
    max_z = np.max(vertices[:, 2])
    center_x = (min_x + max_x)/2.0
    center_y = (min_y + max_y)/2.0
    center_z = (min_z + max_z)/2.0
    center = (center_x, center_y, center_z)
    L = max_x - min_x
    W = max_y - min_y
    H = max_z - min_z
    return center, L, W, H

class Boite:
    def __init__(self, point_ref, longeur, largeur, hauteur):
        assert isinstance(point_ref, tuple)
        assert len(point_ref) == 3
        self.x = point_ref[0]
        self.y = point_ref[1]
        self.z = point_ref[2]
        self.longeur = longeur
        self.largeur = largeur
        self.hauteur = hauteur

        # fristly we think the reference point is at the origin
        self._temp_vertices = np.array([[-longeur / 2.0, -largeur / 2.0, -hauteur / 2.0],
                                       [longeur / 2.0, -largeur / 2.0, -hauteur / 2.0],
                                       [longeur / 2.0, largeur / 2.0, -hauteur / 2.0],
                                       [-longeur / 2.0, largeur / 2.0, -hauteur / 2.0],
                                       [-longeur / 2.0, -largeur / 2.0, hauteur / 2.0],
                                       [longeur / 2.0, -largeur / 2.0, hauteur / 2.0],
                                       [longeur / 2.0, largeur / 2.0, hauteur / 2.0],
                                       [-longeur / 2.0, largeur / 2.0, hauteur / 2.0]])

    def translation(self, length_x, length_y, length_z):
        self.x = self.x + length_x
        self.y = self.y + length_y
        self.z = self.z + length_z

    def rotation3d(self, alpha, beta, gamma):
        """
        :param alpha: perform a rotation about x axis
        :param beta: perform a rotation about y axis
        :param gamma: perform a rotation about z axis
        :return:
        """
        rx = np.array([[1, 0, 0],
                       [0, np.cos(alpha), -np.sin(alpha)],
                       [0, np.sin(alpha), np.cos(alpha)]])
        ry = np.array([[np.cos(beta), 0, np.sin(beta)],
                       [0, 1, 0],
                       [-np.sin(beta), 0, np.cos(beta)]])
        rz = np.array([[np.cos(gamma), -np.sin(gamma), 0],
                       [np.sin(gamma), np.cos(gamma), 0],
                       [0, 0, 1]])
        self._temp_vertices = np.transpose(rz.dot(ry.dot(rx.dot(np.transpose(self._temp_vertices)))))
        self._update_faces()

    def _update_vertices(self):
        # after some operation we need to translate object to real location.
        self.vertices = np.ones((self._temp_vertices.shape[0], 1)) * np.array(
            [self.x, self.y, self.z]) + self._temp_vertices

    def _update_faces(self):
        # obtain all surfaces.
        self._update_vertices()
        self.faces = [[self.vertices[0], self.vertices[1], self.vertices[2], self.vertices[3]],
                      [self.vertices[4], self.vertices[5], self.vertices[6], self.vertices[7]],
                      [self.vertices[0], self.vertices[1], self.vertices[5], self.vertices[4]],
                      [self.vertices[2], self.vertices[3], self.vertices[7], self.vertices[6]],
                      [self.vertices[1], self.vertices[2], self.vertices[6], self.vertices[5]],
                      [self.vertices[4], self.vertices[7], self.vertices[3], self.vertices[0]]]

    def getFaces(self):
        self._update_faces()
        return self.faces

    def getVertices(self):
        self._update_vertices()
        return self.vertices

    def getSurfaceTextPos(self):
        """
        just get position for text
        :return:
        """
        vertices = self.getVertices()
        pos_list = []
        pos = vertices[0]+vertices[1]+vertices[2]+vertices[3]
        pos = pos/4.0
        pos_list.append(pos)
        pos = vertices[0]+vertices[1]+vertices[5]+vertices[4]
        pos = pos/4.0
        pos_list.append(pos)
        pos = vertices[1]+vertices[2]+vertices[6]+vertices[5]
        pos = pos/4.0
        pos_list.append(pos)
        pos = vertices[2]+vertices[3]+vertices[7]+vertices[6]
        pos = pos/4.0
        pos_list.append(pos)
        pos = vertices[3]+vertices[7]+vertices[4]+vertices[0]
        pos = pos/4.0
        pos_list.append(pos)
        pos = vertices[4]+vertices[5]+vertices[6]+vertices[7]
        pos = pos/4.0
        pos_list.append(pos)
        pos_list = np.array(pos_list)
        return pos_list

    def getBottomSurfaceIndex(self):
        """
        get current bottom surface index
        :return:
        """
        pos_list = self.getSurfaceTextPos()
        return np.argmin(pos_list[:,2])
    def getTopSurfaceIndex(self):
        pos_list = self.getSurfaceTextPos()
        return np.argmax(pos_list[:,2])

    def getBottomSurfaceHeight(self):
        bottomSurfaceIdx = self.getBottomSurfaceIndex()
        pos_list = self.getSurfaceTextPos()
        return pos_list[bottomSurfaceIdx, 2]
    def getTopSurfaceHeight(self):
        topSurfaceIdx = self.getTopSurfaceIndex()
        pos_list = self.getSurfaceTextPos()
        return pos_list[topSurfaceIdx, 2]

    def getBottomVertices(self):
        num_face = 1
        vertices_list = []
        vertices = self.getVertices()
        lowest_z = np.min(vertices[:, 2])
        for vertice in vertices:
            if abs(vertice[2] - lowest_z) < ALMOST_EQUAL_GAP:
                vertices_list.append(list(vertice))
        face_dict = {}
        face_dict["0"] = vertices_list
        return num_face, face_dict

    def getTopVertices(self):
        num_face = 1
        vertices_list = []
        vertices = self.getVertices()
        highest_z = np.max(vertices[:, 2])
        for vertice in vertices:
            if abs(vertice[2] - highest_z) < ALMOST_EQUAL_GAP:
                vertices_list.append(list(vertice))
        face_dict = {}
        face_dict["0"] = vertices_list
        return num_face, face_dict

    def getCurrentLWH(self):
        vertices = self.getVertices()
        center, L, W, H = getLWHByBoiteVertices(vertices)
        return center, L, W, H


    def drawSurfaceIndexOnAxes(self, ax, **kwargs):
        pos_list = self.getSurfaceTextPos()
        for i in range(pos_list.shape[0]):
            ax.text(*pos_list[i], "{}".format(i), "x", **kwargs)
        ax.scatter3D(pos_list[:, 0], pos_list[:, 1], pos_list[:, 2], **kwargs)

    def drawVerticeIndexOnAxes(self, ax, **kwargs):
        vertices = self.getVertices()
        for i in range(vertices.shape[0]):
            ax.text(*vertices[i], "{}".format(i), "x", **kwargs)

    def getPoly3d(self, facecolors=None, edgecolors=None):
        if facecolors == None and edgecolors == None:
            edgecolors, facecolors = random_mpl_color_rgb_rgba()
        elif facecolors == None and edgecolors != None:
            if isinstance(edgecolors, str):
                edgecolors = mplcolors.to_rgb(edgecolors)
            assert isinstance(edgecolors, tuple)
            facecolors = (edgecolors[0], edgecolors[1], edgecolors[2]) + (0.25,)
        elif edgecolors == None and facecolors != None:
            if isinstance(facecolors, str):
                facecolors = mplcolors.to_rgb(facecolors)
            assert isinstance(facecolors, tuple)
            edgecolors = (facecolors[0], facecolors[1], facecolors[2])
        self._update_faces()
        poly3d = Poly3DCollection(self.faces,
                                  facecolors=facecolors, linewidths=1, edgecolors=edgecolors)
        return poly3d


class Pyramid:
    def __init__(self, point_ref, longeur, largeur, point_top):
        assert isinstance(point_ref, tuple)
        assert isinstance(point_top, tuple)
        assert len(point_ref) == 3
        self.x = point_ref[0]
        self.y = point_ref[1]
        self.z = point_ref[2]
        self.largeur = largeur 
        self.longeur = longeur 
        self.top_x = point_top[0]
        self.top_y = point_top[1]
        self.top_z = point_top[2]

        # suppose object is situated at origin of coordinate firstly.
        self._temp_vertices = np.array([[self.top_x-self.x, self.top_y-self.y, self.top_z-self.z],
                                 [0, 0, 0],
                                 [longeur, 0, 0],
                                 [longeur, largeur, 0],
                                 [0, largeur, 0]])

    def translation(self, length_x, length_y, length_z):
        self.x = self.x + length_x
        self.y = self.y + length_y
        self.z = self.z + length_z

    def rotation3d(self, alpha, beta, gamma):
        """
        :param alpha: perform a rotation about x axis
        :param beta: perform a rotation about y axis
        :param gamma: perform a rotation about z axis
        :return:
        """
        rx = np.array([[1, 0, 0],
                       [0, np.cos(alpha), -np.sin(alpha)],
                       [0, np.sin(alpha), np.cos(alpha)]])
        ry = np.array([[np.cos(beta), 0, np.sin(beta)],
                       [0, 1, 0],
                       [-np.sin(beta), 0, np.cos(beta)]])
        rz = np.array([[np.cos(gamma), -np.sin(gamma), 0],
                       [np.sin(gamma), np.cos(gamma), 0],
                       [0, 0, 1]])
        self._temp_vertices = np.transpose(rz.dot(ry.dot(rx.dot(np.transpose(self._temp_vertices)))))
        self._update_faces()
    def _update_vertices(self):
        # after some operation we need to translate object to real location.
        self.vertices = np.ones((self._temp_vertices.shape[0], 1))*np.array([self.x, self.y, self.z]) + self._temp_vertices
    def _update_faces(self):
        # obtain all surfaces.
        self._update_vertices()
        self.faces = [[self.vertices[0], self.vertices[1], self.vertices[2]],
                      [self.vertices[0], self.vertices[2], self.vertices[3]],
                      [self.vertices[0], self.vertices[3], self.vertices[4]],
                      [self.vertices[0], self.vertices[4], self.vertices[1]],
                      [self.vertices[1], self.vertices[2], self.vertices[3], self.vertices[4]]]
    def getFaces(self):
        self._update_faces()
        return self.faces

    def getVertices(self):
        self._update_vertices()
        return self.vertices

    def getPoly3d(self, facecolors=None, edgecolors=None):
        if facecolors==None and edgecolors==None:
            edgecolors, facecolors = random_mpl_color_rgb_rgba()
        elif facecolors == None:
            if isinstance(edgecolors, str):
                edgecolors = mplcolors.to_rgb(edgecolors)
            assert isinstance(edgecolors, tuple)
            facecolors=(edgecolors[0], edgecolors[1], edgecolors[2])+(0.25,)
        elif edgecolors == None:
            if isinstance(facecolors, str):
                facecolors = mplcolors.to_rgb(facecolors)
            assert isinstance(facecolors, tuple)
            edgecolors=(facecolors[0], facecolors[1], facecolors[2])
        self._update_faces()
        poly3d = Poly3DCollection(self.faces,
                         facecolors=facecolors, linewidths=1, edgecolors=edgecolors)
        return poly3d


class LForme:
    def __init__(self, point_ref, L_short_outer_len, L_width, L_longer_outer_len, L_short_inner_len, L_longer_inner_len):
        assert isinstance(point_ref, tuple)
        assert len(point_ref) == 3
        self.x = point_ref[0]
        self.y = point_ref[1]
        self.z = point_ref[2]
        self.L_short_outer_len = L_short_outer_len
        self.L_width = L_width
        self.L_longer_outer_len = L_longer_outer_len
        self.L_short_inner_len = L_short_inner_len
        self.L_longer_inner_len = L_longer_inner_len 

        # fristly we think the reference point is at the origin
        # L forme object has 12 vertices 
        self._temp_vertices = np.array([
            [-L_short_outer_len / 2.0, -L_width / 2.0, -L_longer_outer_len / 2.0], # 0
            [L_short_outer_len / 2.0, -L_width / 2.0, -L_longer_outer_len / 2.0], # 1
            [L_short_outer_len / 2.0, L_width / 2.0, -L_longer_outer_len / 2.0], # 2
            [-L_short_outer_len / 2.0, L_width / 2.0, -L_longer_outer_len / 2.0], # 3
            [-L_short_outer_len / 2.0, -L_width / 2.0, L_longer_outer_len / 2.0], # 4
            [L_short_outer_len / 2.0 - L_short_inner_len, -L_width / 2.0, L_longer_outer_len / 2.0], # 5
            [L_short_outer_len / 2.0 - L_short_inner_len, L_width / 2.0, L_longer_outer_len / 2.0], # 6
            [-L_short_outer_len / 2.0, L_width / 2.0, L_longer_outer_len / 2.0],  # 7 
            [L_short_outer_len / 2.0 - L_short_inner_len, -L_width / 2.0, L_longer_outer_len / 2.0 - L_longer_inner_len], # 8 
            [L_short_outer_len / 2.0, -L_width / 2.0, L_longer_outer_len / 2.0 - L_longer_inner_len], # 9 
            [L_short_outer_len / 2.0, L_width / 2.0, L_longer_outer_len / 2.0 - L_longer_inner_len], # 10
            [L_short_outer_len / 2.0 - L_short_inner_len, L_width / 2.0, L_longer_outer_len / 2.0 - L_longer_inner_len], # 11
            [L_short_outer_len / 2.0 - L_short_inner_len, -L_width / 2.0, -L_longer_outer_len / 2.0], # 12 pseudo-vertice
            [-L_short_outer_len / 2.0, -L_width / 2.0, L_longer_outer_len / 2.0 - L_longer_inner_len], # 13 pseudo-vertice
            [L_short_outer_len / 2.0 - L_short_inner_len, L_width / 2.0, -L_longer_outer_len / 2.0], # 14 pseudo-vertice
            [-L_short_outer_len / 2.0, L_width / 2.0, L_longer_outer_len / 2.0 - L_longer_inner_len], # 15 pseudo-vertice
        ])

    def translation(self, length_x, length_y, length_z):
        self.x = self.x + length_x
        self.y = self.y + length_y
        self.z = self.z + length_z

    def rotation3d(self, alpha, beta, gamma):
        """
        :param alpha: perform a rotation about x axis
        :param beta: perform a rotation about y axis
        :param gamma: perform a rotation about z axis
        :return:
        """
        rx = np.array([[1, 0, 0],
                       [0, np.cos(alpha), -np.sin(alpha)],
                       [0, np.sin(alpha), np.cos(alpha)]])
        ry = np.array([[np.cos(beta), 0, np.sin(beta)],
                       [0, 1, 0],
                       [-np.sin(beta), 0, np.cos(beta)]])
        rz = np.array([[np.cos(gamma), -np.sin(gamma), 0],
                       [np.sin(gamma), np.cos(gamma), 0],
                       [0, 0, 1]])
        self._temp_vertices = np.transpose(rz.dot(ry.dot(rx.dot(np.transpose(self._temp_vertices)))))
        self._update_faces()

    def _update_vertices(self):
        # after some operation we need to translate object to real location.
        self.vertices = np.ones((self._temp_vertices.shape[0], 1)) * np.array(
            [self.x, self.y, self.z]) + self._temp_vertices

    def _update_faces(self):
        # obtain all surfaces.
        self._update_vertices()
        self.faces = [[self.vertices[0], self.vertices[1], self.vertices[2], self.vertices[3]],
                      [self.vertices[4], self.vertices[5], self.vertices[6], self.vertices[7]],
                      [self.vertices[4], self.vertices[0], self.vertices[3], self.vertices[7]],
                      [self.vertices[1], self.vertices[2], self.vertices[10],self.vertices[9]],
                      [self.vertices[8], self.vertices[9], self.vertices[10], self.vertices[11]],
                      [self.vertices[8], self.vertices[11], self.vertices[6], self.vertices[5]],
                      [self.vertices[0], self.vertices[1], self.vertices[9], self.vertices[8], self.vertices[5], self.vertices[4]],
                      [self.vertices[3], self.vertices[2], self.vertices[10], self.vertices[11], self.vertices[6], self.vertices[7]]]

    def getFaces(self):
        self._update_faces()
        return self.faces

    def getVertices(self):
        self._update_vertices()
        return self.vertices

    def getSurfaceTextPos(self):
        """
        just get position for text
        :return:
        """
        vertices = self.getVertices()
        pos_list = []
        pos = vertices[0]+vertices[1]+vertices[2]+vertices[3]
        pos = pos/4.0
        pos_list.append(pos)
        pos = vertices[0]+vertices[8]
        pos = pos/2.0
        pos_list.append(pos)
        pos = vertices[1]+vertices[2]+vertices[10]+vertices[9]
        pos = pos/4.0
        pos_list.append(pos)
        pos = vertices[3]+vertices[11]
        pos = pos/2.0
        pos_list.append(pos)
        pos = vertices[3]+vertices[0]+vertices[4]+vertices[7]
        pos = pos/4.0
        pos_list.append(pos)
        pos = vertices[4]+vertices[5]+vertices[6]+vertices[7]
        pos = pos/4.0
        pos_list.append(pos)
        pos = vertices[8]+vertices[9]+vertices[10]+vertices[11]
        pos = pos/4.0
        pos_list.append(pos)
        pos = vertices[8]+vertices[11]+vertices[6]+vertices[5]
        pos = pos/4.0
        pos_list.append(pos)
        pos_list = np.array(pos_list)
        return pos_list

    def getBottomSurfaceIndex(self):
        """
        get current bottom surface index
        :return:
        """
        pos_list = self.getSurfaceTextPos()
        return np.argmin(pos_list[:,2])
    def getTopSurfaceIndex(self):
        pos_list = self.getSurfaceTextPos()
        return np.argmax(pos_list[:,2])

    def getBottomSurfaceHeight(self):
        bottomSurfaceIdx = self.getBottomSurfaceIndex()
        pos_list = self.getSurfaceTextPos()
        return pos_list[bottomSurfaceIdx, 2]
    def getTopSurfaceHeight(self):
        topSurfaceIdx = self.getTopSurfaceIndex()
        pos_list = self.getSurfaceTextPos()
        return pos_list[topSurfaceIdx, 2]

    def getBottomVertices(self):
        vertices = self.getVertices()
        face_dict = {}
        if self.getBottomSurfaceIndex() == 0:
            num_face = 1
            vertices_list = [list(vertices[0]), list(vertices[1]), list(vertices[2]), list(vertices[3])]
            face_dict["0"] = vertices_list
            return num_face, face_dict
        elif self.getBottomSurfaceIndex() == 4:
            num_face = 1
            vertices_list = [list(vertices[0]), list(vertices[3]), list(vertices[7]), list(vertices[4])]
            face_dict["0"] = vertices_list
            return num_face, face_dict
        elif self.getBottomSurfaceIndex() == 1:
            num_face = 2
            vertices_list = [list(vertices[0]), list(vertices[12]), list(vertices[5]), list(vertices[4])]
            face_dict["0"] = vertices_list
            vertices_list = [list(vertices[12]), list(vertices[1]), list(vertices[9]), list(vertices[8])]
            face_dict["1"] = vertices_list
            return num_face, face_dict
        elif self.getBottomSurfaceIndex() == 3:
            num_face = 2
            vertices_list = [list(vertices[3]), list(vertices[14]), list(vertices[6]), list(vertices[7])]
            face_dict["0"] = vertices_list
            vertices_list = [list(vertices[14]), list(vertices[2]), list(vertices[10]), list(vertices[11])]
            face_dict["1"] = vertices_list
            return num_face, face_dict
        elif self.getBottomSurfaceIndex() == 5:
            num_face = 2
            vertices_list = [list(vertices[4]), list(vertices[5]), list(vertices[6]), list(vertices[7])]
            face_dict["0"] = vertices_list
            vertices_list = [list(vertices[8]), list(vertices[9]), list(vertices[10]), list(vertices[11])]
            face_dict["1"] = vertices_list
            return num_face, face_dict
        elif self.getBottomSurfaceIndex() == 2:
            num_face = 2
            vertices_list = [list(vertices[1]), list(vertices[2]), list(vertices[10]), list(vertices[9])]
            face_dict["0"] = vertices_list
            vertices_list = [list(vertices[8]), list(vertices[11]), list(vertices[6]), list(vertices[5])]
            face_dict["1"] = vertices_list
            return num_face, face_dict



    def getTopVertices(self):
        vertices = self.getVertices()
        face_dict = {}
        # log.info("BottomSurfaceIndex: {}".format(self.getBottomSurfaceIndex()))
        if self.getBottomSurfaceIndex() == 0:
            num_face = 2
            vertices_list = [list(vertices[4]), list(vertices[5]), list(vertices[6]), list(vertices[7])]
            face_dict["0"] = vertices_list
            vertices_list = [list(vertices[8]), list(vertices[9]), list(vertices[10]), list(vertices[11])]
            face_dict["1"] = vertices_list
            return num_face, face_dict
        elif self.getBottomSurfaceIndex() == 4:
            num_face = 2
            vertices_list = [list(vertices[1]), list(vertices[2]), list(vertices[10]), list(vertices[9])]
            face_dict["0"] = vertices_list
            vertices_list = [list(vertices[8]), list(vertices[11]), list(vertices[6]), list(vertices[5])]
            face_dict["1"] = vertices_list
            return num_face, face_dict
        elif self.getBottomSurfaceIndex() == 1:
            num_face = 2
            vertices_list = [list(vertices[3]), list(vertices[14]), list(vertices[6]), list(vertices[7])]
            face_dict["0"] = vertices_list
            vertices_list = [list(vertices[14]), list(vertices[2]), list(vertices[10]), list(vertices[11])]
            face_dict["1"] = vertices_list
            return num_face, face_dict
        elif self.getBottomSurfaceIndex() == 3:
            num_face = 2
            vertices_list = [list(vertices[0]), list(vertices[12]), list(vertices[5]), list(vertices[4])]
            face_dict["0"] = vertices_list
            vertices_list = [list(vertices[12]), list(vertices[1]), list(vertices[9]), list(vertices[8])]
            face_dict["1"] = vertices_list
            return num_face, face_dict
        elif self.getBottomSurfaceIndex() == 5:
            num_face = 1
            vertices_list = [list(vertices[0]), list(vertices[1]), list(vertices[2]), list(vertices[3])]
            face_dict["0"] = vertices_list
            return num_face, face_dict
        elif self.getBottomSurfaceIndex() == 2:
            num_face = 1
            vertices_list = [list(vertices[0]), list(vertices[3]), list(vertices[7]), list(vertices[4])]
            face_dict["0"] = vertices_list
            return num_face, face_dict

    def getCurrentLWH_VerticalBoite0(self):
        vertices = self.getVertices()
        if self.getBottomSurfaceIndex()==0 or self.getBottomSurfaceIndex()==5 or \
                self.getBottomSurfaceIndex() ==1 or self.getBottomSurfaceIndex()==3:
            vertices0 = np.array([vertices[0],
                                  vertices[12],
                                  vertices[14],
                                  vertices[3],
                                  vertices[4],
                                  vertices[5],
                                  vertices[6],
                                  vertices[7]])
            center, L, W, H = getLWHByBoiteVertices(vertices0)
        elif self.getBottomSurfaceIndex() == 2 or self.getBottomSurfaceIndex() == 4:
            vertices0 = np.array([vertices[0],
                                  vertices[1],
                                  vertices[2],
                                  vertices[3],
                                  vertices[9],
                                  vertices[10],
                                  vertices[15],
                                  vertices[13]])
            center, L, W, H = getLWHByBoiteVertices(vertices0)

        return center, L, W, H

    def getCurrentLWH_VerticalBoite1(self):
        vertices = self.getVertices()
        if self.getBottomSurfaceIndex() == 0 or self.getBottomSurfaceIndex() == 5 or \
                self.getBottomSurfaceIndex() == 1 or self.getBottomSurfaceIndex() == 3:
            vertices1 = np.array([vertices[12],
                                  vertices[1],
                                  vertices[2],
                                  vertices[14],
                                  vertices[8],
                                  vertices[9],
                                  vertices[10],
                                  vertices[11]])
            center, L, W, H = getLWHByBoiteVertices(vertices1)
        elif self.getBottomSurfaceIndex() == 2 or self.getBottomSurfaceIndex() == 4:
            vertices1 = np.array([vertices[13],
                                  vertices[8],
                                  vertices[11],
                                  vertices[15],
                                  vertices[4],
                                  vertices[5],
                                  vertices[6],
                                  vertices[7]])
            center, L, W, H = getLWHByBoiteVertices(vertices1)

        return center, L, W, H



    def drawSurfaceIndexOnAxes(self, ax, **kwargs):
        pos_list = self.getSurfaceTextPos()
        for i in range(pos_list.shape[0]):
            ax.text(*pos_list[i], "{}".format(i), "x", **kwargs)
        ax.scatter3D(pos_list[:, 0], pos_list[:, 1], pos_list[:, 2], **kwargs)

    def drawVerticeIndexOnAxes(self, ax, **kwargs):
        vertices = self.getVertices()
        for i in range(vertices.shape[0]):
            ax.text(*vertices[i], "{}".format(i), "x", **kwargs)



    def getPoly3d(self, facecolors=None, edgecolors=None):
        if facecolors == None and edgecolors == None:
            edgecolors, facecolors = random_mpl_color_rgb_rgba()
        elif facecolors == None and edgecolors != None:
            if isinstance(edgecolors, str):
                edgecolors = mplcolors.to_rgb(edgecolors)
            assert isinstance(edgecolors, tuple)
            facecolors = (edgecolors[0], edgecolors[1], edgecolors[2]) + (0.25,)
        elif edgecolors == None and facecolors != None:
            if isinstance(facecolors, str):
                facecolors = mplcolors.to_rgb(facecolors)
            assert isinstance(facecolors, tuple)
            edgecolors = (facecolors[0], facecolors[1], facecolors[2])
        self._update_faces()
        poly3d = Poly3DCollection(self.faces,
                                  facecolors=facecolors, linewidths=1, edgecolors=edgecolors)
        return poly3d



class Cylinder:
    def __init__(self, point_ref, diameter_x, diameter_y, length, N=20):
        assert isinstance(point_ref, tuple)
        assert len(point_ref) == 3
        self.x = point_ref[0]
        self.y = point_ref[1]
        self.z = point_ref[2]
        self.diameter_x = diameter_x
        self.diameter_y = diameter_y
        self.radius_x = diameter_x/2.0
        self.radius_y = diameter_y/2.0
        self.length = length

        Zs = np.linspace(-self.length/2.0,
                         self.length/2.0,
                         N)
        theta = np.linspace(0 , 2*np.pi, N)
        theta_grid, Z_grid = np.meshgrid(theta, Zs)
        X_grid = self.radius_x*np.cos(theta_grid)
        Y_grid = self.radius_y*np.sin(theta_grid)
        self.X_grid = X_grid
        self.Y_grid = Y_grid
        self.Z_grid = Z_grid
        self._temp_vertices = np.array([[0, 0, self.length/2.0],
                                        [0, 0, -self.length/2.0]])
        
    def translation(self, length_x, length_y, length_z):
        self.x += length_x
        self.y += length_y
        self.z += length_z

    def _update_vertices(self):
        # after some operation we need to translate object to real location.
        self.vertices = np.ones((self._temp_vertices.shape[0], 1)) * np.array(
            [self.x, self.y, self.z]) + self._temp_vertices
        
    def rotation3d(self, alpha, beta, gamma):
        """
        :param alpha: perform a rotation about x axis
        :param beta: perform a rotation about y axis
        :param gamma: perform a rotation about z axis
        :return:
        """
        temp = np.array([self.X_grid, self.Y_grid, self.Z_grid])
        temp = np.transpose(temp, (1,2,0))
        rx = np.array([[1, 0, 0],
                       [0, np.cos(alpha), -np.sin(alpha)],
                       [0, np.sin(alpha), np.cos(alpha)]])
        ry = np.array([[np.cos(beta), 0, np.sin(beta)],
                       [0, 1, 0],
                       [-np.sin(beta), 0, np.cos(beta)]])
        rz = np.array([[np.cos(gamma), -np.sin(gamma), 0],
                       [np.sin(gamma), np.cos(gamma), 0],
                       [0, 0, 1]])
        self.X_grid, self.Y_grid, self.Z_grid = np.transpose(
                                            np.dot(np.dot(np.dot(temp, rx), ry) ,rz),
                                            (2,0,1))
        self._temp_vertices = np.transpose(rz.dot(ry.dot(rx.dot(np.transpose(self._temp_vertices)))))

    def getVertices(self):
        self._update_vertices()
        return self.vertices

    def getSurfaceTextPos(self):
        """
        just get position for text
        :return:
        """
        vertices = self.getVertices()
        pos_list = []
        pos = vertices[0]
        pos_list.append(pos)
        pos = vertices[1]
        pos_list.append(pos)
        pos_list = np.array(pos_list)
        return pos_list

    def getBottomSurfaceIndex(self):
        """
        get current bottom surface index
        :return:
        """
        pos_list = self.getSurfaceTextPos()
        if abs(pos_list[0][2] - pos_list[1][2]) < ALMOST_EQUAL_GAP:
            return None
        else:
            return np.argmin(pos_list)

    def drawSurfaceIndexOnAxes(self, ax, **kwargs):
        pos_list = self.getSurfaceTextPos()
        for i in range(pos_list.shape[0]):
            ax.text(*pos_list[i], "{}".format(i), "x", **kwargs)
        ax.scatter3D(pos_list[:, 0], pos_list[:, 1], pos_list[:, 2], **kwargs)

    def drawVerticeIndexOnAxes(self, ax, **kwargs):
        vertices = self.getVertices()
        for i in range(vertices.shape[0]):
            ax.text(*vertices[i], "{}".format(i), "x", **kwargs)



    def drawCylinderOnAxes(self, ax, facecolors=None):

        self.X_grid += self.x
        self.Y_grid += self.y
        self.Z_grid += self.z
        ax.plot_surface(self.X_grid, self.Y_grid, self.Z_grid, color=facecolors)




if __name__ == "__main__":
    fig = plt.figure(figsize=(10,10))
    ax1 = fig.add_subplot(111, projection="3d")
    ax1.cla()
    ax1.set_xlabel("X")
    ax1.set_ylabel("Y")
    ax1.set_zlabel("Z")
    ax1.set_xlim(-0, 12)
    ax1.set_ylim(-0, 12)
    ax1.set_zlim(-0, 12)

    # ax1.grid(False)
    # boite = Boite((1.5,2,2.5), 3,4,5)
    # boite.rotation3d(np.pi/2, 0,0)
    # ax1.add_collection3d(boite.getPoly3d(facecolors=random_mpl_color_rgba()))
    # boite.drawSurfaceIndexOnAxes(ax1, color="red")
    # L, W, H = boite.getCurrentLWH()
    # print("L={}, W={}, H={}".format(L, W, H))
    # num_face, face_dict = boite.getBottomVertices()
    # print(face_dict)
    # boite.drawVerticeIndexOnAxes(ax1)
    Lobject = LForme((1.5, 2, 2.5), 3, 4, 5, 2, 4)
    ax1.add_collection3d(Lobject.getPoly3d(facecolors=random_mpl_color_rgba()))
    Lobject.drawVerticeIndexOnAxes(ax1)
    Lobject.drawSurfaceIndexOnAxes(ax1, color="red")
    center, L, W, H = Lobject.getCurrentLWH_VerticalBoite0()
    print("center = {}, L={}, W={}, H={}".format(center, L, W, H))
    # Lobject.getBottomVertices()

    # ax1.add_collection3d(Lobject.getPoly3d(facecolors=random_mpl_color_rgba()))
    # cylinder = Cylinder((5,5,5), 4, 4, 4, N=50)
    # cylinder.drawVerticeIndexOnAxes(ax1)
    # cylinder.drawSurfaceIndexOnAxes(ax1)
    # cylinder.translation(5,5,5)
    # cylinder.drawCylinderOnAxes(ax1, facecolors=random_mpl_color_rgba())
    # cylinder.rotation3d(np.pi/2, 0, np.pi/2)
    # cylinder.drawCylinderOnAxes(ax1, facecolors=random_mpl_color_rgba())


    plt.show()
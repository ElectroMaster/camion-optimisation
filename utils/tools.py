import pandas as pd
import numpy as np
import os
import re


def read_dechargement_order_csv(csv_file):
    """
    :param csv_file: csv file which save dechargement order
    :return: enterprise order, dechargement order, entreprise_item_dict dict
    """
    lines = pd.read_csv(csv_file)

    entreprise_item_dict = {}
    item_entreprise_dict = {}
    entreprise_order = []
    for i, line in lines.iterrows():
        entreprise = line["Entreprise"]
        item = line["Item"]
        if entreprise not in entreprise_order:
            entreprise_order.append(entreprise)
            entreprise_item_dict[entreprise] = []
        entreprise_item_dict[entreprise].append(item)
        item_entreprise_dict[str(item)] = entreprise
    dechargement_order = []
    for entreprise in entreprise_order:
        sub_list = []
        sub_list.extend(entreprise_item_dict[entreprise])
        dechargement_order.append(sub_list)
    return entreprise_order, dechargement_order, entreprise_item_dict, item_entreprise_dict

def print_dechargement_order(entreprise_order, entreprise_item_dict):
    print("Dechargement order: ")
    for entreprise in entreprise_order:
        print("Entreprise '{}': {};".format(entreprise, entreprise_item_dict[entreprise]))

def read_colis_list_csv(csv_file):
    lines = pd.read_csv(csv_file)
    B = []
    items_weights = []
    items_load_bearing_capacity = []
    for i, line in lines.iterrows():
        item_index = line["Item_index"]
        item_forme = line["Item_forme"]
        item_forme = item_forme.lower()
        item_weight = line["Weight"]
        item_load_bearing_capacity = line["Load_bearing_capacity"]
        items_weights.append(float(item_weight))
        items_load_bearing_capacity.append(float(item_load_bearing_capacity))
        temp_list = []
        if item_forme == "boite":
            for i in range(1,4):
                param_name = "Param{}".format(i)
                param_i = float(line[param_name])
                temp_list.append(param_i)
        elif item_forme == "lforme":
            for i in range(1,6):
                param_name = "Param{}".format(i)
                param_i = float(line[param_name])
                temp_list.append(param_i)
        else:
            raise Exception("Unknown item forme")
        B.append(temp_list)

    return B, items_weights, items_load_bearing_capacity



def load_population(rootdir):
    """
    :param rootdir: parent folder of npy file
    :param file: npy file that we want load, if not set, it will load last pop_{05d}.npy file
    :return:
    """
    list = os.listdir(rootdir)
    num_list = []
    npy_list = []
    for item in list:
        if re.match(r"pop_\d{5}.npy", item) != None:
            num_str = re.sub(r'.npy', "", item)
            num_str = re.sub(r'pop_', "", num_str)
            num_list.append(int(num_str))
            npy_list.append(item)

    if len(num_list) != 0:
        idx = np.argmax(np.array(num_list))
        pop_file = npy_list[idx]
        iteration = num_list[idx]
        pop_file_path = os.path.join(rootdir, pop_file)
        print("Loaded population file {}.".format(pop_file_path))
        pop = np.load(pop_file_path)
    else:
        raise Exception("No population numpy file in folder {}".format(os.path.abspath(rootdir)))

    return pop, iteration

def getItemEntrepriseDictAndEntrepriseItemDict(csv_dict):
    """
    :param csv_file: csv file which save dechargement order
    :return: enterprise order, dechargement order, entreprise_item_dict dict
    """
    df = pd.DataFrame(csv_dict)

    enterprise_item_dict = {}
    item_enterprise_dict = {}
    enterprise_list = []
    for i, line in df.iterrows():
        enterprise = line["Enterprise"]
        item_No = i
        if enterprise not in enterprise_list:
            enterprise_list.append(enterprise)
            enterprise_item_dict[enterprise] = []

        enterprise_item_dict[enterprise].append(item_No)
        item_enterprise_dict[str(item_No)] = enterprise

    print(enterprise_item_dict)
    print(item_enterprise_dict)
    return enterprise_item_dict, item_enterprise_dict

def getIdealDechargementOrderFromEntrepriseOrder(enterprise_order, enterprise_item_dict):
    dechargement_order = []
    for enterprise in enterprise_order:
        sub_list = []
        sub_list.extend(enterprise_item_dict[enterprise])
        dechargement_order.append(sub_list)
    return dechargement_order


def get_rotation3d_dict():
    count = 0
    rotation3d_dict = {}
    for alpha in range(4):
        for beta in range(4):
            for gamma in range(4):
                rotation3d_dict[count] = (alpha*np.pi/2, beta*np.pi/2, gamma*np.pi/2)
                count += 1
    return rotation3d_dict



if __name__ == "__main__":
    entreprise_order, ideal_dechargement_order, entreprise_item_dict, item_entreprise_dict = read_dechargement_order_csv(
                                                        "../ideal_dechargement_order.csv")
    print(entreprise_order)
    print(entreprise_item_dict)
    print(item_entreprise_dict)
    print(ideal_dechargement_order)

    # rotation3d_dict = get_rotation3d_dict()
    # print(rotation3d_dict)
    # B, composants_weights, composants_load_bearing_capacity = read_colis_list_csv("../colis_list.csv")
    # print(B)
    # print(composants_weights)
    # print(composants_load_bearing_capacity)
import pandas as pd
import os
import logging

logging.basicConfig()
log = logging.getLogger("csvTools")
log.setLevel(logging.INFO)


def read_colis_list_csv(csv_file):
    """

    :param csv_file: csv filename
    :return:  csv_dict where all data in
    """
    return read_raw_colis_list_csv(csv_file)

def read_raw_colis_list_csv(csv_file):
    """

    :param csv_file: csv filename
    :return:  csv_dict where all data in
    """
    lines = pd.read_csv(csv_file)
    assert "Item_Geometry" in lines.keys()
    enterprise_list = []
    param1_list = []
    param2_list = []
    param3_list = []
    param4_list = []
    param5_list = []
    weights_list = []
    load_bearing_capacity_list = []
    itemGeometry_list = []
    bottom_surface_No_list = []
    for i, line in lines.iterrows():
        itemGeometry = line["Item_Geometry"].lower()
        try:
            if itemGeometry == "boite" or itemGeometry == "cylinder":
                param1 = line["Param1"]
                param2 = line["Param2"]
                param3 = line["Param3"]
                param4 = 0
                param5 = 0
            elif itemGeometry == "lforme":
                param1 = line["Param1"]
                param2 = line["Param2"]
                param3 = line["Param3"]
                param4 = line["Param4"]
                param5 = line["Param5"]
            else:
                log.warning("Unknown item geometry, will set item geometry 'boite'")
                itemGeometry = "boite"
                param1 = line["Param1"]
                param2 = line["Param2"]
                param3 = line["Param3"]
                param4 = 0
                param5 = 0
        except Exception:
            log.warning("Cannot parse item {}, Will use default values".format(i))
            param1 = 0
            param2 = 0
            param3 = 0
            param4 = 0
            param5 = 0
        itemGeometry_list.append(itemGeometry)
        param1_list.append(param1)
        param2_list.append(param2)
        param3_list.append(param3)
        param4_list.append(param4)
        param5_list.append(param5)

        try:
            enterprise = line["Enterprise"]
        except Exception:
            enterprise = "Default"

        enterprise_list.append(enterprise)

        try:
            weight = float(line["Weight"])
        except Exception:
            weight = 0.0

        weights_list.append(weight)

        try:
            load_bearing_capacity = float(line["Load_bearing_capacity"])
        except Exception:
            load_bearing_capacity = 0.0
        load_bearing_capacity_list.append(load_bearing_capacity)

        try:
            bottom_surface_No = int(line["Bottom_surface_No"])
        except Exception:
            bottom_surface_No = -1
        bottom_surface_No_list.append(bottom_surface_No)


    csv_dict = {}
    csv_dict["Enterprise"] = enterprise_list
    csv_dict["Item_Geometry"] = itemGeometry_list
    csv_dict["Param1"] = param1_list
    csv_dict["Param2"] = param2_list
    csv_dict["Param3"] = param3_list
    csv_dict["Param4"] = param4_list
    csv_dict["Param5"] = param5_list
    csv_dict["Weight"] = weights_list
    csv_dict["Load_bearing_capacity"] = load_bearing_capacity_list
    csv_dict["Bottom_surface_No"] = bottom_surface_No_list

    return csv_dict


def saveIntoTempsCsv(csv_dict, temp_file_name=None, temps_folder=None):
    df = pd.DataFrame(csv_dict)
    if temps_folder == None:
        temps_folder = "temps"
    if temp_file_name == None:
        temp_file_name = ".temps_colis_list.csv#"
    if not os.path.exists(temps_folder):
        os.mkdir(temps_folder)
    csv_path = os.path.join(temps_folder, temp_file_name)
    df.to_csv(csv_path)

def createEmptyCsvDict():
    csv_dict = {}
    csv_dict["Enterprise"] = []
    csv_dict["Item_Geometry"] = []
    csv_dict["Param1"] = []
    csv_dict["Param2"] = []
    csv_dict["Param3"] = []
    csv_dict["Param4"] = []
    csv_dict["Param5"] = []
    csv_dict["Weight"] = []
    csv_dict["Load_bearing_capacity"] = []
    csv_dict["Bottom_surface_No"] = []
    return csv_dict

def readFromTempsCsv(temp_file_name=None, temps_folder=None):
    if temps_folder == None:
        temps_folder = "temps"
    if temp_file_name == None:
        temp_file_name = ".temps_colis_list.csv#"
    csv_path = os.path.join(temps_folder, temp_file_name)
    if not os.path.exists(csv_path):
        csv_dict = createEmptyCsvDict()
    else:
        try:
            csv_dict = read_raw_colis_list_csv(csv_path)
        except Exception:
            csv_dict = createEmptyCsvDict()
    return csv_dict

def deleteTempsCsv(temp_file_name=None, temps_folder=None):
    if temps_folder == None:
        temps_folder = "temps"
    if temp_file_name == None:
        temp_file_name = ".temps_colis_list.csv#"
    csv_path = os.path.join(temps_folder, temp_file_name)
    if os.path.exists(csv_path):
        os.remove(csv_path)





if __name__ == '__main__':
    csv_dict = read_raw_colis_list_csv("../colis_list.csv")
    # df = pd.DataFrame(csv_dict)
    # df.to_csv("test.csv")
    saveIntoTempsCsv(csv_dict)
    csv_dict = readFromTempsCsv()
    print(csv_dict)
import pandas as pd

def get_entreprise_order(dechargement_order):
    alphabet = []
    for letter in range(65, 91):
        alphabet.append(chr(letter))
    entreprise_list = []
    for i in range(len(dechargement_order)):
        sub_entreprise_list = []
        for j in range(len(dechargement_order[i])):
            sub_entreprise_list.append(alphabet[i])
        entreprise_list.append(sub_entreprise_list)
    return entreprise_list

if __name__ == "__main__":
    csv_dict = {}
    ideal_dechargement_order = []
    ideal_dechargement_order.append([11, 12, 4, 6])
    ideal_dechargement_order.append([13, 0])
    ideal_dechargement_order.append([10, 5, 2])
    ideal_dechargement_order.append([14, 7, 9, 8, 1, 3])
    ideal_entreprise_order = get_entreprise_order(ideal_dechargement_order)
    entreprise_list = []
    dechargment_list = []
    for i in range(len(ideal_dechargement_order)):
        entreprise_list.extend(ideal_entreprise_order[i])
        dechargment_list.extend(ideal_dechargement_order[i])
    print(entreprise_list)
    print(dechargment_list)

    csv_dict["Entreprise"] = entreprise_list
    csv_dict["Item"] = dechargment_list

    df = pd.DataFrame(csv_dict)
    df.to_csv("ideal_dechargement_order.csv", index=False)
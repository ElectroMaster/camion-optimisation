import sys
import PyQt5.QtWidgets as PyQtWs 
import PyQt5.QtGui as PyQtGui
import PyQt5.QtCore as PyQtCore



import matplotlib
matplotlib.use("Qt5Agg")


import forme
from subwins import newFileImpl, mplCanvas, welcomePageImpl, helpImpl
import utils.csvTools as csvTools

VERSION = "0.0.1"
VERSION_STR = "V " + VERSION


class SignalEnsemble(PyQtCore.QObject):
    # 1e para means row index of item
    # 2e para means whether we can edit item .
    showEditWin = PyQtCore.pyqtSignal(int, bool)

    # this bool means if we show open file page from menubar, True means yes
    back2NewFileWin = PyQtCore.pyqtSignal(bool)



class MainWidget(PyQtWs.QMainWindow):

    def __init__(self):
        super().__init__()
        self.delelteTempFile()
        self.signalEnsemble = SignalEnsemble()
        self.signalEnsemble.showEditWin.connect(self.showEditWin)
        self.signalEnsemble.back2NewFileWin.connect(self.showNewFileWin)
        self.applyTheme()
        self.initUI()

    def initUI(self):

        self.resize(1366, 768)
        # self.setGeometry(300, 300, 300, 220)
        self.centered()
        self.setWindowTitle('ATHENA')
        self.setWindowIcon(PyQtGui.QIcon('materials/icons/logo-128px.png'))

        self.addCustomMenuBarAndToolBar()
        self.statusBar().showMessage("Ready")

        self.showWelcomePage()


        self.show()

    def centered(self):
        frameGeometry = self.frameGeometry()
        centerPoint = PyQtWs.QDesktopWidget().availableGeometry().center()
        frameGeometry.moveCenter(centerPoint)
        self.move(frameGeometry.topLeft())

    def showNewFileWin(self, fromMenuBar):
        if fromMenuBar:
            self.delelteTempFile()
        self.newFileWidget = PyQtWs.QWidget()
        self.newFileWidget.setProperty("content-background", True)
        hboxl = PyQtWs.QHBoxLayout(self.newFileWidget)
        nw = newFileImpl.NewFileWidget(self.signalEnsemble)
        hboxl.addWidget(nw)
        self.setCentralWidget(self.newFileWidget)
        
    def showOpenFileWin(self):
        fname = PyQtWs.QFileDialog.getOpenFileName(self, 'Open CSV file', '.', "CSV Files (*.csv);;All Files (*)")
        if fname[0]:
            # if you choose some css file
            self.csvFileName = fname[0]
            csv_dict = csvTools.read_raw_colis_list_csv(self.csvFileName)
            csvTools.saveIntoTempsCsv(csv_dict)
            self.showNewFileWin(False)


    def addCustomMenuBarAndToolBar(self):
        ### MenuBar
        menubar = self.menuBar()
        fileMenu = menubar.addMenu('&File')
        helpMenu = menubar.addMenu('&Help')

        # File Menu
        newFileAct = PyQtWs.QAction(PyQtGui.QIcon("materials/icons/new-file-32px.png"),"New...", self)
        newFileAct.triggered.connect(lambda: self.showNewFileWin(True))
        newFileAct.setStatusTip("Create a new colis list csv file")

        openFileAct = PyQtWs.QAction(PyQtGui.QIcon("materials/icons/open-file-32px.png"),"Open...", self)
        openFileAct.setShortcut("Ctrl+O")
        openFileAct.triggered.connect(self.showOpenFileWin)
        openFileAct.setStatusTip("Open a colis list file")


        fileMenu.addAction(newFileAct)
        fileMenu.addAction(openFileAct)

        # Help Menu
        aboutAct = PyQtWs.QAction(PyQtGui.QIcon("materials/icons/question-32px.png"), "About", self)
        aboutAct.triggered.connect(self.showAboutWin)
        aboutAct.setStatusTip("Show information about interface")

        helpMenu.addAction(aboutAct)


        ### ToolBar
        toolbar = self.addToolBar("toolBar")
        toolbar.addAction(newFileAct)
        toolbar.addAction(openFileAct)
        spacer = PyQtWs.QWidget()
        spacer.setSizePolicy(PyQtWs.QSizePolicy.Expanding, PyQtWs.QSizePolicy.Expanding)
        toolbar.addWidget(spacer)
        toolbar.addAction(aboutAct)
        toolbar.setIconSize(PyQtCore.QSize(16,16))


    def showWelcomePage(self):

        self.welcomeWidget = welcomePageImpl.getWelcomeWidget(VERSION_STR)
        self.welcomeWidget.setProperty("content-background", True)
        self.setCentralWidget(self.welcomeWidget)




    def showEditWin(self, row_index, readonlyFlag):
        self.editWidget = PyQtWs.QWidget()
        self.editWidget.setObjectName("editWidget")
        self.editWidget.setProperty("content-background", True)
        hboxl = PyQtWs.QHBoxLayout(self.editWidget)
        ew = newFileImpl.EditItemWidget(row_index,readonlyFlag, self.signalEnsemble)
        hboxl.addWidget(ew)

        self.setCentralWidget(self.editWidget)

    def showAboutWin(self):
        self.aboutWin = helpImpl.AboutWin()

        self.aboutWin.show()



    def applyTheme(self):
        self.setStyleSheet("""
        QWidget[content-background=true]{
        background-color:rgb(255,255,255);
        }
        """)

    def delelteTempFile(self):
        csvTools.deleteTempsCsv()

    def closeEvent(self, event):
        reply = PyQtWs.QMessageBox.question(self, "Message",
                                     "Are you sure to quit?",
                                     PyQtWs.QMessageBox.Yes | PyQtWs.QMessageBox.No,
                                     PyQtWs.QMessageBox.No)
        if reply == PyQtWs.QMessageBox.Yes:
            self.delelteTempFile()
            event.accept()
        else:
            event.ignore()




if __name__ == '__main__':

    globalSS = """
    
    """

    app = PyQtWs.QApplication(sys.argv)
    # app.setFont(PyQtGui.QFont("Calibri Light"))
    app.setStyleSheet(globalSS)
    ex = MainWidget()
    sys.exit(app.exec_())
import numpy as np
import matplotlib
matplotlib.use('TkAgg')
import matplotlib.colors as mplcolors
from mpl_toolkits.mplot3d.art3d import Poly3DCollection
import matplotlib.pyplot as plt
import itertools
import pandas as pd
import sys


def random_mpl_color_rgb():
    return (np.random.uniform(), np.random.uniform(), np.random.uniform())


def random_mpl_color_rgba(alpha=0.25):
    rgba_tup = random_mpl_color_rgb() + (alpha,)
    return rgba_tup


def random_mpl_color_rgb_rgba(alpha=0.25):
    """
    generate a pair of rgb and rgba color, both of them have same R G B value.
    """
    rgb_tup = random_mpl_color_rgb()
    rgba_tup = rgb_tup + (alpha,)
    return rgb_tup, rgba_tup


class Boite:
    def __init__(self, point_ref, longeur, largeur, hauteur):
        assert isinstance(point_ref, tuple)
        assert len(point_ref) == 3
        self.x = point_ref[0]
        self.y = point_ref[1]
        self.z = point_ref[2]
        self.longeur = longeur
        self.largeur = largeur
        self.hauteur = hauteur

        # fristly we think the reference point is at the origin
        self._temp_sommets = np.array([[-longeur / 2.0, -largeur / 2.0, -hauteur / 2.0],
                                       [longeur / 2.0, -largeur / 2.0, -hauteur / 2.0],
                                       [longeur / 2.0, largeur / 2.0, -hauteur / 2.0],
                                       [-longeur / 2.0, largeur / 2.0, -hauteur / 2.0],
                                       [-longeur / 2.0, -largeur / 2.0, hauteur / 2.0],
                                       [longeur / 2.0, -largeur / 2.0, hauteur / 2.0],
                                       [longeur / 2.0, largeur / 2.0, hauteur / 2.0],
                                       [-longeur / 2.0, largeur / 2.0, hauteur / 2.0]])

    def translation(self, length_x, length_y, length_z):
        self.x = self.x + length_x
        self.y = self.y + length_y
        self.z = self.z + length_z

    def rotation3d(self, alpha, beta, gamma):
        """
        :param alpha: perform a rotation about x axis
        :param beta: perform a rotation about y axis
        :param gamma: perform a rotation about z axis
        :return:
        """
        rx = np.array([[1, 0, 0],
                       [0, np.cos(alpha), -np.sin(alpha)],
                       [0, np.sin(alpha), np.cos(alpha)]])
        ry = np.array([[np.cos(beta), 0, np.sin(beta)],
                       [0, 1, 0],
                       [-np.sin(beta), 0, np.cos(beta)]])
        rz = np.array([[np.cos(gamma), -np.sin(gamma), 0],
                       [np.sin(gamma), np.cos(gamma), 0],
                       [0, 0, 1]])
        self._temp_sommets = np.transpose(rz.dot(ry.dot(rx.dot(np.transpose(self._temp_sommets)))))
        self._update_verts()

    def _update_sommets(self):
        # after some operation we need to translate object to real location.
        self.sommets = np.ones((self._temp_sommets.shape[0], 1)) * np.array(
            [self.x, self.y, self.z]) + self._temp_sommets

    def _update_verts(self):
        # obtain all surfaces.
        self._update_sommets()
        self.verts = [[self.sommets[0], self.sommets[1], self.sommets[2], self.sommets[3]],
                      [self.sommets[4], self.sommets[5], self.sommets[6], self.sommets[7]],
                      [self.sommets[0], self.sommets[1], self.sommets[5], self.sommets[4]],
                      [self.sommets[2], self.sommets[3], self.sommets[7], self.sommets[6]],
                      [self.sommets[1], self.sommets[2], self.sommets[6], self.sommets[5]],
                      [self.sommets[4], self.sommets[7], self.sommets[3], self.sommets[0]]]

    def getVerts(self):
        self._update_verts()
        return self.verts

    def getSommets(self):
        self._update_sommets()
        return self.sommets

    def getPoly3d(self, facecolors=None, edgecolors=None):
        if facecolors == None and edgecolors == None:
            edgecolors, facecolors = random_mpl_color_rgb_rgba()
        elif facecolors == None and edgecolors != None:
            if isinstance(edgecolors, str):
                edgecolors = mplcolors.to_rgb(edgecolors)
            assert isinstance(edgecolors, tuple)
            facecolors = (edgecolors[0], edgecolors[1], edgecolors[2]) + (0.25,)
        elif edgecolors == None and facecolors != None:
            if isinstance(facecolors, str):
                facecolors = mplcolors.to_rgb(facecolors)
            assert isinstance(facecolors, tuple)
            edgecolors = (facecolors[0], facecolors[1], facecolors[2])
        self._update_verts()
        poly3d = Poly3DCollection(self.verts,
                                  facecolors=facecolors, linewidths=1, edgecolors=edgecolors)
        return poly3d


class Camion(Boite):
    def __init__(self, point_ref, longeur, largeur, hauteur):
        super().__init__(point_ref, longeur, largeur, hauteur)

    @classmethod
    def atOrigine(cls, longeur, largeur, hauteur):
        return cls((longeur / 2.0, largeur / 2.0, hauteur / 2.0), longeur, largeur, hauteur)

    def init(self, camion_color=(1, 1, 1, 0), edgecolors="black"):
        self.fig = plt.figure()
        self.ax = self.fig.add_subplot(111, projection="3d")
        self.ax1.set_aspect(1)
        self.ax1.add_collection3d(super().getPoly3d(facecolors=camion_color, edgecolors=edgecolors))

    def addPoly3d(self, poly3d):
        self.ax1.add_collection3d(poly3d)

    def draw(self, show_axis=False, show_axis_label=False, show_grid=False):
        axis_switch = "on" if show_axis else "off"
        plt.axis(axis_switch)
        if show_axis_label == True:
            self.ax1.set_xlabel("X")
            self.ax1.set_ylabel("Y")
            self.ax1.set_zlabel("Z")
        self.ax1.grid(show_grid)
        plt.show()


#####################################################################

def translateSubDNA(sub_dna, longeur, dna_size=10):
    res = sub_dna.dot(2 ** np.arange(dna_size)[::-1])
    res = res / float(2 ** dna_size - 1)
    return res * longeur


def translateDNA(pop, longeur, largeur, hauteur, dna_size=10):
    X_all = np.zeros((pop.shape[0], pop.shape[1], 3))
    P_all = np.zeros((pop.shape[0], pop.shape[1]))
    for i in range(pop.shape[0]):
        X_dna_i = pop[i]
        X_dna_i_x = X_dna_i[:, :dna_size]
        X_dna_i_y = X_dna_i[:, dna_size:2 * dna_size]
        X_dna_i_z = X_dna_i[:, 2 * dna_size:3 * dna_size]
        P_dna_i = X_dna_i[:, 3 * dna_size:4 * dna_size]

        X_i_x = translateSubDNA(X_dna_i_x, longeur, dna_size=dna_size)
        X_i_y = translateSubDNA(X_dna_i_y, largeur, dna_size=dna_size)
        X_i_z = translateSubDNA(X_dna_i_z, hauteur, dna_size=dna_size)

        P_i = translateSubDNA(P_dna_i, 6, dna_size=dna_size)
        P_i = P_i.astype(np.int)
        P_i[np.where(P_i == 6)] = 5
        X_i = np.transpose(np.vstack((X_i_x, X_i_y, X_i_z)))
        X_all[i] = X_i
        P_all[i] = P_i

    return X_all, P_all


def F(X, B, C, w_pen=1.0, w_pro=1.0):
    """
    fonction objectifs
    :param X:  matrice de positions des centres des boites
    :param B: matrices de dimensions des boites
    :return:
    """
    m = X.shape[0]
    d = X.shape[1]
    part_penetration = 0
    part_protrusion = 0

    V_intersection = 0
    V_total = 0

    for i in range(m - 1):
        for j in range(i + 1, m):
            Gamma_ij = 1
            Delta_ij_list = []
            for k in range(d):
                Gamma_ij = Gamma_ij * max(0.0, min(X[i, k] + B[i, k] / 2.0, X[j, k] + B[j, k] / 2.0) -
                                          max(X[i, k] - B[i, k] / 2.0, X[j, k] - B[j, k] / 2.0))
                Delta_ij_list.append(max(0.0, (B[i, k] + B[j, k]) / 2.0 - abs(X[i, k] - X[j, k])))
            Delta_ij = min(Delta_ij_list)

            V_intersection = V_intersection + Gamma_ij
            part_penetration = part_penetration + Gamma_ij * Delta_ij
            # print("Partie penetration entre composant {} et {}: {}".format(i, j, Gamma_ij*Delta_ij))

    for i in range(m):
        Gamma_bar_part1 = 1
        Gamma_bar_part2 = 1
        Delta_bar_list_i = []
        for k in range(d):
            Gamma_bar_part1 = Gamma_bar_part1 * B[i, k]
            Gamma_bar_part2 = Gamma_bar_part2 * max(0.0, min(C[k], X[i, k] + B[i, k] / 2.0) - max(0.0, X[i, k] - B[
                i, k] / 2.0))

            Delta_bar_list_i.append(max(0.0, abs(X[i, k] - C[k] / 2.0) + (B[i, k] - C[k]) / 2.0))

        Gamma_bar_i = Gamma_bar_part1 - Gamma_bar_part2
        # print("composant {}: {}".format(i, Gamma_bar_i))
        Delta_bar_i = max(Delta_bar_list_i)

        V_total = V_total + Gamma_bar_part1
        part_protrusion = part_protrusion + Gamma_bar_i * Delta_bar_i
        # print("Partie protrusion de composant {}: {}".format(i, Gamma_bar_i*Delta_bar_i))

    # compute V_c
    B_c = np.max(X+B/2, axis = 0) - np.min(X-B/2, axis=0)
    V_c = 1
    for k in range(d):
        V_c = V_c*B_c[k]
    vide_ratio = 0
    vide_ratio = 1 - (V_total - V_intersection)/(V_c)
    # print(vide_ratio)
    return w_pen * part_penetration + w_pro * part_protrusion, vide_ratio


# def get_fitness(pred1, pred2):
#     """
#     On veut F le plus bas
#     :param pred:
#     :return:
#     """
#     pred = pred1
#     h = np.max(pred) - pred + 1e-4
#     return h/h.sum()

# def get_fitness(pred1, pred2):
#     """
#     On veut F le plus bas
#     :param pred:
#     :return:
#     """
#     pred = np.multiply(pred1, pred2)
#     h = np.max(pred) - pred + 1e-4
#     return h/h.sum()

def get_fitness(pred1, pred2):
    """
    On veut F le plus bas
    :param pred:
    :return:
    """
    h1 = np.max(pred1) - pred1
    h1 = h1 / h1.sum()
    # print(h1.sum())
    h2 = np.max(pred1) - pred1
    h2 = h2 / h2.sum()
    # print(h2.sum())
    return (h1 + h2) / 2.0


def select(pop, fitness):
    idx = np.random.choice(np.arange(pop.shape[0]), size=pop.shape[0],
                           replace=True, p=fitness)
    return pop[idx]


def crossover(parent, pop, cross_rate=0.8):
    if np.random.rand() < cross_rate:
        # si il y a des croissement avec individus idx
        idx = np.random.randint(0, pop.shape[0], size=1)
        cross_points = np.random.randint(0, 2, size=(pop.shape[1], pop.shape[2])).astype(np.bool)
        parent[cross_points] = pop[idx, cross_points]
    return parent


def mutate(child, mutation_rate=3e-4):
    for i in range(child.shape[0]):
        for j in range(child.shape[1]):
            if np.random.rand() < mutation_rate:
                child[i, j] = 1 - child[i, j]
    return child


def compute_scores(X_all, P_all, B, C, permutation_array):
    pop_size = X_all.shape[0]
    B_all = np.zeros((pop_size, B.shape[0], B.shape[1]))
    scores = np.zeros((pop_size, 2))
    for i in range(pop_size):
        B_i = np.zeros_like(B)
        for j in range(B.shape[0]):
            B_i[j] = B[j, permutation_array[int(P_all[i][j])]]
        B_all[i] = B_i
        F_values, vide_values = F(X_all[i], B_i, C)
        scores[i, 0] = F_values
        scores[i, 1] = vide_values
    return scores, B_all

def identify_pareto(scores, pop_ids):
    """
    chercher les points situe au pareto front
    :param scores:
    :return: les index des points
    """
    pop_size = scores.shape[0]
    # on suppose tous les points sont dans pareto front
    belong_pareto_front = np.ones(pop_size, dtype=bool)
    for i in range(pop_size):
        for j in range(pop_size):
            if all(scores[j] <= scores[i]) and any(scores[j] < scores[i]):
                # si un point i n'est pas dans pareto front, on le supprime.
                belong_pareto_front[i] = 0
                break
    return pop_ids[belong_pareto_front]


def calculate_crowding(scores):

    pop_size = scores.shape[0]
    scores_dims = scores.shape[1]

    # create crowding matrix of population (row) and score (column)
    crowding_matrix = np.zeros((pop_size, scores_dims))

    # normalise scores (ptp is max-min)
    normed_scores = (scores - scores.min(0)) / scores.ptp(0)

    # calculate crowding distance for each score in turn
    for col in range(scores_dims):
        crowding = np.zeros(pop_size)

        # end points have maximum crowding
        crowding[0] = 1
        crowding[pop_size - 1] = 1

        # Sort each score (to calculate crowding between adjacent scores)
        sorted_scores = np.sort(normed_scores[:, col])

        sorted_scores_index = np.argsort(
            normed_scores[:, col])

        # Calculate crowding distance for each individual
        crowding[1:pop_size - 1] = sorted_scores[2:pop_size] - sorted_scores[0:pop_size - 2]

        # resort to orginal order (two steps)
        re_sort_order = np.argsort(sorted_scores_index)
        sorted_crowding = crowding[re_sort_order]

        # Record crowding distances
        crowding_matrix[:, col] = sorted_crowding

    # Sum crowding distances of each score
    crowding_distances = np.sum(crowding_matrix, axis=1)

    return crowding_distances


def reduce_by_crowding(scores, number_to_select):
    """
    This function selects a number of solutions based on tournament of
    crowding distances. Two members of the population are picked at
    random. The one with the higher croding dostance is always picked
    """
    pop_ids = np.arange(scores.shape[0])
    crowding_distances = calculate_crowding(scores)
    picked_pop_ids = np.zeros((number_to_select))
    picked_scores = np.zeros((number_to_select, scores.shape[1]))

    for i in range(number_to_select):

        pop_size = pop_ids.shape[0]

        fighter1ID = np.random.randint(0, pop_size)
        fighter2ID = np.random.randint(0, pop_size)

        # If fighter # 1 is better
        if crowding_distances[fighter1ID] >= crowding_distances[
            fighter2ID]:

            # add solution to picked solutions array
            picked_pop_ids[i] = pop_ids[
                fighter1ID]

            # Add score to picked scores array
            picked_scores[i, :] = scores[fighter1ID, :]

            # remove selected solution from available solutions
            pop_ids = np.delete(pop_ids, (fighter1ID), axis=0)

            scores = np.delete(scores, (fighter1ID), axis=0)

            crowding_distances = np.delete(crowding_distances, (fighter1ID), axis=0)
        else:
            picked_pop_ids[i] = pop_ids[fighter2ID]

            picked_scores[i, :] = scores[fighter2ID, :]

            pop_ids = np.delete(pop_ids, (fighter2ID), axis=0)

            scores = np.delete(scores, (fighter2ID), axis=0)

            crowding_distances = np.delete(
                crowding_distances, (fighter2ID), axis=0)

    # Convert to integer
    picked_pop_ids = np.asarray(picked_pop_ids, dtype=int)

    return picked_pop_ids

def build_pareto_population(pop, scores, pop_size):
    """

    :param pop:
    :param scores:
    :return:
    """
    unselected_pop_ids = np.arange(pop.shape[0])
    all_pop_ids = np.arange(pop.shape[0])
    pareto_front_ids = np.array([])
    while len(pareto_front_ids) < pop_size:
        temp_pareto_front_ids = identify_pareto(scores[unselected_pop_ids],
                                                unselected_pop_ids)
        # check size of total pareto front if larger than pop_size, reduce new pareto front by crowding
        combined_pareto_size = len(pareto_front_ids) + len(temp_pareto_front_ids)
        # print("len_pareto_front_ids:{}, len_pareto_front_ids:{}".format(len(pareto_front_ids), len(temp_pareto_front_ids)))
        if combined_pareto_size > pop_size:
            number_to_select = pop_size - len(pareto_front_ids)
            selected_individus = reduce_by_crowding(scores[temp_pareto_front_ids], number_to_select)
            temp_pareto_front_ids = temp_pareto_front_ids[selected_individus]

        # Add latest pareto front to full Pareto front
        pareto_front_ids = np.hstack((pareto_front_ids, temp_pareto_front_ids))

        # Update unslected population ID by using sets to find IDs in all
        # ids that are not in the selected front
        unselected_set = set(all_pop_ids) - set(pareto_front_ids)
        unselected_pop_ids = np.array(list(unselected_set))

    # convert to integer
    pop = pop[pareto_front_ids.astype(int)]
    return pop, pareto_front_ids.astype(int)



if __name__ == '__main__':

    camion_largeur = 10;
    camion_longeur = 10;
    camion_hauteur = 10;
    camion = Camion.atOrigine(camion_largeur, camion_largeur, camion_hauteur);

    # B = np.array([[3.0, 4.0, 1.0],
    #              [2.0, 4.0, 2.0],
    #              [4.0, 4.0, 3.0],
    #              [4.0, 1.0, 2.0],
    #              [2.0, 2.0, 4.0],
    #              [1.0, 4.0, 3.0],
    #              [1.0, 4.0, 4.0],
    #              [3.0, 4.0, 3.0],
    #              [4.0, 1.0, 3.0],
    #              [1.0, 1.0, 1.0]])

    # B = np.array([[5.0, 5.0, 5.0],
    #              [5.0, 5.0, 5.0],
    #              [5.0, 5.0, 5.0],
    #              [5.0, 5.0, 5.0],
    #              [5.0, 5.0, 5.0],
    #               [5.0, 5.0, 5.0],
    #               [5.0, 5.0, 5.0],
    #               [5.0, 5.0, 5.0]])

    # B = np.random.randint(1,6, size=(nb_composant, 3)).astype(np.float)

    # B = np.load("B_15_items.npy")
    B = np.load("6_item.npy")
    np.save("B.npy", B)
    nb_composant = B.shape[0]
    # np.save("B.npy", B)
    print(B)
    V_total = 0
    for i in range(B.shape[0]):
        V_i = 1
        for k in range(B.shape[1]):
            V_i = V_i * B[i, k]
        V_total = V_total + V_i
    print("le volume des tous composants:")
    print(V_total)
    permutation_list = list(itertools.permutations([0, 1, 2]))
    permutation_array = np.asarray(permutation_list, dtype=np.int)
    C = np.array([camion_longeur, camion_largeur, camion_hauteur]).astype(np.float)
    np.save("C.npy", C)
    ## creer la population initiale
    # 用基因对composant的位置信息，朝向信息进行编码。

    POP_SIZE = 200
    # on utlise un dna qui a un longeur 10 pour exprimer un x,
    # on effet, pour exprimer (x,y,z) , il faut un dna avec longeur 30
    # le dernier dna qui a longeur 10 est pour exprimer l'orientation
    DNA_SIZE = 10

    # initialializer la population
    pop = np.random.randint(2, size=(POP_SIZE, nb_composant, DNA_SIZE * 4))
    # afficher tous les valeurs d'une matrice
    np.set_printoptions(threshold=sys.maxsize)

    fig = plt.figure(figsize=(10,10))
    ax1 = fig.add_subplot(221, projection="3d")
    ax2 = fig.add_subplot(222)

    ax3 = fig.add_subplot(223)


    ax4 = fig.add_subplot(224)


    ax1.set_aspect(1)
    F_min_values = []
    vide_idx_values = []

    iterations = 2000

    boite_color_list = []
    for i in range(nb_composant):
        boite_color_list.append(random_mpl_color_rgba())
    for _iter in range(iterations):
        pop_copy = pop.copy()
        for parent in pop:
            # croissement entre les parents
            child = crossover(parent, pop)
            pop_copy = np.append(pop_copy, np.expand_dims(child, axis=0), axis=0)
        # supprimer les individus qui sont pareils. maintenant pop_size peut etre n'est pas pop_size
        pop = np.unique(pop_copy, axis=0)
        shuffle_idx = np.arange(pop.shape[0])
        np.random.shuffle(shuffle_idx)
        pop = pop[shuffle_idx]
        print("Before building pareto front, pop_size: {}".format(pop.shape))
        # mutation
        for child in pop:
            child[:] = mutate(child, mutation_rate=3e-2/(int(_iter/100)*100+1))

        X_all, P_all = translateDNA(pop, camion_longeur, camion_largeur, camion_hauteur)
        scores, B_all = compute_scores(X_all, P_all, B, C, permutation_array)
        pop, next_generation_ids = build_pareto_population(pop, scores, POP_SIZE)
        if _iter % 100 == 0 and _iter !=0:
            np.save("pop_{:0>5d}.npy".format(_iter),pop)


        X_all = X_all[next_generation_ids]
        B_all = B_all[next_generation_ids]
        scores = scores[next_generation_ids]

        individus_at_1st_pareto_idx = identify_pareto(scores, np.arange(POP_SIZE))
        scores_at_1st_pareto = scores[individus_at_1st_pareto_idx]
        # print(individus_at_1st_pareto_idx)
        sorted_scores = scores_at_1st_pareto[np.argsort(scores_at_1st_pareto[:,1])]
        # print(scores_at_1st_pareto)
        # print(np.argmin(scores_at_1st_pareto[:,1], axis=0))
        # print(individus_at_1st_pareto_idx)
        # solution_chosen = int(np.random.choice(individus_at_1st_pareto_idx, 1))
        solution_chosen = individus_at_1st_pareto_idx[np.argmin(scores_at_1st_pareto[:,1], axis=0)]
        # print(solution_chosen)

        X = X_all[solution_chosen]
        B_idx = B_all[solution_chosen]
        score_chosen = scores[solution_chosen]
        F_min_values.append(score_chosen[0])
        vide_idx_values.append(score_chosen[1])
        # on choisit le plus moins compact mais respect meilleur les contraintes non-chevauchement

        print("iteration {} - F_values: {}; Vide ratio: {};".format(_iter, score_chosen[0], score_chosen[1]))

        ax1.cla()
        ax2.cla()
        ax3.cla()
        if _iter % 10 ==0:
            ax4.cla()
        ax1.set_xlabel("X")
        ax1.set_ylabel("Y")
        ax1.set_zlabel("Z")
        ax1.set_xlim(-0, camion_largeur)
        ax1.set_ylim(-0, camion_longeur)
        ax1.set_zlim(-0, camion_hauteur)
        ax1.grid(False)
        ax1.add_collection3d(camion.getPoly3d(facecolors=(1, 1, 1, 0), edgecolors="black"))
        boite_list = []
        for i in range(X.shape[0]):
            boite = Boite((X[i, 0], X[i, 1], X[i, 2]), B_idx[i, 0], B_idx[i, 1], B_idx[i, 2])
            boite_list.append(boite)
            ax1.add_collection3d(boite.getPoly3d(facecolors=boite_color_list[i]))
            ax1.text(*X[i, :], str(i), 'x')

        ax2.set_xlabel("Iterations")
        ax2.set_ylabel("F values")
        ax2.plot(np.arange(len(F_min_values)), F_min_values, 'b-', linewidth=1)
        ax3.set_xlabel("Iterations")
        ax3.set_ylabel("vide ratio values")
        ax3.plot(np.arange(len(F_min_values)), vide_idx_values, 'b-', linewidth=1)

        # plot
        ax4.set_xlabel("vide ratio")
        ax4.set_ylabel("F value")
        ax4.scatter(scores[:, 1], scores[:, 0], color=random_mpl_color_rgba())
        # ax4.scatter(scores[individus_at_1st_pareto_idx, 1], scores[individus_at_1st_pareto_idx, 0], marker="^", color=random_mpl_color_rgb())
        ax4.plot(sorted_scores[:,1], sorted_scores[:,0])
        plt.pause(0.1)
        fig.savefig("chart.png")


















        # X_all, P_all = translateDNA(pop, camion_longeur, camion_largeur, camion_hauteur)
        # B_all = np.zeros((POP_SIZE, B.shape[0], B.shape[1]))
        # F_values = np.zeros(POP_SIZE)
        # vide_values = np.zeros(POP_SIZE)
        # for i in range(POP_SIZE):
        #     B_i = np.zeros_like(B)
        #     for j in range(B.shape[0]):
        #         B_i[j] = B[j, permutation_array[int(P_all[i][j])]]
        #     B_all[i] = B_i
        #     F_values[i], vide_values[i] = F(X_all[i], B_i, C)
        # fitness = get_fitness(F_values, vide_values)
        #
        # idx = np.argmax(fitness)
        # X = X_all[idx]
        # B_idx = B_all[idx]
        #
        # if _iter != 0 and _iter % 100 == 0:
        #     np.save("pop_{}.npy".format(_iter), pop)
        #     np.save("fitness_{}.npy".format(_iter), fitness)
        #     np.save("B-all_{}.npy".format(_iter), B_all)
        #
        # pop = select(pop, fitness)
        # pop_copy = pop.copy()
        # for parent in pop:
        #     child = crossover(parent, pop_copy)
        #     child = mutate(child)
        #     parent[:] = child
        #
        # ax1.cla()
        # ax1.set_xlabel("X")
        # ax1.set_ylabel("Y")
        # ax1.set_zlabel("Z")
        # ax1.set_xlim(-0, camion_largeur)
        # ax1.set_ylim(-0, camion_longeur)
        # ax1.set_zlim(-0, camion_hauteur)
        # ax1.grid(False)
        # ax1.add_collection3d(camion.getPoly3d(facecolors=(1, 1, 1, 0), edgecolors="black"))
        # boite_list = []
        # for i in range(X.shape[0]):
        #     boite = Boite((X[i, 0], X[i, 1], X[i, 2]), B_idx[i, 0], B_idx[i, 1], B_idx[i, 2])
        #     boite_list.append(boite)
        #     ax1.add_collection3d(boite.getPoly3d(facecolors=boite_color_list[i]))
        #     ax1.text(*X[i, :], str(i), 'x')
        # plt.pause(0.1)
        # F_min_values.append(F_values[idx])
        # vide_idx_values.append(vide_values[idx])
        # ax2.plot(np.arange(len(F_min_values)), F_min_values, 'b-', linewidth=1)
        # ax3.plot(np.arange(len(F_min_values)), vide_idx_values, 'b-', linewidth=1)
        #
        # print("iteration {} - F_values: {}; Vide ratio: {};".format(_iter, F_values[idx], vide_values[idx]))
        # # critere
        #
        # if F_values[idx] < 1e-3:
        #     fig.savefig("result.png")
        #     break
        #
        # # break











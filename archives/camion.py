import numpy as np
import matplotlib.pyplot as plt

from mpl_toolkits.mplot3d import Axes3D
from mpl_toolkits.mplot3d.art3d import Poly3DCollection, Line3DCollection
from matplotlib.figure import Figure
from forme import Cuboid, Pyramid
class Camion(Cuboid):
    def __init__(self, point_ref, longeur, largeur, hauteur):
        super().__init__(point_ref, longeur, largeur, hauteur)


    @classmethod
    def atOrigine(cls, longeur, largeur, hauteur):
        return cls((0,0,0), longeur, largeur, hauteur)

    def init(self, camion_color = (1,1,1, 0), edgecolors="black"):
        self.fig = plt.figure()
        self.ax = self.fig.add_subplot(111, projection="3d")
        self.ax.set_aspect(1)
        # dessiner les sommets
        # self.ax.scatter3D(self.sommets[:, 0], self.sommets[:, 1], self.sommets[:, 2])
        # dessiner les bords
        self.ax.add_collection3d(super().getPoly3d(facecolors=camion_color, edgecolors=edgecolors))

    def addPoly3d(self, poly3d):
        self.ax.add_collection3d(poly3d)

    def addSpheroid(self,  center_x, center_y, center_z, radius_x, radius_y, radius_z, color=(1,0,0,0.25)):
            u = np.linspace(0, 2 * np.pi, 100)
            v = np.linspace(0, np.pi, 100)
            x = radius_x * np.outer(np.cos(u), np.sin(v)) + center_x
            y = radius_y * np.outer(np.sin(u), np.sin(v)) + center_y
            z = radius_z * np.outer(np.ones(np.size(u)), np.cos(v)) + center_z
            # Plot the surface
            self.ax.plot_surface(x, y, z, color=color)

    def addSphere(self,  center_x, center_y, center_z, radius=1, color=(1,0,0,0.25)):
        self.addSpheroid(center_x, center_y, center_z, radius, radius, radius, color=color)

    def draw(self, show_axis=False, show_axis_label=False):
        axis_switch = "on" if show_axis else "off"
        plt.axis(axis_switch)
        if show_axis_label==True:
            self.ax.set_xlabel("X")
            self.ax.set_ylabel("Y")
            self.ax.set_zlabel("Z")
        plt.show()


if __name__ == '__main__':

    camion = Camion.atOrigine(2,2,2)
    cuboid = Cuboid((1,1,1), 1,2,1)
    pyramid = Pyramid((1,1,0), 1,1, (1.5,1.5, 1))
    camion.init()
    camion.ax.set_xlim(-1, 3)
    camion.ax.set_ylim(-1, 3)
    camion.ax.set_zlim(-1, 3)
    ######################################
    ## clean old 3d object ##

    # one3d = cuboid.getPoly3d()
    # camion.addPoly3d(one3d)
    # one3d.remove()
    ######################################
    cuboid.rotation3d(0, 0, np.pi/4)
    camion.addPoly3d(cuboid.getPoly3d())
    cuboid2 = Cuboid((0,0,0), 1,1,1)
    camion.addPoly3d(cuboid2.getPoly3d())

    cuboid2.translation(2,2,2)
    camion.addPoly3d(cuboid2.getPoly3d())


    camion.addPoly3d(pyramid.getPoly3d())
    pyramid.rotation3d(np.pi/4, 0,0)
    camion.addPoly3d(pyramid.getPoly3d())
    # camion.addSphere(1,1,1)
    # camion.addSpheroid(1,1,1, 1,1,1)
    camion.draw()

import numpy as np
import matplotlib
matplotlib.use('TkAgg')
import matplotlib.colors as mplcolors
from mpl_toolkits.mplot3d.art3d import Poly3DCollection
import matplotlib.pyplot as plt
import itertools
import pandas as pd
import sys


def random_mpl_color_rgb():
    return (np.random.uniform(), np.random.uniform(), np.random.uniform())


def random_mpl_color_rgba(alpha=0.25):
    rgba_tup = random_mpl_color_rgb() + (alpha,)
    return rgba_tup


def random_mpl_color_rgb_rgba(alpha=0.25):
    """
    generate a pair of rgb and rgba color, both of them have same R G B value.
    """
    rgb_tup = random_mpl_color_rgb()
    rgba_tup = rgb_tup + (alpha,)
    return rgb_tup, rgba_tup


class Boite:
    def __init__(self, point_ref, longeur, largeur, hauteur):
        assert isinstance(point_ref, tuple)
        assert len(point_ref) == 3
        self.x = point_ref[0]
        self.y = point_ref[1]
        self.z = point_ref[2]
        self.longeur = longeur
        self.largeur = largeur
        self.hauteur = hauteur

        # fristly we think the reference point is at the origin
        self._temp_sommets = np.array([[-longeur / 2.0, -largeur / 2.0, -hauteur / 2.0],
                                       [longeur / 2.0, -largeur / 2.0, -hauteur / 2.0],
                                       [longeur / 2.0, largeur / 2.0, -hauteur / 2.0],
                                       [-longeur / 2.0, largeur / 2.0, -hauteur / 2.0],
                                       [-longeur / 2.0, -largeur / 2.0, hauteur / 2.0],
                                       [longeur / 2.0, -largeur / 2.0, hauteur / 2.0],
                                       [longeur / 2.0, largeur / 2.0, hauteur / 2.0],
                                       [-longeur / 2.0, largeur / 2.0, hauteur / 2.0]])

    def translation(self, length_x, length_y, length_z):
        self.x = self.x + length_x
        self.y = self.y + length_y
        self.z = self.z + length_z

    def rotation3d(self, alpha, beta, gamma):
        """
        :param alpha: perform a rotation about x axis
        :param beta: perform a rotation about y axis
        :param gamma: perform a rotation about z axis
        :return:
        """
        rx = np.array([[1, 0, 0],
                       [0, np.cos(alpha), -np.sin(alpha)],
                       [0, np.sin(alpha), np.cos(alpha)]])
        ry = np.array([[np.cos(beta), 0, np.sin(beta)],
                       [0, 1, 0],
                       [-np.sin(beta), 0, np.cos(beta)]])
        rz = np.array([[np.cos(gamma), -np.sin(gamma), 0],
                       [np.sin(gamma), np.cos(gamma), 0],
                       [0, 0, 1]])
        self._temp_sommets = np.transpose(rz.dot(ry.dot(rx.dot(np.transpose(self._temp_sommets)))))
        self._update_verts()

    def _update_sommets(self):
        # after some operation we need to translate object to real location.
        self.sommets = np.ones((self._temp_sommets.shape[0], 1)) * np.array(
            [self.x, self.y, self.z]) + self._temp_sommets

    def _update_verts(self):
        # obtain all surfaces.
        self._update_sommets()
        self.verts = [[self.sommets[0], self.sommets[1], self.sommets[2], self.sommets[3]],
                      [self.sommets[4], self.sommets[5], self.sommets[6], self.sommets[7]],
                      [self.sommets[0], self.sommets[1], self.sommets[5], self.sommets[4]],
                      [self.sommets[2], self.sommets[3], self.sommets[7], self.sommets[6]],
                      [self.sommets[1], self.sommets[2], self.sommets[6], self.sommets[5]],
                      [self.sommets[4], self.sommets[7], self.sommets[3], self.sommets[0]]]

    def getVerts(self):
        self._update_verts()
        return self.verts

    def getSommets(self):
        self._update_sommets()
        return self.sommets

    def getPoly3d(self, facecolors=None, edgecolors=None):
        if facecolors == None and edgecolors == None:
            edgecolors, facecolors = random_mpl_color_rgb_rgba()
        elif facecolors == None and edgecolors != None:
            if isinstance(edgecolors, str):
                edgecolors = mplcolors.to_rgb(edgecolors)
            assert isinstance(edgecolors, tuple)
            facecolors = (edgecolors[0], edgecolors[1], edgecolors[2]) + (0.25,)
        elif edgecolors == None and facecolors != None:
            if isinstance(facecolors, str):
                facecolors = mplcolors.to_rgb(facecolors)
            assert isinstance(facecolors, tuple)
            edgecolors = (facecolors[0], facecolors[1], facecolors[2])
        self._update_verts()
        poly3d = Poly3DCollection(self.verts,
                                  facecolors=facecolors, linewidths=1, edgecolors=edgecolors)
        return poly3d


class Camion(Boite):
    def __init__(self, point_ref, longeur, largeur, hauteur):
        super().__init__(point_ref, longeur, largeur, hauteur)

    @classmethod
    def atOrigine(cls, longeur, largeur, hauteur):
        return cls((longeur / 2.0, largeur / 2.0, hauteur / 2.0), longeur, largeur, hauteur)

    def init(self, camion_color=(1, 1, 1, 0), edgecolors="black"):
        self.fig = plt.figure()
        self.ax = self.fig.add_subplot(111, projection="3d")
        self.ax1.set_aspect(1)
        self.ax1.add_collection3d(super().getPoly3d(facecolors=camion_color, edgecolors=edgecolors))

    def addPoly3d(self, poly3d):
        self.ax1.add_collection3d(poly3d)

    def draw(self, show_axis=False, show_axis_label=False, show_grid=False):
        axis_switch = "on" if show_axis else "off"
        plt.axis(axis_switch)
        if show_axis_label == True:
            self.ax1.set_xlabel("X")
            self.ax1.set_ylabel("Y")
            self.ax1.set_zlabel("Z")
        self.ax1.grid(show_grid)
        plt.show()


#####################################################################
def overlap2d(center_1, length_1, width_1, center_2, length_2, width_2):
    overlap_length = max(0, min(center_1[0] + length_1/2, center_2[0]+length_2/2) -
                        max(center_1[0] - length_1/2, center_2[0] - length_2/2))
    overlap_width = max(0, min(center_1[1] + width_1/2, center_2[1]+width_2/2) -
                        max(center_1[1] - width_1/2, center_2[1] - width_2/2))
    overlap_area = overlap_length * overlap_width
    ret = False
    if overlap_area != 0:
        ret = True
    return ret, overlap_area, overlap_length, overlap_width

def addGravity(X, B, len_ignore=0.1):
    X_copy = X.copy()
    z_set = set(X_copy[:, 2])
    placement_order = []
    while (len(z_set) != 0):
        lowest_z = min(z_set)
        idx = X_copy[:, 2] == lowest_z
        msg_str = []
        for i, j in enumerate(list(idx)):
            if j == True:
                msg_str.append(i)
        # print("lowest z: {}, Number of lowest composant: {}".format(lowest_z, msg_str))
        placement_order.extend(msg_str)
        z_set.remove(lowest_z)

    # print("Placement order: ", placement_order)

    placed_list = []
    for composant_i in placement_order:
        # print("considering composant i >>> {} ...".format(composant_i))
        # print(placed_list)
        z_last_composant = 0
        if len(placed_list) != 0:
            overlap_list = []
            for composant_j in placed_list:
                # print("--composant j >>> {}".format(composant_j))
                ret, _, overlap_length, overlap_width = overlap2d(X_copy[composant_i], B[composant_i, 0], B[composant_i, 1],
                                   X_copy[composant_j], B[composant_j, 0], B[composant_j, 1])
                # when overlap exist and all overlap lenth larger than len_ignore, we stack it on another composant
                if (ret and overlap_length>len_ignore and overlap_width>len_ignore):
                    overlap_list.append(composant_j)
            # print("overlap_list: {}".format(overlap_list))
            if len(overlap_list) == 0:
                z_last_composant = 0
            else:
                z_last_composant = np.max(X_copy[overlap_list, 2] + B[overlap_list, 2] / 2)
        z_i = B[composant_i, 2] / 2 + z_last_composant
        # print("z_i: ", z_i)
        X_copy[composant_i, 2] = z_i

        placed_list.append(composant_i)
    return X_copy



def translateSubDNA(sub_dna, longeur, dna_size=10):
    res = sub_dna.dot(2 ** np.arange(dna_size)[::-1])
    res = res / float(2 ** dna_size - 1)
    return res * longeur


def translateDNA(pop, longeur, largeur, hauteur, dna_size=10):
    X_all = np.zeros((pop.shape[0], pop.shape[1], 3))
    P_all = np.zeros((pop.shape[0], pop.shape[1]))
    for i in range(pop.shape[0]):
        X_dna_i = pop[i]
        X_dna_i_x = X_dna_i[:, :dna_size]
        X_dna_i_y = X_dna_i[:, dna_size:2 * dna_size]
        X_dna_i_z = X_dna_i[:, 2 * dna_size:3 * dna_size]
        P_dna_i = X_dna_i[:, 3 * dna_size:4 * dna_size]

        X_i_x = translateSubDNA(X_dna_i_x, longeur, dna_size=dna_size)
        X_i_y = translateSubDNA(X_dna_i_y, largeur, dna_size=dna_size)
        X_i_z = translateSubDNA(X_dna_i_z, hauteur, dna_size=dna_size)

        P_i = translateSubDNA(P_dna_i, 6, dna_size=dna_size)
        P_i = P_i.astype(np.int)
        P_i[np.where(P_i == 6)] = 5
        X_i = np.transpose(np.vstack((X_i_x, X_i_y, X_i_z)))
        X_all[i] = X_i
        P_all[i] = P_i

    return X_all, P_all

def F(X, B, C, w_pen=1.0, w_pro=1.0):
    """
    fonction objectifs
    :param X:  matrice de positions des centres des boites
    :param B: matrices de dimensions des boites
    :return:
    """
    m = X.shape[0]
    d = X.shape[1]
    part_penetration = 0
    part_protrusion = 0


    for i in range(m - 1):
        for j in range(i + 1, m):
            Gamma_ij = 1
            Delta_ij_list = []
            for k in range(d):
                Gamma_ij = Gamma_ij * max(0.0, min(X[i, k] + B[i, k] / 2.0, X[j, k] + B[j, k] / 2.0) -
                                          max(X[i, k] - B[i, k] / 2.0, X[j, k] - B[j, k] / 2.0))
                Delta_ij_list.append(max(0.0, (B[i, k] + B[j, k]) / 2.0 - abs(X[i, k] - X[j, k])))
            Delta_ij = min(Delta_ij_list)

            part_penetration = part_penetration + Gamma_ij * Delta_ij
            # print("Partie penetration entre composant {} et {}: {}".format(i, j, Gamma_ij*Delta_ij))

    for i in range(m):
        Gamma_bar_part1 = 1
        Gamma_bar_part2 = 1
        Delta_bar_list_i = []
        for k in range(d):
            Gamma_bar_part1 = Gamma_bar_part1 * B[i, k]
            Gamma_bar_part2 = Gamma_bar_part2 * max(0.0, min(C[k], X[i, k] + B[i, k] / 2.0) - max(0.0, X[i, k] - B[
                i, k] / 2.0))

            Delta_bar_list_i.append(max(0.0, abs(X[i, k] - C[k] / 2.0) + (B[i, k] - C[k]) / 2.0))

        Gamma_bar_i = Gamma_bar_part1 - Gamma_bar_part2
        # print("composant {}: {}".format(i, Gamma_bar_i))
        Delta_bar_i = max(Delta_bar_list_i)

        part_protrusion = part_protrusion + Gamma_bar_i * Delta_bar_i
        # print("Partie protrusion de composant {}: {}".format(i, Gamma_bar_i*Delta_bar_i))


    return w_pen * part_penetration + w_pro * part_protrusion

# def F(X, B, C):
#     """
#     fonction des contraintes, this version not consider intersection
#     :param X:  matrice de positions des centres des boites
#     :param B: matrices de dimensions des boites
#     :return:
#     """
#     m = X.shape[0]
#     d = X.shape[1]
#     part_protrusion = 0
#
#     for i in range(m):
#         Gamma_bar_part1 = 1
#         Gamma_bar_part2 = 1
#         Delta_bar_list_i = []
#         for k in range(d):
#             Gamma_bar_part1 = Gamma_bar_part1 * B[i, k]
#             Gamma_bar_part2 = Gamma_bar_part2 * max(0.0, min(C[k], X[i, k] + B[i, k] / 2.0) - max(0.0, X[i, k] - B[
#                 i, k] / 2.0))
#
#             Delta_bar_list_i.append(max(0.0, abs(X[i, k] - C[k] / 2.0) + (B[i, k] - C[k]) / 2.0))
#
#         Gamma_bar_i = Gamma_bar_part1 - Gamma_bar_part2
#         # print("composant {}: {}".format(i, Gamma_bar_i))
#         Delta_bar_i = max(Delta_bar_list_i)
#
#         part_protrusion = part_protrusion + Gamma_bar_i * Delta_bar_i
#         # print("Partie protrusion de composant {}: {}".format(i, Gamma_bar_i*Delta_bar_i))
#
#     return  part_protrusion

def compute_vide_ratio(X, B):
    """
    compute vide ratio, attention: this version haven't considered intersection between composants.
    :param X:
    :param B:
    :param C:
    :return:
    """
    m = X.shape[0]
    d = X.shape[1]
    V_intersection = 0
    V_total = 0

    for i in range(m - 1):
        for j in range(i + 1, m):
            Gamma_ij = 1
            for k in range(d):
                Gamma_ij = Gamma_ij * max(0.0, min(X[i, k] + B[i, k] / 2.0, X[j, k] + B[j, k] / 2.0) -
                                          max(X[i, k] - B[i, k] / 2.0, X[j, k] - B[j, k] / 2.0))

            V_intersection = V_intersection + Gamma_ij
    for i in range(m):
        V_one_composant = 1
        for k in range(d):
            V_one_composant = V_one_composant * B[i, k]
        V_total = V_total + V_one_composant

    # compute V_c
    B_c = np.max(X+B/2, axis = 0) - np.min(X-B/2, axis=0)
    V_c = 1
    for k in range(d):
        V_c = V_c*B_c[k]
    vide_ratio = 0
    vide_ratio = 1 - (V_total-V_intersection)/(V_c)
    return vide_ratio




def compute_contact_area(X, B, C, len_ignore=0.1):
    """
    fonction objectifs: compute all contact area between one composant and another,
    and add contact area between composants and lower surface of container.
    """
    X_copy = X.copy()
    B_copy = B.copy()

    ## suppose lower surface of container is a composant with height equal to 0
    X_copy = np.vstack((np.array([C[0] / 2, C[1] / 2, 0]), X_copy))
    B_copy = np.vstack((np.array([C[0], C[1], 0]), B_copy))
    m = X_copy.shape[0]
    d = X_copy.shape[1]
    contact_area_total = 0
    for i in range(m - 1):
        for j in range(i + 1, m):
            contact_area_length_list = []
            for k in range(d):
                contact_area_length = min(X_copy[i, k] + B_copy[i, k] / 2.0, X_copy[j, k] + B_copy[j, k] / 2.0) - \
                                          max(X_copy[i, k] - B_copy[i, k] / 2.0, X_copy[j, k] - B_copy[j, k] / 2.0)
                contact_area_length_list.append(contact_area_length)
            contact_area = 0
            if (np.all(np.array(contact_area_length_list) >= 0)):
                # if two composant contact

                contact_area_length_list = list(filter(lambda a: a >len_ignore, contact_area_length_list))
                # if any length less than len_ignore, we think that they not contact each other,
                if (len(contact_area_length_list) == 2):
                    contact_area = contact_area_length_list[0] * contact_area_length_list[1]
                contact_area_total += contact_area
    return contact_area_total


def get_overload_value(X, B, composants_weight, load_bearing_capacity, len_ignore=0.1):
    X_copy = X.copy()
    upper_surface_list = X_copy[:, 2] + B[:, 2] / 2.0
    z_set = set(upper_surface_list)
    placement_order = []
    while (len(z_set) != 0):
        lowest_z = min(z_set)
        idx = upper_surface_list == lowest_z
        msg_str = []
        for i, j in enumerate(list(idx)):
            if j == True:
                msg_str.append(i)
        #         print("lowest z: {}, Number of lowest composant: {}".format(lowest_z, msg_str))
        placement_order.extend(msg_str)
        z_set.remove(lowest_z)

    # print("Placement order: ", placement_order)

    placed_list = []
    # load_bearing_composants ids
    load_bearing_list = [None for i in range(len(placement_order))]
    # load_bearing area
    load_bearing_area_list = [None for i in range(len(placement_order))]

    for composant_i in placement_order:
        if len(placed_list) != 0:
            overlap_list = []
            overlap_area_list = []
            for composant_j in placed_list:
                ret, overlap_area, overlap_length, overlap_width = overlap2d(X_copy[composant_i], B[composant_i, 0], B[composant_i, 1],
                                              X_copy[composant_j], B[composant_j, 0], B[composant_j, 1])
                if (ret and overlap_length>len_ignore and overlap_width>len_ignore):
                    overlap_list.append(composant_j)
                    overlap_area_list.append(overlap_area)

            if len(overlap_list) != 0:
                highest_z = np.max(upper_surface_list[overlap_list])
                idx = upper_surface_list[overlap_list] == highest_z
                load_bearing_list[composant_i] = list(np.array(overlap_list)[idx])
                load_bearing_area_list[composant_i] = list(np.array(overlap_area_list)[idx])

        placed_list.append(composant_i)

    # print("load_bearing list: ", load_bearing_list)
    # print("load_bearing_area_list: ", load_bearing_area_list)
    load_bearing_weights = np.zeros((X_copy.shape[0]))
    placement_order.reverse()
    for composant_i in placement_order:
        # if exist one or more composants support composant i
        if load_bearing_list[composant_i] != None:
            for i, composant_j in enumerate(load_bearing_list[composant_i]):
                total_overlap_area = np.sum(np.array(load_bearing_area_list[composant_i]))
                # something like pressure
                load_bearing_weight_of_j = ( composants_weight[composant_i] + load_bearing_weights[composant_i]) * \
                                           (load_bearing_area_list[composant_i][i] / total_overlap_area)
                load_bearing_weights[composant_j] = load_bearing_weights[composant_j] + load_bearing_weight_of_j

    # print("load_bearing_weights: ", load_bearing_weights)
    # print("load_bearing_capacity: ", load_bearing_capacity)

    res = 0
    if np.any(load_bearing_weights > load_bearing_capacity):
        not_meet_requirements_idx = load_bearing_weights > load_bearing_capacity
        res = np.sum(load_bearing_weights[not_meet_requirements_idx] - load_bearing_capacity[not_meet_requirements_idx])
        return res
    else:
        return res
# def get_fitness(pred1, pred2):
#     """
#     On veut F le plus bas
#     :param pred:
#     :return:
#     """
#     pred = pred1
#     h = np.max(pred) - pred + 1e-4
#     return h/h.sum()

# def get_fitness(pred1, pred2):
#     """
#     On veut F le plus bas
#     :param pred:
#     :return:
#     """
#     pred = np.multiply(pred1, pred2)
#     h = np.max(pred) - pred + 1e-4
#     return h/h.sum()

def get_fitness(pred1, pred2):
    """
    On veut F le plus bas
    :param pred:
    :return:
    """
    h1 = np.max(pred1) - pred1
    h1 = h1 / h1.sum()
    # print(h1.sum())
    h2 = np.max(pred1) - pred1
    h2 = h2 / h2.sum()
    # print(h2.sum())
    return (h1 + h2) / 2.0


def select(pop, fitness):
    idx = np.random.choice(np.arange(pop.shape[0]), size=pop.shape[0],
                           replace=True, p=fitness)
    return pop[idx]


def crossover(parent, pop, cross_rate=0.8):
    if np.random.rand() < cross_rate:
        # si il y a des croissement avec individus idx
        idx = np.random.randint(0, pop.shape[0], size=1)
        cross_points = np.random.randint(0, 2, size=(pop.shape[1], pop.shape[2])).astype(np.bool)
        parent[cross_points] = pop[idx, cross_points]
    return parent


def mutate(child, mutation_rate=3e-4):
    for i in range(child.shape[0]):
        for j in range(child.shape[1]):
            if np.random.rand() < mutation_rate:
                child[i, j] = 1 - child[i, j]
    return child


def P2B(P_all, B, permutation_array):
    pop_size = P_all.shape[0]
    B_all = np.zeros((pop_size, B.shape[0], B.shape[1]))
    for i in range(pop_size):
        B_i = np.zeros_like(B)
        for j in range(B.shape[0]):
            B_i[j] = B[j, permutation_array[int(P_all[i][j])]]
        B_all[i] = B_i
    return B_all

def compute_scores(X_all, B_all, C, len_ignore=0.1):
    pop_size = X_all.shape[0]
    scores = np.zeros((pop_size, 2))
    for i in range(pop_size):
        B_i = B_all[i]
        X_i = X_all[i]

        X_i = addGravity(X_i, B_i, len_ignore=len_ignore)
        contact_area = -compute_contact_area(X_i,B_i,C, len_ignore=len_ignore)
        vide_values = compute_vide_ratio(X_i, B_i)
        scores[i, 0] = contact_area
        scores[i, 1] = vide_values
    return scores

def compute_F_values(X_all, B_all, C, len_ignore=0.1):
    pop_size = X_all.shape[0]
    F_values = np.zeros((pop_size))
    for i in range(pop_size):
        B_i = B_all[i]
        X_i = X_all[i]

        X_i = addGravity(X_i, B_i, len_ignore=len_ignore)
        F_value = F(X_i, B_i, C)
        F_values[i] = F_value
    return F_values

def compute_overloaded_values(X_all, B_all, composants_weight, load_bearing_capacity, len_ignore=0.1):
    pop_size = X_all.shape[0]
    overloaded_values = np.zeros((pop_size))
    for i in range(pop_size):
        B_i = B_all[i]
        X_i = X_all[i]
        X_i = addGravity(X_i, B_i, len_ignore=len_ignore)
        overloaded_value = get_overload_value(X_i, B_i, composants_weight, load_bearing_capacity, len_ignore=len_ignore)
        overloaded_values[i] = overloaded_value
    return overloaded_values

def identify_pareto(scores, pop_ids):
    """
    chercher les points situe au pareto front
    :param scores:
    :return: les index des points
    """
    pop_size = scores.shape[0]
    # on suppose tous les points sont dans pareto front
    belong_pareto_front = np.ones(pop_size, dtype=bool)
    for i in range(pop_size):
        for j in range(pop_size):
            if all(scores[j] <= scores[i]) and any(scores[j] < scores[i]):
                # si un point i n'est pas dans pareto front, on le supprime.
                belong_pareto_front[i] = 0
                break
    return pop_ids[belong_pareto_front]


def calculate_crowding(scores):

    pop_size = scores.shape[0]
    scores_dims = scores.shape[1]

    # create crowding matrix of population (row) and score (column)
    crowding_matrix = np.zeros((pop_size, scores_dims))

    # normalise scores (ptp is max-min)
    normed_scores = (scores - scores.min(0)) / (scores.ptp(0)+1e-6)

    # calculate crowding distance for each score in turn
    for col in range(scores_dims):
        crowding = np.zeros(pop_size)

        # end points have maximum crowding
        crowding[0] = 1
        crowding[pop_size - 1] = 1

        # Sort each score (to calculate crowding between adjacent scores)
        sorted_scores = np.sort(normed_scores[:, col])

        sorted_scores_index = np.argsort(
            normed_scores[:, col])

        # Calculate crowding distance for each individual
        crowding[1:pop_size - 1] = sorted_scores[2:pop_size] - sorted_scores[0:pop_size - 2]

        # resort to orginal order (two steps)
        re_sort_order = np.argsort(sorted_scores_index)
        sorted_crowding = crowding[re_sort_order]

        # Record crowding distances
        crowding_matrix[:, col] = sorted_crowding

    # Sum crowding distances of each score
    crowding_distances = np.sum(crowding_matrix, axis=1)

    return crowding_distances


def reduce_by_crowding(scores, number_to_select):
    """
    This function selects a number of solutions based on tournament of
    crowding distances. Two members of the population are picked at
    random. The one with the higher croding dostance is always picked
    """
    pop_ids = np.arange(scores.shape[0])
    crowding_distances = calculate_crowding(scores)
    picked_pop_ids = np.zeros((number_to_select))
    picked_scores = np.zeros((number_to_select, scores.shape[1]))

    for i in range(number_to_select):

        pop_size = pop_ids.shape[0]

        fighter1ID = np.random.randint(0, pop_size)
        fighter2ID = np.random.randint(0, pop_size)

        # If fighter # 1 is better
        if crowding_distances[fighter1ID] >= crowding_distances[
            fighter2ID]:

            # add solution to picked solutions array
            picked_pop_ids[i] = pop_ids[
                fighter1ID]

            # Add score to picked scores array
            picked_scores[i, :] = scores[fighter1ID, :]

            # remove selected solution from available solutions
            pop_ids = np.delete(pop_ids, (fighter1ID), axis=0)

            scores = np.delete(scores, (fighter1ID), axis=0)

            crowding_distances = np.delete(crowding_distances, (fighter1ID), axis=0)
        else:
            picked_pop_ids[i] = pop_ids[fighter2ID]

            picked_scores[i, :] = scores[fighter2ID, :]

            pop_ids = np.delete(pop_ids, (fighter2ID), axis=0)

            scores = np.delete(scores, (fighter2ID), axis=0)

            crowding_distances = np.delete(
                crowding_distances, (fighter2ID), axis=0)

    # Convert to integer
    picked_pop_ids = np.asarray(picked_pop_ids, dtype=int)

    return picked_pop_ids

def build_pareto_population(pop, scores, pop_size):
    """

    :param pop:
    :param scores:
    :return:
    """
    unselected_pop_ids = np.arange(pop.shape[0])
    all_pop_ids = np.arange(pop.shape[0])
    pareto_front_ids = np.array([])
    while len(pareto_front_ids) < pop_size:
        temp_pareto_front_ids = identify_pareto(scores[unselected_pop_ids],
                                                unselected_pop_ids)
        # check size of total pareto front if larger than pop_size, reduce new pareto front by crowding
        combined_pareto_size = len(pareto_front_ids) + len(temp_pareto_front_ids)
        # print("len_pareto_front_ids:{}, len_pareto_front_ids:{}".format(len(pareto_front_ids), len(temp_pareto_front_ids)))
        if combined_pareto_size > pop_size:
            number_to_select = pop_size - len(pareto_front_ids)
            selected_individus = reduce_by_crowding(scores[temp_pareto_front_ids], number_to_select)
            temp_pareto_front_ids = temp_pareto_front_ids[selected_individus]

        # Add latest pareto front to full Pareto front
        pareto_front_ids = np.hstack((pareto_front_ids, temp_pareto_front_ids))

        # Update unslected population ID by using sets to find IDs in all
        # ids that are not in the selected front
        unselected_set = set(all_pop_ids) - set(pareto_front_ids)
        unselected_pop_ids = np.array(list(unselected_set))

    # convert to integer
    pop = pop[pareto_front_ids.astype(int)]
    return pop, pareto_front_ids.astype(int)

def select_by_tournament(scores, number_to_select):
    """
    This function selects a number of solutions based on tournament
    select solutions with minor score
    score_array: 1d-array
    """
    pop_ids = np.arange(scores.shape[0])
    picked_pop_ids = np.zeros((number_to_select))
    picked_scores = np.zeros((number_to_select))

    for i in range(number_to_select):

        pop_size = pop_ids.shape[0]

        fighter1ID = np.random.randint(0, pop_size)
        fighter2ID = np.random.randint(0, pop_size)

        # If fighter # 1 is better
        if scores[fighter1ID] <= scores[fighter2ID]:

            # add solution to picked solutions array
            picked_pop_ids[i] = pop_ids[fighter1ID]

            # Add score to picked scores array
            picked_scores[i] = scores[fighter1ID]

            # remove selected solution from available solutions
            pop_ids = np.delete(pop_ids, (fighter1ID), axis=0)

            scores = np.delete(scores, (fighter1ID), axis=0)

        else:
            picked_pop_ids[i] = pop_ids[fighter2ID]

            picked_scores[i] = scores[fighter2ID]

            pop_ids = np.delete(pop_ids, (fighter2ID), axis=0)

            scores = np.delete(scores, (fighter2ID), axis=0)


    # Convert to integer
    picked_pop_ids = np.asarray(picked_pop_ids, dtype=int)

    return picked_pop_ids


def create_new_generation(pop, iteration, no_less_than=400, no_more_than=500, mutation_rate=None):
    while(pop.shape[0]<no_less_than):
        pop_copy = pop.copy()
        for parent in pop:
            # croissement entre les parents
            child = crossover(parent, pop)
            pop_copy = np.append(pop_copy, np.expand_dims(child, axis=0), axis=0)
        # supprimer les individus qui sont pareils. maintenant pop_size peut etre n'est pas pop_size
        pop = np.unique(pop_copy, axis=0)

    if pop.shape[0]>no_more_than:
        pop = pop[:no_more_than]
    shuffle_idx = np.arange(pop.shape[0])
    np.random.shuffle(shuffle_idx)
    pop = pop[shuffle_idx]
    # mutation
    # iteration = iteration % 400
    for child in pop:
        if mutation_rate==None:
            denominateur = int(iteration/100)
            if denominateur == 0:
                denominateur = 1
            else:
                denominateur = denominateur*10
            child[:] = mutate(child, mutation_rate=1e-3 / denominateur)
        else:
            child[:] = mutate(child, mutation_rate=mutation_rate)
    return pop


def select_by_constraints(pop, parent_iteration,  C, permutation_array, constraint_F=1e-3, len_ignore=0.1):
    _sub_iter = 0
    while(True):
        print("\n")
        print(">>> [constraint test] sub_iteration: ", _sub_iter)
        pop = create_new_generation(pop, parent_iteration, no_less_than=300, no_more_than=500)
        POP_SIZE = pop.shape[0]
        X_all, P_all = translateDNA(pop, C[0], C[1], C[2])
        B_all = P2B(P_all, B, permutation_array)
        overloaded_values = compute_overloaded_values(X_all, B_all, composants_weight, composants_load_bearing_capacity)
        F_values = compute_F_values(X_all, B_all, C, len_ignore=len_ignore)

        meet_requirements_ids = np.logical_and(overloaded_values == 0, F_values<constraint_F)
        pop_meet_requireemnts = pop[meet_requirements_ids]
        num_meet_requirements = np.sum(meet_requirements_ids.astype(int))
        print("num_meet_load bearing requirements = ", num_meet_requirements)
        if (num_meet_requirements < 250):
            not_meet_requirements_ids = np.logical_not(meet_requirements_ids)
            overloaded_values_not_meet_requirements = overloaded_values[not_meet_requirements_ids]
            F_values_not_meet_requirements = F_values[not_meet_requirements_ids]
            pop_not_meet_requirements = pop[not_meet_requirements_ids]
            scores = np.zeros((POP_SIZE-num_meet_requirements, 2))
            scores[:, 0] = overloaded_values_not_meet_requirements
            scores[:, 1] = F_values_not_meet_requirements

            print(scores[:10])

            pop_selected, next_generation_ids = build_pareto_population(pop_not_meet_requirements, scores, 250-num_meet_requirements)
            pop = np.append(pop_meet_requireemnts, pop_selected, axis=0)
        else:
            pop = pop_meet_requireemnts
            break

        _sub_iter = _sub_iter + 1

    return pop



if __name__ == '__main__':

    camion_largeur = 12;
    camion_longeur = 12;
    camion_hauteur = 12;
    camion = Camion.atOrigine(camion_largeur, camion_largeur, camion_hauteur);

    B = np.load("B_15_items.npy")
    np.save("B.npy", B)

    composants_weight = np.array([8, 8, 4, 4, 3, 6, 4, 1, 2, 4, 6, 9, 6, 10, 10]).astype(float)
    composants_load_bearing_capacity = np.array([25, 15, 7.5, 1, 10,
                                               3, 20, 11, 3, 20,
                                               32, 38, 7, 8, 12]).astype(float)
    np.save(composants_weight)


    nb_composant = B.shape[0]
    print("B = \n{}".format(B))

    V_total = 0
    for i in range(B.shape[0]):
        V_i = 1
        for k in range(B.shape[1]):
            V_i = V_i * B[i, k]
        V_total = V_total + V_i
    print("le volume des tous composants:")
    print(V_total)
    permutation_list = list(itertools.permutations([0, 1, 2]))
    permutation_array = np.asarray(permutation_list, dtype=np.int)
    C = np.array([camion_longeur, camion_largeur, camion_hauteur]).astype(np.float)
    np.save("C.npy", C)


    LEN_IGNORE = 0.2
    ## creer la population initiale
    # 用基因对composant的位置信息，朝向信息进行编码。

    POP_SIZE = 200
    # on utlise un dna qui a un longeur 10 pour exprimer un x,
    # on effet, pour exprimer (x,y,z) , il faut un dna avec longeur 30
    # le dernier dna qui a longeur 10 est pour exprimer l'orientation
    DNA_SIZE = 10

    # initialializer la population
    pop = np.random.randint(2, size=(POP_SIZE, nb_composant, DNA_SIZE * 4))
    # afficher tous les valeurs d'une matrice
    np.set_printoptions(threshold=sys.maxsize)

    fig = plt.figure(figsize=(10,10))
    ax1 = fig.add_subplot(221, projection="3d")
    ax2 = fig.add_subplot(222)

    ax3 = fig.add_subplot(223)


    ax4 = fig.add_subplot(224)


    ax1.set_aspect(1)
    contact_area_min_values = []
    vide_idx_values = []

    iterations = 5000

    boite_color_list = []
    for i in range(nb_composant):
        boite_color_list.append(random_mpl_color_rgba())
    for _iter in range(iterations):
        # pop = create_new_generation(pop, _iter, no_less_than=300, no_more_than=800)
        # pop = select_by_overloaded_values(pop, _iter, C, permutation_array)
        #
        # pop = select_by_F_values(pop, _iter, C, permutation_array, len_ignore=LEN_IGNORE)
        pop = select_by_constraints(pop, _iter, C, permutation_array, len_ignore=LEN_IGNORE)
        print("pop shape: ", pop.shape)


        X_all, P_all = translateDNA(pop, C[0], C[1], C[2])
        B_all = P2B(P_all, B, permutation_array)
        F_values = compute_F_values(X_all, B_all, C, len_ignore=LEN_IGNORE)
        # overloaded_values = compute_overloaded_values(X_all, B_all, composants_weight, composants_load_bearing_capacity)
        # print(overloaded_values)
        # break


        scores = compute_scores(X_all, B_all, C, len_ignore=LEN_IGNORE)
        print(scores.shape)
        pop, next_generation_ids = build_pareto_population(pop, scores, POP_SIZE)
        if _iter % 100 == 0 and _iter !=0:
            np.save("pop_{:0>5d}.npy".format(_iter),pop)


        X_all = X_all[next_generation_ids]
        B_all = B_all[next_generation_ids]
        scores = scores[next_generation_ids]
        F_values = F_values[next_generation_ids]

        individus_at_1st_pareto_idx = identify_pareto(scores, np.arange(POP_SIZE))
        scores_at_1st_pareto = scores[individus_at_1st_pareto_idx]
        # print(individus_at_1st_pareto_idx)
        sorted_scores = scores_at_1st_pareto[np.argsort(scores_at_1st_pareto[:,1])]
        # print(scores_at_1st_pareto)
        # print(np.argmin(scores_at_1st_pareto[:,1], axis=0))
        # print(individus_at_1st_pareto_idx)
        # solution_chosen = int(np.random.choice(individus_at_1st_pareto_idx, 1))
        solution_chosen = individus_at_1st_pareto_idx[np.argmin(scores_at_1st_pareto[:,1], axis=0)]
        # print(solution_chosen)

        X = X_all[solution_chosen]
        B_idx = B_all[solution_chosen]
        X = addGravity(X,B_idx, len_ignore=LEN_IGNORE)
        score_chosen = scores[solution_chosen]
        F_value_chosen = F_values[solution_chosen]
        contact_area_min_values.append(score_chosen[0])
        vide_idx_values.append(score_chosen[1])
        # on choisit le plus moins compact mais respect meilleur les contraintes non-chevauchement

        print("iteration {} - contact_area_values: {}; Vide ratio: {};".format(_iter, score_chosen[0], score_chosen[1]))
        print("F_values: ", F_values[:10])

        ax1.cla()
        ax2.cla()
        ax3.cla()
        if _iter % 10 ==0:
            ax4.cla()
        ax1.set_xlabel("X")
        ax1.set_ylabel("Y")
        ax1.set_zlabel("Z")
        ax1.set_xlim(-0, camion_largeur)
        ax1.set_ylim(-0, camion_longeur)
        ax1.set_zlim(-0, camion_hauteur)
        ax1.grid(False)
        ax1.add_collection3d(camion.getPoly3d(facecolors=(1, 1, 1, 0), edgecolors="black"))
        boite_list = []
        for i in range(X.shape[0]):
            boite = Boite((X[i, 0], X[i, 1], X[i, 2]), B_idx[i, 0], B_idx[i, 1], B_idx[i, 2])
            boite_list.append(boite)
            ax1.add_collection3d(boite.getPoly3d(facecolors=boite_color_list[i]))
            ax1.text(*X[i, :], str(i), 'x')

        text_str = "Contact_area: {:.4f}\nVide ratio:{:.4f}\nF_value:{:.4f}".format(score_chosen[0], score_chosen[1], F_value_chosen)
        ax1.text2D(0.05, 0.95, text_str, transform=ax1.transAxes)

        ax2.set_xlabel("Iterations")
        ax2.set_ylabel("contact area values")
        ax2.plot(np.arange(len(contact_area_min_values)), contact_area_min_values, 'b-', linewidth=1)
        ax3.set_xlabel("Iterations")
        ax3.set_ylabel("vide ratio values")
        ax3.plot(np.arange(len(contact_area_min_values)), vide_idx_values, 'b-', linewidth=1)

        # plot
        ax4.set_xlabel("vide ratio")
        ax4.set_ylabel("contact area value")
        ax4.scatter(scores[:, 1], scores[:, 0], color=random_mpl_color_rgba())
        # ax4.scatter(scores[individus_at_1st_pareto_idx, 1], scores[individus_at_1st_pareto_idx, 0], marker="^", color=random_mpl_color_rgb())
        ax4.plot(sorted_scores[:,1], sorted_scores[:,0])
        plt.pause(0.1)
        fig.savefig("chart.png")














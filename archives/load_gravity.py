import os
import re
import numpy as np
import itertools
import math
import matplotlib

matplotlib.use('TkAgg')
import matplotlib.pyplot as plt
import contraintes_poids_test as colis


def load_population(rootdir="./", npy_file=None):
    """

    :param rootdir: parent folder of npy file
    :param file: npy file that we want load, if not set, it will load last pop_{05d}.npy file
    :return:
    """
    if npy_file != None:
        pop_file_path = os.path.join(rootdir, npy_file)
        pop = np.load(pop_file_path)
    else:
        list = os.listdir(rootdir)
        num_list = []
        npy_list = []
        for item in list:
            if re.match(r"pop_\d{5}.npy", item) != None:
                num_str = re.sub(r'.npy', "", item)
                num_str = re.sub(r'pop_', "", num_str)
                num_list.append(int(num_str))
                npy_list.append(item)

        if len(num_list) != 0:
            idx = np.argmax(np.array(num_list))
            pop_file = npy_list[idx]
            pop_file_path = os.path.join(rootdir, pop_file)
            pop = np.load(pop_file_path)
        else:
            raise Exception("No population numpy file in folder {}".format(os.path.abspath(rootdir)))

    return pop

def overlap2d(center_1, length_1, width_1, center_2, length_2, width_2):
    overlap_length = max(0, min(center_1[0] + length_1/2, center_2[0]+length_2/2) -
                        max(center_1[0] - length_1/2, center_2[0] - length_2/2))
    overlap_width = max(0, min(center_1[1] + width_1/2, center_2[1]+width_2/2) -
                        max(center_1[1] - width_1/2, center_2[1] - width_2/2))
    overlap_area = overlap_length * overlap_width
    ret = False
    if overlap_area != 0:
        ret = True
    return ret, overlap_area

def addGravity(X, B):
    X_copy = X.copy()
    z_set = set(X_copy[:, 2])
    placement_order = []
    while (len(z_set) != 0):
        lowest_z = min(z_set)
        idx = X_copy[:, 2] == lowest_z
        msg_str = []
        for i, j in enumerate(list(idx)):
            if j == True:
                msg_str.append(i)
        # print("lowest z: {}, Number of lowest composant: {}".format(lowest_z, msg_str))
        placement_order.extend(msg_str)
        z_set.remove(lowest_z)

    # print("Placement order: ", placement_order)

    placed_list = []
    for composant_i in placement_order:
        # print("considering composant i >>> {} ...".format(composant_i))
        # print(placed_list)
        z_last_composant = 0
        if len(placed_list) != 0:
            overlap_list = []
            for composant_j in placed_list:
                # print("--composant j >>> {}".format(composant_j))
                ret, _ = overlap2d(X_copy[composant_i], B[composant_i, 0], B[composant_i, 1],
                                   X_copy[composant_j], B[composant_j, 0], B[composant_j, 1])
                if (ret):
                    overlap_list.append(composant_j)
            # print("overlap_list: {}".format(overlap_list))
            if len(overlap_list) == 0:
                z_last_composant = 0
            else:
                z_last_composant = np.max(X_copy[overlap_list, 2] + B[overlap_list, 2] / 2)
        z_i = B[composant_i, 2] / 2 + z_last_composant
        # print("z_i: ", z_i)
        X_copy[composant_i, 2] = z_i

        placed_list.append(composant_i)
    return X_copy


if __name__ == '__main__':
    B = np.load("B.npy")
    C = np.load("C.npy")

    camion = colis.Camion.atOrigine(C[0], C[1], C[2]);
    pop = load_population()
    pop_size = pop.shape[0]
    nb_composant = pop.shape[1]
    permutation_list = list(itertools.permutations([0, 1, 2]))
    permutation_array = np.asarray(permutation_list, dtype=np.int)
    X_all, P_all = colis.translateDNA(pop, C[0], C[1], C[2])

    B_all = colis.P2B(P_all, B, permutation_array)
    scores = colis.compute_scores(X_all, B_all, C)
    F_values = colis.compute_F_values(X_all, B_all, C)



    individus_at_1st_pareto_idx = colis.identify_pareto(scores, np.arange(pop_size))
    scores_at_1st_pareto = scores[individus_at_1st_pareto_idx]
    sorted_scores = scores_at_1st_pareto[np.argsort(scores_at_1st_pareto[:, 1])]
    print(individus_at_1st_pareto_idx)
    if individus_at_1st_pareto_idx.shape[0] > 10:
        individus_at_1st_pareto_idx = colis.reduce_by_crowding(scores_at_1st_pareto, 5)
        scores_at_1st_pareto = scores[individus_at_1st_pareto_idx]

    print(individus_at_1st_pareto_idx)
    num_items = individus_at_1st_pareto_idx.shape[0]
    fig_row = int(math.sqrt(num_items + 1))
    fig_cols = int((num_items + 1) / fig_row) + 1

    fig = plt.figure(figsize=(10 / fig_row * fig_cols, 10))

    boite_color_list = []
    for i in range(nb_composant):
        boite_color_list.append(colis.random_mpl_color_rgba())
    for item_idx in range(num_items):
        ax_idx = fig.add_subplot(fig_row, fig_cols, item_idx + 1, projection="3d")
        ax_idx.set_aspect(1)
        ax_idx.set_xlabel("X")
        ax_idx.set_ylabel("Y")
        ax_idx.set_zlabel("Z")
        ax_idx.set_xlim(-0, C[0])
        ax_idx.set_ylim(-0, C[1])
        ax_idx.set_zlim(-0, C[2])
        ax_idx.grid(False)
        ax_idx.add_collection3d(camion.getPoly3d(facecolors=(1, 1, 1, 0), edgecolors="black"))
        idx = individus_at_1st_pareto_idx[item_idx]
        X = X_all[idx]
        B_idx = B_all[idx]
        X = addGravity(X, B_idx)
        scores_idx = scores[idx]
        F_value_idx = F_values[idx]
        text_str = "Contact area: {:.4f}\n Vide ratio:{:.4f}\nF_value:{:.4f}".format(scores_idx[0], scores_idx[1], F_value_idx)
        ax_idx.text2D(0.05, 0.95, text_str, transform=ax_idx.transAxes)
        boite_list = []
        for i in range(X.shape[0]):
            boite = colis.Boite((X[i, 0], X[i, 1], X[i, 2]), B_idx[i, 0], B_idx[i, 1], B_idx[i, 2])
            boite_list.append(boite)
            ax_idx.add_collection3d(boite.getPoly3d(facecolors=boite_color_list[i]))
            ax_idx.text(*X[i, :], str(i), 'x')

    ax_idx = fig.add_subplot(fig_row, fig_cols, fig_row * fig_cols)
    ax_idx.set_xlabel("vide ratio")
    ax_idx.set_ylabel("Contact area")
    ax_idx.scatter(scores[:, 1], scores[:, 0], color=colis.random_mpl_color_rgba())
    ax_idx.plot(sorted_scores[:, 1], sorted_scores[:, 0])
    plt.show()
    fig.savefig("resultat.png")
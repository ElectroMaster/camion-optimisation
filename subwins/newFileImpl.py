import sys
import PyQt5.QtWidgets as PyQtWs
import PyQt5.QtGui as PyQtGui
import PyQt5.QtCore as PyQtCore
import subwins.mplCanvas as mplCanvas
import forme
import numpy as np
import pandas as pd
import utils.csvTools as csvTools
import qtawesome as qta

EN = PyQtCore.QLocale(PyQtCore.QLocale.English, PyQtCore.QLocale.UnitedStates)
# refresh figure every #ms
REFRESHRATE = 1000

btnSS1 = """
        QPushButton{
        font-family: 'Open Sans', 'sans-serif';
        font-szie: 22px;
            border-radius: 3px;
            padding: 7px 12px;
            text-decoration: none;
            color: #fff;
        
        
            background-color: #55acee;
            box-shadow: 0px 5px 0px 0px #3C93D5;
        }
        QPushButton::Hover{
            background-color: #6FC6FF;
        }
        
        QPushButton::pressed{
            box-shadow: 0px 1px 0px 0px;
        }
        """
lineEditSS1 = """
            QLineEdit{
                padding: 5px 10px;
                font-size:14px;
                font-family: Arial;
                border: 2px solid #1abc9c;
                border-left-width: 0;
                border-right-width:0;
                border-top-width:0;
                

            }    
                
            """
labelSS1 = """
            QLabel {
                font-size:14px;
                font-family:Arial;
            }
            """

comboBoxSS1 = """
QComboBox {
            border-radius: 3px;
            padding: 7px 12px;
            text-decoration: none;
            color: #fff;
        
        
            background-color:#9b59b6;
            box-shadow: 0px 5px 0px 0px #3C93D5;
}
QComboBox:on { /* shift the text when the popup opens */
    background-color: #eed1ff;
}


    QComboBox::drop-down {
    border-radius: 3px;
    width:40px;
    }
    QComboBox::down-arrow {
        image: url(materials/icons/down-arrow.png);
        width:16px;
    }

    QComboBox QAbstractItemView {
        color: black;
        border: none;
        background-color:#fff;
        selection-background-color: #9b59b6;
        selection-color:#fff;
    }


            """

class DemonstrateCanvas(mplCanvas.MplCanvas3d):
    def __init__(self, parent=None, width=10, height=10, dpi=100, itemForme=None):
        self.itemForme = itemForme
        mplCanvas.MplCanvas3d.__init__(self, parent=parent, width=width, height=height, dpi=dpi)
        timer = PyQtCore.QTimer(self)
        timer.timeout.connect(self.update_figure)
        timer.start(REFRESHRATE)
    """Simple canvas with a sine plot."""
    def axesSetting(self):
        self.axes.set_aspect(1)
        self.axes.set_xlabel("X")
        self.axes.set_ylabel("Y")
        self.axes.set_zlabel("Z")
        self.axes.set_xlim(-4, 4)
        self.axes.set_ylim(-4, 4)
        self.axes.set_zlim(-4, 4)
        self.axes.grid(False)
        self.axes.set_axis_off()
    def compute_initial_figure(self):
        self.facecolors = forme.random_mpl_color_rgba()
        pass

    def update_figure(self):
        self.axes.cla()
        self.axesSetting()
        if self.itemForme == "cylinder":
            cylinder = forme.Cylinder((0, 0, 0), 3, 3, 5, N=50)
            cylinder.drawCylinderOnAxes(self.axes, facecolors=self.facecolors)

            self.axes.plot3D([-1.5, 1.5], [0, 0], [2.5, 2.5], label="diameter_x", linewidth=2)
            self.axes.plot3D([0, 0], [-1.5, 1.5], [2.5, 2.5], label="diameter_y", linewidth=2)
            self.axes.plot3D([0, 0], [0, 0], [-2.5, 2.5], label="length", linewidth=2)
            cylinder.drawSurfaceIndexOnAxes(self.axes)
            self.axes.legend(loc="upper right")

        elif self.itemForme == "boite":
            boite = forme.Boite((0,0,0), 3,4,5)
            self.axes.add_collection3d(boite.getPoly3d(facecolors=self.facecolors))
            self.axes.plot3D([-1.7, 1.5], [-2.2, -2.2], [-2.5, -2.5], label="length", linewidth=2)
            self.axes.plot3D([-1.7, -1.7], [-2.2, 1.5], [-2.5, -2.5], label="width", linewidth=2)
            self.axes.plot3D([-1.7, -1.7], [-2.2, -2.2], [-2.5, 2.5], label="height", linewidth=2)
            boite.drawSurfaceIndexOnAxes(self.axes)
            self.axes.legend(loc="upper right")

        elif self.itemForme == "lforme":
            lobject = forme.LForme((0, 0, 0), 3, 4, 5, 2, 4)
            self.axes.add_collection3d(lobject.getPoly3d(facecolors=self.facecolors))
            self.axes.plot3D([-1.7, 1.5], [-2.2, -2.2], [-2.5, -2.5], label="length", linewidth=2)
            self.axes.plot3D([-1.7, -1.7], [-2.2, 1.5], [-2.5, -2.5], label="width", linewidth=2)
            self.axes.plot3D([-1.7, -1.7], [-2.2, -2.2], [-2.5, 2.5], label="height", linewidth=2)
            self.axes.plot3D([-0.4, 1.5], [2.2, 2.2], [-1.5, -1.5], label="inner length", linewidth=2)
            self.axes.plot3D([-0.4, -0.4], [2.2, 2.2], [-1.5, 2.5], label="inner height", linewidth=2)
            lobject.drawSurfaceIndexOnAxes(self.axes)
            self.axes.legend(loc="upper right")

        self.draw() # <---- do not forget draw
    def updateItemForme(self, itemForme):
        # with help of updatefigure(), we can directly change item name and no need to recreate a new canvas
        # when adding a new item, facecolors can change :)
        self.facecolors = forme.random_mpl_color_rgba()
        self.itemForme = itemForme.lower()



class CreatedItemCanvas(mplCanvas.MplCanvas3d):
    def __init__(self, parent=None, width=10, height=10, dpi=100, input_dict=None):
        self.input_dict = input_dict
        mplCanvas.MplCanvas3d.__init__(self, parent=parent, width=width, height=height, dpi=dpi)
        timer = PyQtCore.QTimer(self)
        timer.timeout.connect(self.update_figure)
        timer.start(REFRESHRATE)
    """Simple canvas with a sine plot."""
    def axesSetting(self):
        self.axes.set_aspect(1)
        self.axes.set_xlabel("X")
        self.axes.set_ylabel("Y")
        self.axes.set_zlabel("Z")
        if self.input_dict ==None:
            self.axes.set_xlim(-4, 4)
            self.axes.set_ylim(-4, 4)
            self.axes.set_zlim(-4, 4)
        else:
            longest_len = max([self.input_dict["Param1"], self.input_dict["Param2"],
                               self.input_dict["Param3"], self.input_dict["Param4"],
                               self.input_dict["Param5"]])
            self.axes.set_xlim(-longest_len/2.0, longest_len/2.0)
            self.axes.set_ylim(-longest_len/2.0, longest_len/2.0)
            self.axes.set_zlim(-longest_len/2.0, longest_len/2.0)
        self.axes.grid(False)
        # self.axes.set_axis_off()
    def compute_initial_figure(self):
        self.facecolors = forme.random_mpl_color_rgba()
        pass

    def update_figure(self):
        self.axes.cla()
        self.axesSetting()
        if self.input_dict == None:
            pass
        else:
            if self.input_dict["Item_Geometry"] == "cylinder":
                cylinder = forme.Cylinder((0, 0, 0), 
                                            self.input_dict["Param1"], 
                                            self.input_dict["Param2"], 
                                            self.input_dict["Param3"], N=50)
                cylinder.drawCylinderOnAxes(self.axes, facecolors=self.facecolors)
                cylinder.drawSurfaceIndexOnAxes(self.axes)
    

    
            elif self.input_dict["Item_Geometry"] == "boite":
                boite = forme.Boite((0,0,0),
                                    self.input_dict["Param1"],
                                    self.input_dict["Param2"],
                                    self.input_dict["Param3"])
                self.axes.add_collection3d(boite.getPoly3d(facecolors=self.facecolors))
                boite.drawSurfaceIndexOnAxes(self.axes)

    
            elif self.input_dict["Item_Geometry"] == "lforme":
                lobject = forme.LForme((0, 0, 0),
                                       self.input_dict["Param1"],
                                       self.input_dict["Param2"],
                                       self.input_dict["Param3"],
                                       self.input_dict["Param4"],
                                       self.input_dict["Param5"])
                self.axes.add_collection3d(lobject.getPoly3d(facecolors=self.facecolors))
                lobject.drawSurfaceIndexOnAxes(self.axes)


        self.draw() # <---- do not forget draw
    def updateByUserInput(self, input_dict):
        self.input_dict = input_dict

















class EditItemWidget(PyQtWs.QWidget):
    def __init__(self, index, readonlyFlag, signalEnsemble, parent=None):
        super().__init__(parent)
        self.readonlyFlag = readonlyFlag
        self.index = index
        self.csv_dict = csvTools.readFromTempsCsv()
        self.geometry_choice_list = ["boite", "lforme", "cylinder"]
        self.item_num = len(self.csv_dict["Item_Geometry"])
        if self.index == -1 or self.index >= self.item_num:
            # list out of range, it means we create a new item
            self.create_flag = True
        else:
            self.create_flag = False

        if self.create_flag:
            self.item_geometry = "boite"
        else:
            self.item_geometry = self.csv_dict["Item_Geometry"][self.index]

        self.signalEnsemble = signalEnsemble
        self.initUI()
        # get user input every 1s, then update figure
        timer = PyQtCore.QTimer(self)
        timer.timeout.connect(self.updateCreatedItem)
        timer.start(1000)


    def initUI(self):
        self.vboxl = PyQtWs.QVBoxLayout()
        self.addLMRWigets()
        self.setLayout(self.vboxl)

    def addLMRWigets(self):
        """
        add left-middle-right widgets
        :return:
        """
        backIcon = qta.icon("fa5s.chevron-left", color="white")
        backButton = PyQtWs.QPushButton(backIcon, "Back", self)
        backButton.setIconSize(PyQtCore.QSize(16,16))
        # backButton.setFixedWidth(80)
        # backButton.setFixedHeight(20)
        backButton.clicked.connect(self.back2NewFileWin)
        backButton.setStyleSheet(btnSS1)
        # self.vboxl.addWidget(backButton)
        hboxl = PyQtWs.QHBoxLayout()



        leftWidget = PyQtWs.QWidget()
        leftWidget.setFixedWidth(300)
        leftWidget.setFixedHeight(400)
        vl = PyQtWs.QVBoxLayout()
        exampleTitle = PyQtWs.QLabel("<p style='font-size: 20px;font-family:Arial;'><span>Item Exemple</span></p>")
        exampleTitle.setFixedHeight(50)
        self.ex_canvas = DemonstrateCanvas()
        self.ex_canvas.updateItemForme(self.item_geometry)
        vl.addWidget(exampleTitle, 0, PyQtCore.Qt.AlignCenter)
        vl.addWidget(self.ex_canvas)

        leftWidget.setLayout(vl)
        
        
        rightWidget = PyQtWs.QWidget()
        rightWidget.setFixedWidth(300)
        rightWidget.setFixedHeight(400)
        vl = PyQtWs.QVBoxLayout()
        exampleTitle = PyQtWs.QLabel("<p style='font-size: 20px; font-family:Arial'><span>Created Item Preview</span></p>")
        exampleTitle.setFixedHeight(50)
        self.createdItem_canvas = CreatedItemCanvas()
        vl.addWidget(exampleTitle, 0, PyQtCore.Qt.AlignCenter)
        vl.addWidget(self.createdItem_canvas)

        rightWidget.setLayout(vl)
        

        self.middleWidget = self.getInitialMiddleWidget()
        hboxl.addWidget(leftWidget)
        hboxl.addWidget(self.middleWidget)
        hboxl.addWidget(rightWidget)
        self.vboxl.addLayout(hboxl)
        


    def getInitialMiddleWidget(self):

        middleWidget = PyQtWs.QWidget()
        middleWidget.setFixedWidth(400)
        tempWidget = PyQtWs.QWidget()
        tempWidget.setFixedHeight(50)
        vboxl = PyQtWs.QVBoxLayout()
        hboxl = PyQtWs.QHBoxLayout()
        formeLabel= PyQtWs.QLabel("Forme:")
        formeLabel.setStyleSheet(labelSS1)
        self.formeCombo = PyQtWs.QComboBox()
        self.formeCombo.addItems(self.geometry_choice_list)
        self.formeCombo.setCurrentIndex(self.geometry_choice_list.index(self.item_geometry))
        self.formeCombo.currentIndexChanged.connect(lambda: self.updateMiddleWidgetLayout(self.formeCombo.currentText()))
        self.formeCombo.setStyleSheet(comboBoxSS1)
        hboxl.addWidget(formeLabel, 0 , PyQtCore.Qt.AlignLeft)
        hboxl.addWidget(self.formeCombo, 1, PyQtCore.Qt.AlignRight)
        tempWidget.setLayout(hboxl)
        vboxl.addWidget(tempWidget)
        self.inputWidget = self.getInputWidget()
        vboxl.addWidget(self.inputWidget)
        middleWidget.setLayout(vboxl)
        self.middleWidgetLayout = vboxl
        # middleWidget.setFixedHeight(self.middleWidgetheight)
        return middleWidget


    def getInputWidget(self):
        inputWidget = PyQtWs.QWidget()
        boiteLabelList = ["Enterprise", "Length", "Width", "Height", "Weight", "Load Bearing Capacity", "Bottom Surface No."]
        cylinderLabelList = ["Enterprise", "Diameter_x", "Diameter_y", "Length", "Weight", "Load Bearing Capacity", "Bottom Surface No."]
        lformeLabelList = ["Enterprise", "Length", "Width", "Height", "Inner Length", "Inner Height", "Weight", "Load Bearing Capacity", "Bottom Surface No."]
        if self.item_geometry == "boite":
            labellist = boiteLabelList
            intValidator = PyQtGui.QIntValidator(-1, 5)
        elif self.item_geometry == "cylinder":
            labellist = cylinderLabelList
            intValidator = PyQtGui.QIntValidator(-1, 1)
        elif self.item_geometry == "lforme":
            labellist = lformeLabelList
            intValidator = PyQtGui.QIntValidator(-1, 7)

        label_count = len(labellist)
        self.lineEditDict = {l : PyQtWs.QLineEdit() for l in labellist}
        self.middleWidgetheight = (len(labellist)+1)*50
        vboxl = PyQtWs.QVBoxLayout()
        len_list = len(labellist)
        for i, label in  enumerate(labellist):
            hboxl = PyQtWs.QHBoxLayout()
            if i== len_list -1:
                qlabel = PyQtWs.QLabel("{}: \n(-1 means no such requirement)".format(label))
            else:
                qlabel = PyQtWs.QLabel(label+":")
            qlabel.setStyleSheet(labelSS1)
            hboxl.addWidget(qlabel, 0, PyQtCore.Qt.AlignLeft)
            self.lineEditDict[label].setFixedWidth(150)
            floatValidator = PyQtGui.QDoubleValidator(0, 100000, 5)
            floatValidator.setLocale(EN)
            intValidator.setLocale(EN)
            if i != 0 and i != len_list-1:
                self.lineEditDict[label].setValidator(floatValidator)
            elif i== len_list -1:
                self.lineEditDict[label].setValidator(intValidator)

            self.lineEditDict[label].setStyleSheet(lineEditSS1)
            hboxl.addWidget(self.lineEditDict[label], 1, PyQtCore.Qt.AlignRight)
            vboxl.addLayout(hboxl)

        if not self.readonlyFlag:
            hboxl = PyQtWs.QHBoxLayout()
            addButton = PyQtWs.QPushButton("Add")
            addButton.setFixedWidth(80)
            addButton.clicked.connect(self.addAct)
            addButton.setStyleSheet(btnSS1)
            hboxl.addWidget(addButton, 2, PyQtCore.Qt.AlignRight)
            cancelButton = PyQtWs.QPushButton("Cancel")
            cancelButton.setFixedWidth(80)
            cancelButton.clicked.connect(self.cancelAct)
            cancelButton.setStyleSheet(btnSS1)
            hboxl.addWidget(cancelButton, 0, PyQtCore.Qt.AlignRight)
            vboxl.addLayout(hboxl)

        inputWidget.setLayout(vboxl)

        self.setInitialInputWidget()
        return inputWidget


    def setInitialInputWidget(self):
        csv_dict = self.csv_dict
        if self.create_flag:
            self.lineEditDict["Enterprise"].setText("Default")
            self.lineEditDict["Weight"].setText(str(0))
            self.lineEditDict["Load Bearing Capacity"].setText(str(0))
            self.lineEditDict["Bottom Surface No."].setText(str(-1))

            if self.item_geometry == "boite":
                self.lineEditDict["Length"].setText(str(3))
                self.lineEditDict["Width"].setText(str(4))
                self.lineEditDict["Height"].setText(str(5))
            if self.item_geometry == "cylinder":
                self.lineEditDict["Diameter_x"].setText(str(2))
                self.lineEditDict["Diameter_y"].setText(str(2))
                self.lineEditDict["Length"].setText(str(4))
            if self.item_geometry == "lforme":
                self.lineEditDict["Length"].setText(str(3))
                self.lineEditDict["Width"].setText(str(4))
                self.lineEditDict["Height"].setText(str(5))

                self.lineEditDict["Inner Length"].setText(str(2))
                self.lineEditDict["Inner Height"].setText( str(4))
        else:
            self.lineEditDict["Enterprise"].setText(
                str(csv_dict["Enterprise"][self.index])
            )
            self.lineEditDict["Weight"].setText(
                str(csv_dict["Weight"][self.index])
            )
            self.lineEditDict["Load Bearing Capacity"].setText(
                str(csv_dict["Load_bearing_capacity"][self.index])
            )
            self.lineEditDict["Bottom Surface No."].setText(
                str(csv_dict["Bottom_surface_No"][self.index])
            )

            if self.item_geometry == "boite":
                self.lineEditDict["Length"].setText(
                    str(csv_dict["Param1"][self.index])
                )
                self.lineEditDict["Width"].setText(
                    str(csv_dict["Param2"][self.index])
                )
                self.lineEditDict["Height"].setText(
                    str(csv_dict["Param3"][self.index])
                )
            if self.item_geometry == "cylinder":
                self.lineEditDict["Diameter_x"].setText(
                    str(csv_dict["Param1"][self.index])
                )
                self.lineEditDict["Diameter_y"].setText(
                    str(csv_dict["Param2"][self.index])
                )
                self.lineEditDict["Length"].setText(
                    str(csv_dict["Param3"][self.index])
                )
            if self.item_geometry == "lforme":
                self.lineEditDict["Length"].setText(
                    str(csv_dict["Param1"][self.index])
                )
                self.lineEditDict["Width"].setText(
                    str(csv_dict["Param2"][self.index])
                )
                self.lineEditDict["Height"].setText(
                    str(csv_dict["Param3"][self.index])
                )

                self.lineEditDict["Inner Length"].setText(
                    str(csv_dict["Param4"][self.index])
                )
                self.lineEditDict["Inner Height"].setText(
                    str(csv_dict["Param5"][self.index])
                )
        if self.readonlyFlag:
            for key in self.lineEditDict.keys():
                self.lineEditDict[key].setReadOnly(True)

    def back2NewFileWin(self):
        self.signalEnsemble.back2NewFileWin.emit(False)

    def updateMiddleWidgetLayout(self, text):
        self.item_geometry = text
        self.ex_canvas.updateItemForme(text)
        self.inputWidget.setParent(None)
        self.inputWidget = self.getInputWidget()
        self.middleWidgetLayout.addWidget(self.inputWidget)


    def cancelAct(self):
        self.resetItemGeometry()
        self.deleteItemsOfLayout(self.vboxl)
        self.addLMRWigets()

    def resetItemGeometry(self):
        if self.create_flag:
            self.item_geometry = "boite"
        else:
            self.item_geometry = self.csv_dict["Item_Geometry"][self.index]

    def deleteItemsOfLayout(self, layout):
        if layout is not None:
            while layout.count():
                item = layout.takeAt(0)
                widget = item.widget()
                if widget is not None:
                    widget.setParent(None)
                else:
                    self.deleteItemsOfLayout(item.layout())

    def updateCreatedItem(self):
        user_input = self.getLineEditText()
        self.createdItem_canvas.updateByUserInput(user_input)


    def getLineEditText(self):
        user_input = {}
        user_input["Item_Geometry"] = self.item_geometry
        user_input["Enterprise"] = self.lineEditDict["Enterprise"].text()
        try:
            user_input["Weight"] = float(self.lineEditDict["Weight"].text())
        except:
            user_input["Weight"] = 0.0

        try:
            user_input["Bottom_surface_No"] = int(self.lineEditDict["Bottom Surface No."].text())
        except:
            user_input["Bottom_surface_No"] = -1


        try:
            user_input["Load_bearing_capacity"] = float(self.lineEditDict["Load Bearing Capacity"].text())
        except:
            user_input["Load_bearing_capacity"] = 0.0
        if self.item_geometry == "boite":
            # we use try except to prevent error when input is "-"
            try:
                user_input["Param1"] = float(self.lineEditDict["Length"].text())
            except Exception:
                user_input["Param1"] = 0.0
            try:
                user_input["Param2"] = float(self.lineEditDict["Width"].text())
            except Exception:
                user_input["Param2"] = 0.0
            try:
                user_input["Param3"] = float(self.lineEditDict["Height"].text())
            except Exception:
                user_input["Param3"] = 0.0
            user_input["Param4"] = 0.0
            user_input["Param5"] = 0.0

        if self.item_geometry == "cylinder":
            try:
                user_input["Param1"] = float(self.lineEditDict["Diameter_x"].text())
            except Exception:
                user_input["Param1"] = 0.0
            try:
                user_input["Param2"] = float(self.lineEditDict["Diameter_y"].text())
            except Exception:
                user_input["Param2"] = 0.0
            try:
                user_input["Param3"] = float(self.lineEditDict["Length"].text())
            except Exception:
                user_input["Param3"] = 0.0
            user_input["Param4"] = 0.0
            user_input["Param5"] = 0.0

        if self.item_geometry == "lforme":
            try:
                user_input["Param1"] = float(self.lineEditDict["Length"].text())
            except Exception:
                user_input["Param1"] = 0.0
            try:
                user_input["Param2"] = float(self.lineEditDict["Width"].text())
            except Exception:
                user_input["Param2"] = 0.0
            try:
                user_input["Param3"] = float(self.lineEditDict["Height"].text())
            except Exception:
                user_input["Param3"] = 0.0
            try:
                user_input["Param4"] = float(self.lineEditDict["Inner Length"].text())
            except Exception:
                user_input["Param4"] = 0.0
            try:
                user_input["Param5"] = float(self.lineEditDict["Inner Height"].text())
            except Exception:
                user_input["Param5"] = 0.0

        return user_input

    def addAct(self):
        user_input = self.getLineEditText()
        for label in user_input.keys():
            if self.create_flag:
                self.csv_dict[label].append(user_input[label])
            else:
                self.csv_dict[label][self.index] = user_input[label]
        csvTools.saveIntoTempsCsv(self.csv_dict)
        self.back2NewFileWin()


class OpsWidget(PyQtWs.QWidget):
    def __init__(self, parent=None):
        super().__init__(parent)
        hboxl = PyQtWs.QHBoxLayout(self)
        self.editButton = PyQtWs.QPushButton()
        self.editButton.setIcon(PyQtGui.QIcon("materials/icons/edit_16px.png"))

        self.deleteButton = PyQtWs.QPushButton()
        self.deleteButton.setIcon(PyQtGui.QIcon("materials/icons/trash_16px.png"))
        # deleteButton.clicked.connect()

        self.viewButton = PyQtWs.QPushButton()
        self.viewButton.setIcon(PyQtGui.QIcon("materials/icons/view_16px.png"))

        self.setStyleSheet("""
        QPushButton{
            background-color: #fff;
            border:none;
            min-width:16px;
            min-height:16px;
        }
        QPushButton::pressed{
            background-color:gray;
            border-style: inset;
        }
        """)

        hboxl.addWidget(self.editButton)
        hboxl.addWidget(self.viewButton)
        hboxl.addWidget(self.deleteButton)
        self.setLayout(hboxl)




class NewFileWidget(PyQtWs.QWidget):
    def __init__(self, signalEnsemble, parent=None):
        super().__init__(parent)
        self.signalEnsemble = signalEnsemble
        self.csv_dict = csvTools.readFromTempsCsv()

        
        self.initUI()

    def initUI(self):
        
        self.item_num = len(self.csv_dict["Item_Geometry"])
        self.column_num = len(self.csv_dict)
        vboxl = PyQtWs.QVBoxLayout(self)
        lb1 = PyQtWs.QLabel()
        lb1.setText("<p style='font-size:36px;font-family:Arial'>Global View</p>")
        lb1.setFixedHeight(200)
        lb1.setAlignment(PyQtCore.Qt.AlignCenter)
        vboxl.addWidget(lb1)

        # main content
        self.main_contentW = PyQtWs.QWidget()
        self.main_contentW.setFixedWidth(1000)
        self.main_contentW.setFixedHeight(500)
        vboxl2 = PyQtWs.QVBoxLayout(self.main_contentW)

        hboxl2 = PyQtWs.QHBoxLayout()
        lb2 = PyQtWs.QLabel()
        lb2.setText("<p>You have {} Items right now</p>".format(self.item_num))

        plus_icon = qta.icon("fa5s.plus", color="white")

        newItemButton = PyQtWs.QPushButton(plus_icon, "Add New Item")
        newItemButton.setFixedWidth(150)
        newItemButton.setObjectName("newItemButton")
        newItemButton.setStyleSheet(btnSS1)
        newItemButton.clicked.connect(self.addItem)

        hboxl2.addWidget((lb2))
        hboxl2.addWidget(newItemButton, 0, PyQtCore.Qt.AlignRight)  # finish hboxl2

        self.tableWidget = PyQtWs.QTableWidget()
        self.tableWidget.setEditTriggers(PyQtWs.QTableWidget.NoEditTriggers)
        self.tableWidget.setRowCount(self.item_num)
        self.tableWidget.setColumnCount(self.column_num + 1)
        headers = ["Operations"]
        headers.extend(list(self.csv_dict.keys()))
        self.tableWidget.setHorizontalHeaderLabels(headers)
        self.tableWidget.horizontalHeader().setDefaultAlignment(PyQtCore.Qt.AlignLeft)


        for i in range(self.item_num):
            for j, column_header in enumerate(list(self.csv_dict.keys())):
                ops = OpsWidget()
                ops.deleteButton.clicked.connect(self.deleteRow)
                ops.editButton.clicked.connect(self.editItem)
                ops.viewButton.clicked.connect(self.viewItem)
                self.tableWidget.setCellWidget(i, 0, ops)
                self.tableWidget.setItem(i, j + 1, PyQtWs.QTableWidgetItem(str(self.csv_dict[column_header][i])))
        # print(self.tableWidget.itemAt(0,0).text())
        # self.tableWidget.removeRow(0)

        save_icon = qta.icon("fa5s.save", color="white")
        saveListButton = PyQtWs.QPushButton(save_icon, "Save Now")
        saveListButton.setFixedWidth(150)
        saveListButton.clicked.connect(self.saveCSV)
        saveListButton.setObjectName("saveListButton")
        saveListButton.setStyleSheet(btnSS1)

        vboxl2.addLayout((hboxl2))
        vboxl2.addWidget(self.tableWidget)
        vboxl2.addWidget(saveListButton, 0, PyQtCore.Qt.AlignRight)

        vboxl.addWidget(self.main_contentW)
        vboxl.setAlignment(PyQtCore.Qt.AlignCenter)

    def deleteRow(self):
        btn = self.sender()
        ops = btn.parent()
        row_index = self.tableWidget.indexAt(ops.pos()).row()
        self.tableWidget.removeRow(row_index)
        for key in self.csv_dict.keys():
            self.csv_dict[key].pop(row_index)
        csvTools.saveIntoTempsCsv(self.csv_dict)

    def editItem(self):
        btn = self.sender()
        ops = btn.parent()
        row_index = self.tableWidget.indexAt(ops.pos()).row()
        # print(row_index)
        self.signalEnsemble.showEditWin.emit(row_index, False)

    def addItem(self):
        if self.item_num == 0:
            # -1 means we create new item from 0
            self.signalEnsemble.showEditWin.emit(-1, False)
        else:
            # it means we create a new item
            self.signalEnsemble.showEditWin.emit(len(self.csv_dict["Item_Geometry"]), False)

    def viewItem(self):
        btn = self.sender()
        ops = btn.parent()
        row_index = self.tableWidget.indexAt(ops.pos()).row()
        # print(row_index)
        self.signalEnsemble.showEditWin.emit(row_index, True)

    def saveCSV(self):
        fname = PyQtWs.QFileDialog.getSaveFileName(None, "Save CSV", ".", "CSV Files (*.csv);;All Files (*)")
        if fname[0]:
            file_path = fname[0]
            df = pd.DataFrame(self.csv_dict)
            df.to_csv(file_path)






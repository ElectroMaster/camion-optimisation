import sys
import PyQt5.QtWidgets as PyQtWs
import PyQt5.QtGui as PyQtGui
import PyQt5.QtCore as PyQtCore
import subwins.mplCanvas as mplCanvas
import forme
import numpy as np

class WelcomeCanvas(mplCanvas.MplCanvas3d):
    """Simple canvas with a sine plot."""
    def axesSetting(self):
        self.axes.set_aspect(1)
        self.axes.set_xlabel("X")
        self.axes.set_ylabel("Y")
        self.axes.set_zlabel("Z")
        self.axes.set_xlim(0, 10)
        self.axes.set_ylim(0, 10)
        self.axes.set_zlim(0, 10)
        self.axes.grid(False)
        self.axes.set_axis_off()
    def compute_initial_figure(self):
        X = np.array([[0.5, 2, 2], [3, 1.5, 1], [3, 3.5, 1], [3, 1, 2.5], [3, 3, 2.5], [4, 2, 4], [2, 2, 3.5],
                      [1.5, 3, 5]]).astype(float)
        B = np.array([[1, 4, 4], [2, 3, 2], [2, 1, 2], [4, 2, 1], [4, 2, 1], [2, 2, 2], [2, 4, 1], [3, 3, 2]]).astype(
            float)
        B_Lforme = np.array([[3, 4, 5, 2, 4], [4, 2, 6, 2, 5]]).astype(float)
        X_Lforme = np.array([[7.5, 8, 2.5], [8, 2, 3]]).astype(float)
        nb_composant = X.shape[0]
        boite_color_list = [(0.060875607663631315, 0.5955154089862812, 0.5122013148420456, 0.25),
                            (0.7564388137030567, 0.64322827237689, 0.733956603323801, 0.25),
                            (0.3237794666285899, 0.37927087921860003, 0.9320399957141835, 0.25),
                            (0.14433997506876006, 0.8858413164590535, 0.48431578207679116, 0.25),
                            (0.6004374510394305, 0.5962569517787925, 0.774400302454286, 0.25),
                            (0.8864868899238086, 0.0002688137947731706, 0.6987696987931398, 0.25),
                            (0.5758947329757933, 0.09342987456577789, 0.5832347125331172, 0.25),
                            (0.3733586751717589, 0.4746755319136905, 0.9026573325961607, 0.25),
                            (0.7731080048371622, 0.08297022317670233, 0.7958617984869675, 0.25),
                            (0.8660576229074144, 0.3943950956219736, 0.2850871073723439, 0.25),
                            (0.34149177318582546, 0.2737464795689839, 0.29356820899678293, 0.25)]
        count = 0
        for i in range(X.shape[0]):
            boite = forme.Boite((X[i, 0], X[i, 1], X[i, 2]), B[i, 0], B[i, 1], B[i, 2])
            self.axes.add_collection3d(boite.getPoly3d(facecolors=boite_color_list[count]))
            count += 1
        for i in range(X_Lforme.shape[0]):
            lforme = forme.LForme((X_Lforme[i, 0], X_Lforme[i, 1], X_Lforme[i, 2]),
                                  B_Lforme[i,0], B_Lforme[i, 1], B_Lforme[i,2], B_Lforme[i,3], B_Lforme[i,4])
            if i == 1:
                lforme.rotation3d(0, 0, np.pi/2)
            self.axes.add_collection3d(lforme.getPoly3d(facecolors=boite_color_list[count]))
            count += 1
        cylinder = forme.Cylinder((2, 9, 2), 4, 4, 4, N=50)
        cylinder.drawCylinderOnAxes(self.axes, facecolors=boite_color_list[count])





def getWelcomeWidget(VERSION_STR):
    welcomeWidget = PyQtWs.QWidget()
    welcomeWidget.setProperty("content-background", True)
    vboxl = PyQtWs.QVBoxLayout(welcomeWidget)
    wc = WelcomeCanvas()
    
    #
    wcTitle = PyQtWs.QLabel()
    wcTitle.setText("<p><span style='font-size:40px;'>ATHENA</span></p>"
    
                    + "An interface help you express your needs clearly"
                    + "<p><span style='font-size:20px'>" + VERSION_STR + "</span></p>"
                    )
    wcTitle.setAlignment(PyQtCore.Qt.AlignCenter)
    wcTitle.setObjectName("wcTitle")
    wcTitle.setStyleSheet("""
    QLabel#wcTitle {
        margin:100px 0 60px 0;
        font-family:Arial;
    }
    """)
    
    vboxl.addWidget(wc)
    vboxl.addWidget(wcTitle)
    return welcomeWidget
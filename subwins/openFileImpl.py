import sys
import PyQt5.QtWidgets as PyQtWs
import PyQt5.QtGui as PyQtGui
import PyQt5.QtCore as PyQtCore
import subwins.mplCanvas as mplCanvas
import forme
import numpy as np
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas

class DemonstrateCanvas(mplCanvas.MplCanvas3d):
    def __init__(self, parent=None, width=10, height=10, dpi=100, itemForme=None):
        self.itemForme = itemForme
        mplCanvas.MplCanvas3d.__init__(self, parent=parent, width=width, height=height, dpi=dpi)
        timer = PyQtCore.QTimer(self)
        timer.timeout.connect(self.update_figure)
        timer.start(1000)
    """Simple canvas with a sine plot."""
    def axesSetting(self):
        self.axes.set_aspect(1)
        self.axes.set_xlabel("X")
        self.axes.set_ylabel("Y")
        self.axes.set_zlabel("Z")
        self.axes.set_xlim(0, 10)
        self.axes.set_ylim(0, 10)
        self.axes.set_zlim(0, 10)
        self.axes.grid(False)
        self.axes.set_axis_off()

    def compute_initial_figure(self):
        self.facecolors = forme.random_mpl_color_rgba()
        pass

    def update_figure(self):
        self.axes.cla()
        self.axesSetting()
        if self.itemForme == "cylinder":
            cylinder = forme.Cylinder((5, 5, 5), 2, 2, 4, N=50)
            cylinder.drawCylinderOnAxes(self.axes, facecolors=self.facecolors)

            self.axes.plot3D([4, 6], [5, 5], [7, 7], label="diameter_x", linewidth=2)
            self.axes.plot3D([5, 5], [4, 6], [7, 7], label="diameter_y", linewidth=2)
            self.axes.plot3D([5, 5], [5, 5], [3, 7], label="length", linewidth=2)
            self.axes.legend(loc="upper center")

        elif self.itemForme == "boite":
            boite = forme.Boite((5,5,5), 3,4,5)
            self.axes.add_collection3d(boite.getPoly3d(facecolors=self.facecolors))
            self.axes.plot3D([3.3, 6.5], [2.8, 2.8], [2.5, 2.5], label="length", linewidth=2)
            self.axes.plot3D([3.3, 3.3], [2.8, 7], [2.5, 2.5], label="width", linewidth=2)
            self.axes.plot3D([3.3, 3.3], [2.8, 2.8], [2.5, 7.5], label="height", linewidth=2)
            self.axes.legend(loc="upper center")

        elif self.itemForme == "lforme":
            lobject = forme.LForme((5, 5, 5), 3, 4, 5, 2, 4)
            self.axes.add_collection3d(lobject.getPoly3d(facecolors=self.facecolors))
            self.axes.plot3D([3.3, 6.5], [2.8, 2.8], [2.5, 2.5], label="length", linewidth=2)
            self.axes.plot3D([3.3, 3.3], [2.8, 7], [2.5, 2.5], label="width", linewidth=2)
            self.axes.plot3D([3.3, 3.3], [2.8, 2.8], [2.5, 7.5], label="height", linewidth=2)
            self.axes.plot3D([4.5, 6.5], [7.2, 7.2], [3.5, 3.5], label="inner length", linewidth=2)
            self.axes.plot3D([4.5, 4.5], [7.2, 7.2], [3.5, 7.5], label="inner height", linewidth=2)
            self.axes.legend(loc="upper center")
        else:
            # if itemforme == None, we do nothing
            pass

        self.draw() # <---- do not forget draw
    def updateItemForme(self, itemForme):
        # with help of updatefigure(), we can directly change item name and no need to recreate a new canvas
        # when adding a new item, facecolors can change :)
        self.facecolors = forme.random_mpl_color_rgba()
        if itemForme !=None:
            self.itemForme = itemForme.lower()
        else:
            self.itemForme = itemForme






class OpenFileWidget(PyQtWs.QWidget):
    def __init__(self,parent=None):
        super().__init__(parent)

        hboxl = PyQtWs.QHBoxLayout(self)

        ex_canvas = DemonstrateCanvas()
        ex_canvas2 = DemonstrateCanvas()
        # test

        ex_canvas.updateItemForme("lforme")
        middle = PyQtWs.QWidget()
        ex_canvas2.updateItemForme("lforme")


        hboxl.addWidget(ex_canvas, 40)
        hboxl.addWidget(middle,20)
        hboxl.addWidget(ex_canvas2,40)
        self.setLayout(hboxl)



def getOpenFileWidget():
    openFileWidget = PyQtWs.QWidget()
    openFileWidget.setObjectName("openFileWidget")
    openFileWidget.setStyleSheet("""
    QWidget#openFileWidget {
        background-color: rgb(255, 255, 255);
        }
    """)
    hboxl = PyQtWs.QHBoxLayout(openFileWidget)

    subFileWidget = OpenFileWidget()
    hboxl.addWidget(subFileWidget)



    return openFileWidget
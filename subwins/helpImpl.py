import sys
import PyQt5.QtWidgets as PyQtWs
import PyQt5.QtGui as PyQtGui
import PyQt5.QtCore as PyQtCore
import subwins.mplCanvas as mplCanvas
import forme
import numpy as np
import pandas as pd
import utils.csvTools as csvTools
import qtawesome as qta

class AboutWin(PyQtWs.QWidget):
    def __init__(self, parent=None):
        super().__init__(parent=parent)
        self.initUI()

    def initUI(self):
        self.resize(640, self.goldenRate(640))
        self.centered()
        self.addBackground()
        self.setWindowTitle("About us")
        self.setWindowIcon(PyQtGui.QIcon('materials/icons/logo-128px.png'))


    def addBackground(self):
        mypalette = PyQtGui.QPalette()
        pixmap = PyQtGui.QPixmap("materials/images/test.png")
        mypalette.setBrush(self.backgroundRole(),
                           PyQtGui.QBrush(
                               pixmap.scaled(640, self.goldenRate(640))
                           ))
        self.setPalette(mypalette)
        self.setAutoFillBackground(True)

    def centered(self):
        frameGeometry = self.frameGeometry()
        centerPoint = PyQtWs.QDesktopWidget().availableGeometry().center()
        frameGeometry.moveCenter(centerPoint)
        self.move(frameGeometry.topLeft())

    def goldenRate(self, width):
        return int(width*0.618)
import sys

import PyQt5.QtWidgets as PyQtWs
import PyQt5.QtCore as PyQtCore

from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.figure import Figure
from mpl_toolkits.mplot3d import axes3d, Axes3D #<-- Note the capitalization!


import forme
import numpy as np

class MplCanvas3d(FigureCanvas):
    """Ultimately, this is a PyQtWs.QWidget (as well as a FigureCanvasAgg, etc.)."""

    def __init__(self, parent=None, width=10, height=10, dpi=100):
        fig = Figure(figsize=(width, height), dpi=dpi)

        FigureCanvas.__init__(self, fig)
        self.setParent(parent)
        FigureCanvas.setSizePolicy(self,
                                   PyQtWs.QSizePolicy.Expanding,
                                   PyQtWs.QSizePolicy.Expanding)
        FigureCanvas.updateGeometry(self)

        # axes must be created after the creation of canvas to make sure we can use mouse to rotate figure
        self.axes = Axes3D(fig)
        self.axes.mouse_init()
        self.axesSetting()
        self.compute_initial_figure()

    def axesSetting(self):
        pass
    def compute_initial_figure(self):
        pass




# class MyDynamicMplCanvas(MplCanvas):
#     """A canvas that updates itself every second with a new plot."""
#
#     def __init__(self, *args, **kwargs):
#         MplCanvas.__init__(self, *args, **kwargs)
#         timer = PyQtCore.QTimer(self)
#         timer.timeout.connect(self.update_figure)
#         timer.start(1000)
#
#     def compute_initial_figure(self):
#         self.axes.plot([0, 1, 2, 3], [1, 2, 0, 4], 'r')
#
#     def update_figure(self):
#         # Build a list of 4 random integers between 0 and 10 (both inclusive)
#         # l = [random.randint(0, 10) for i in range(4)]
#         self.axes.cla()
#         # self.axes.plot([0, 1, 2, 3], l, 'r')
#         self.draw()
import numpy as np

from utils.csvTools import read_colis_list_csv
from utils.tools import get_rotation3d_dict, load_population,\
    getItemEntrepriseDictAndEntrepriseItemDict, getIdealDechargementOrderFromEntrepriseOrder
from forme import Boite, LForme, Cylinder, random_mpl_color_rgba
import time
import matplotlib.pyplot as plt
import argparse
import logging
import sys
import functools
import re




logging.basicConfig()
log = logging.getLogger("colis.py ")
np.set_printoptions(threshold=sys.maxsize)
rotation3d_dict = get_rotation3d_dict()

class Camion(Boite):
    def __init__(self, point_ref, longeur, largeur, hauteur):
        super().__init__(point_ref, longeur, largeur, hauteur)

    @classmethod
    def atOrigine(cls, longeur, largeur, hauteur):
        return cls((longeur / 2.0, largeur / 2.0, hauteur / 2.0), longeur, largeur, hauteur)

    def init(self, camion_color=(1, 1, 1, 0), edgecolors="black"):
        self.fig = plt.figure()
        self.ax = self.fig.add_subplot(111, projection="3d")
        self.ax1.set_aspect(1)
        self.ax1.add_collection3d(super().getPoly3d(facecolors=camion_color, edgecolors=edgecolors))

    def addPoly3d(self, poly3d):
        self.ax1.add_collection3d(poly3d)

    def draw(self, show_axis=False, show_axis_label=False, show_grid=False):
        axis_switch = "on" if show_axis else "off"
        plt.axis(axis_switch)
        if show_axis_label == True:
            self.ax1.set_xlabel("X")
            self.ax1.set_ylabel("Y")
            self.ax1.set_zlabel("Z")
        self.ax1.grid(show_grid)
        plt.show()
        
def overlap2d(center_1, length_1, width_1, center_2, length_2, width_2):
    overlap_length = max(0, min(center_1[0] + length_1/2, center_2[0]+length_2/2) -
                        max(center_1[0] - length_1/2, center_2[0] - length_2/2))
    overlap_width = max(0, min(center_1[1] + width_1/2, center_2[1]+width_2/2) -
                        max(center_1[1] - width_1/2, center_2[1] - width_2/2))
    overlap_area = overlap_length * overlap_width
    ret = False
    if overlap_area != 0:
        ret = True
    return ret, overlap_area, overlap_length, overlap_width


def computeOverlap2dByVertices(vertices1, vertices2):
    assert len(vertices1) == 4 and len(
        vertices2) == 4, "computeOverlap2dByVertices only accept 4 vertices for each rectangle"
    vertices1 = np.array(vertices1).astype(np.float)
    vertices2 = np.array(vertices2).astype(np.float)
    x_min1 = np.min(vertices1[:, 0])
    x_max1 = np.max(vertices1[:, 0])
    x_min2 = np.min(vertices2[:, 0])
    x_max2 = np.max(vertices2[:, 0])
    overlap_length = max(0.0, min(x_max1, x_max2) - max(x_min1, x_min2))

    y_min1 = np.min(vertices1[:, 1])
    y_max1 = np.max(vertices1[:, 1])
    y_min2 = np.min(vertices2[:, 1])
    y_max2 = np.max(vertices2[:, 1])
    overlap_width = max(0.0, min(y_max1, y_max2) - max(y_min1, y_min2))

    overlap_area = overlap_length * overlap_width
    ret = False
    if overlap_area != 0:
        ret = True
    return ret, overlap_area


def addGravity(X, P, csv_dict):
    # principle of get raw placement order is similar with quick sort
    nb_composant = X.shape[0]
    # get raw placement order
    raw_placement_order = list(range(nb_composant))
    for i in range(nb_composant-1):
        for idx in range(nb_composant-i-1):
            # we will sort composant_j and composant_j+1
            swap_flag = False
            j = raw_placement_order[idx]
            jplus1 = raw_placement_order[idx+1]
            # log.debug(">>>Composant j = {}, composant j+1 = {}".format(j, jplus1))
            # log.debug("raw placement order: {}".format(raw_placement_order))
            if csv_dict["Item_Geometry"][j] == "boite":
                if csv_dict["Item_Geometry"][jplus1] == "boite":
                    # B-B: composant j is boite and composant j+1 is boite
                    boite_j = Boite(tuple(X[j]), csv_dict["Param1"][j], csv_dict["Param2"][j], csv_dict["Param3"][j])
                    boite_jplus1 = Boite(tuple(X[jplus1]), csv_dict["Param1"][jplus1], csv_dict["Param2"][jplus1], csv_dict["Param3"][jplus1])
                    boite_j.rotation3d(*rotation3d_dict[P[j]])
                    boite_jplus1.rotation3d(*rotation3d_dict[P[jplus1]])
                    if boite_j.getBottomSurfaceHeight() > boite_jplus1.getBottomSurfaceHeight():
                        # if composant j is on top of composant j+1
                        # we need to change placement order
                        swap_flag = True
                elif csv_dict["Item_Geometry"][jplus1] == "lforme":
                    # B-L: composant j is boite and composant j+1 is lforme
                    boite_j = Boite(tuple(X[j]), csv_dict["Param1"][j], csv_dict["Param2"][j], csv_dict["Param3"][j])
                    lforme_jplus1 = LForme(tuple(X[jplus1]), csv_dict["Param1"][jplus1], csv_dict["Param2"][jplus1],
                                    csv_dict["Param3"][jplus1], csv_dict["Param4"][jplus1],csv_dict["Param5"][jplus1])
                    boite_j.rotation3d(*rotation3d_dict[P[j]])
                    lforme_jplus1.rotation3d(*rotation3d_dict[P[jplus1]])
                    if lforme_jplus1.getBottomSurfaceIndex() == 0 or lforme_jplus1.getBottomSurfaceIndex() == 4:
                        if boite_j.getBottomSurfaceHeight()>lforme_jplus1.getBottomSurfaceHeight():
                            swap_flag = True
                    elif lforme_jplus1.getBottomSurfaceIndex() == 2 or lforme_jplus1.getBottomSurfaceIndex() == 5:
                        if boite_j.getTopSurfaceHeight()>lforme_jplus1.getTopSurfaceHeight():
                            swap_flag = True
                    elif lforme_jplus1.getBottomSurfaceIndex() == 1 or lforme_jplus1.getBottomSurfaceIndex() == 3:
                        if boite_j.getBottomSurfaceHeight()>lforme_jplus1.getBottomSurfaceHeight():
                            swap_flag = True

            elif csv_dict["Item_Geometry"][j] == "lforme":
                if csv_dict["Item_Geometry"][jplus1] == "boite":
                    # L-B
                    lforme_j = LForme(tuple(X[j]), csv_dict["Param1"][j], csv_dict["Param2"][j],
                                csv_dict["Param3"][j], csv_dict["Param4"][j], csv_dict["Param5"][j])
                    boite_jplus1 = Boite(tuple(X[jplus1]), csv_dict["Param1"][jplus1], csv_dict["Param2"][jplus1], csv_dict["Param3"][jplus1])
                    lforme_j.rotation3d(*rotation3d_dict[P[j]])
                    boite_jplus1.rotation3d(*rotation3d_dict[P[jplus1]])
                    if lforme_j.getBottomSurfaceIndex() == 0 or lforme_j.getBottomSurfaceIndex()==4:
                        if lforme_j.getBottomSurfaceHeight() > boite_jplus1.getBottomSurfaceHeight():
                            swap_flag = True
                    elif lforme_j.getBottomSurfaceIndex() == 2 or lforme_j.getBottomSurfaceIndex() == 5:
                        if lforme_j.getTopSurfaceHeight() > boite_jplus1.getTopSurfaceHeight():
                            swap_flag = True
                    elif lforme_j.getBottomSurfaceIndex() == 1 or lforme_j.getBottomSurfaceIndex() == 3:
                        if lforme_j.getBottomSurfaceHeight() > boite_jplus1.getBottomSurfaceHeight():
                            swap_flag = True
                elif csv_dict["Item_Geometry"][jplus1] == "lforme":
                    # L-L
                    lforme_j = LForme(tuple(X[j]), csv_dict["Param1"][j], csv_dict["Param2"][j],
                                csv_dict["Param3"][j], csv_dict["Param4"][j], csv_dict["Param5"][j])
                    lforme_jplus1 = LForme(tuple(X[jplus1]), csv_dict["Param1"][jplus1], csv_dict["Param2"][jplus1],
                                    csv_dict["Param3"][jplus1], csv_dict["Param4"][jplus1],csv_dict["Param5"][jplus1])
                    lforme_j.rotation3d(*rotation3d_dict[P[j]])
                    lforme_jplus1.rotation3d(*rotation3d_dict[P[jplus1]])
                    if lforme_j.getBottomSurfaceIndex() == 0 or lforme_j.getBottomSurfaceIndex() == 4:
                        if lforme_jplus1.getBottomSurfaceIndex() == 0 or lforme_jplus1.getBottomSurfaceIndex() == 4:
                            if lforme_j.getBottomSurfaceHeight() > lforme_jplus1.getBottomSurfaceHeight():
                                swap_flag = True
                        elif lforme_jplus1.getBottomSurfaceIndex() == 2 or lforme_jplus1.getBottomSurfaceIndex() == 5:
                            if lforme_j.getBottomSurfaceHeight() > lforme_jplus1.getTopSurfaceHeight():
                                swap_flag = True
                        elif lforme_jplus1.getBottomSurfaceIndex() == 1 or lforme_jplus1.getBottomSurfaceIndex() == 3:
                            if lforme_j.getBottomSurfaceHeight() > lforme_jplus1.getBottomSurfaceHeight():
                                swap_flag = True
                    elif lforme_j.getBottomSurfaceIndex() == 2 or lforme_j.getBottomSurfaceIndex() == 5:
                        if lforme_jplus1.getBottomSurfaceIndex() == 0 or lforme_jplus1.getBottomSurfaceIndex() == 4:
                            if lforme_j.getTopSurfaceHeight() > lforme_jplus1.getBottomSurfaceHeight():
                                swap_flag = True
                        elif lforme_jplus1.getBottomSurfaceIndex() == 2 or lforme_jplus1.getBottomSurfaceIndex() == 5:
                            if lforme_j.getTopSurfaceHeight() > lforme_jplus1.getTopSurfaceHeight():
                                swap_flag = True
                        elif lforme_jplus1.getBottomSurfaceIndex() == 1 or lforme_jplus1.getBottomSurfaceIndex() == 3:
                            if lforme_j.getTopSurfaceHeight() > lforme_jplus1.getTopSurfaceHeight():
                                swap_flag = True
                    elif lforme_j.getBottomSurfaceIndex() == 1 or lforme_j.getBottomSurfaceIndex() == 3:
                        if lforme_jplus1.getBottomSurfaceIndex() == 0 or lforme_jplus1.getBottomSurfaceIndex() == 4:
                            if lforme_j.getBottomSurfaceHeight() > lforme_jplus1.getBottomSurfaceHeight():
                                swap_flag = True
                        elif lforme_jplus1.getBottomSurfaceIndex() == 2 or lforme_jplus1.getBottomSurfaceIndex() == 5:
                            if lforme_j.getTopSurfaceHeight() > lforme_jplus1.getTopSurfaceHeight():
                                swap_flag = True
                        elif lforme_jplus1.getBottomSurfaceIndex() == 1 or lforme_jplus1.getBottomSurfaceIndex() == 3:
                            if lforme_j.getBottomSurfaceHeight() > lforme_jplus1.getBottomSurfaceHeight():
                                swap_flag = True
            # log.debug("Swap flag = {}".format(swap_flag))
            if swap_flag:
                temp = raw_placement_order[idx + 1]
                raw_placement_order[idx + 1] = raw_placement_order[idx]
                raw_placement_order[idx] = temp

    # log.debug(raw_placement_order)

    placed_list = []
    for composant_i in raw_placement_order:
        z_last_composant = 0

            # if we haven't placed any composants, z_last_compsant will be 0
            # if we have placed some composants, we need to detect

        z_record_list = []
        z_last_composant = 0
        if csv_dict["Item_Geometry"][composant_i] == "boite":
            boite_i = Boite(tuple(X[composant_i]),
                            csv_dict["Param1"][composant_i], csv_dict["Param2"][composant_i],
                            csv_dict["Param3"][composant_i])
            boite_i.rotation3d(*rotation3d_dict[P[composant_i]])
            _, face_dict_i = boite_i.getBottomVertices()
            vertices_i = face_dict_i["0"]
            if len(placed_list) != 0:
                for composant_j in  placed_list:
                    if csv_dict["Item_Geometry"][composant_j] == "boite":
                        # B-B

                        boite_j = Boite(tuple(X[composant_j]),
                                        csv_dict["Param1"][composant_j], csv_dict["Param2"][composant_j],
                                        csv_dict["Param3"][composant_j])
                        boite_j.rotation3d(*rotation3d_dict[P[composant_j]])
                        _, face_dict_j = boite_j.getTopVertices()

                        vertices_j = face_dict_j["0"]
                        ret, _ = computeOverlap2dByVertices(vertices_i, vertices_j)
                        if ret:
                            # if composant i have overlap with composant j, we need record this height
                            z_record_list.append(np.max(np.array(vertices_j)[:, 2]))

                    elif csv_dict["Item_Geometry"][composant_j] == "lforme":
                        # B-L
                        lforme_j = LForme(tuple(X[composant_j]),
                                                csv_dict["Param1"][composant_j], csv_dict["Param2"][composant_j],
                                                csv_dict["Param3"][composant_j], csv_dict["Param4"][composant_j],
                                                csv_dict["Param5"][composant_j])
                        lforme_j.rotation3d(*rotation3d_dict[P[composant_j]])
                        num_face, face_dict_j = lforme_j.getTopVertices()
                        # cases:
                        lforme_jBottomIdx = lforme_j.getBottomSurfaceIndex()
                        if lforme_jBottomIdx == 0 or lforme_jBottomIdx == 4:
                            assert num_face == 2, "Internal Error"
                            vertices_j_0 = face_dict_j["0"]
                            ret, _ = computeOverlap2dByVertices(vertices_i, vertices_j_0)
                            if ret:
                                z_record_list.append(np.max(np.array(vertices_j_0)[:,2]))
                            else:
                                vertices_j_1 = face_dict_j["1"]
                                ret, _ = computeOverlap2dByVertices(vertices_i, vertices_j_1)
                                if ret:
                                    z_record_list.append(np.max(np.array(vertices_j_1)[:,2]))
                        elif lforme_jBottomIdx == 2 or lforme_jBottomIdx == 5:
                            assert num_face == 1, "Internal Error"
                            vertices_j = face_dict_j["0"]
                            ret, _ = computeOverlap2dByVertices(vertices_i, vertices_j)
                            if ret:
                                z_record_list.append(np.max(np.array(vertices_j)[:,2]))
                        elif lforme_jBottomIdx == 1 or lforme_jBottomIdx == 3:
                            assert num_face == 2, "Internal Error"
                            vertices_j_0 = face_dict_j["0"]
                            ret, _ = computeOverlap2dByVertices(vertices_i, vertices_j_0)
                            if ret:
                                z_record_list.append(np.max(np.array(vertices_j_0)[:,2]))
                            else:
                                vertices_j_1 = face_dict_j["1"]
                                ret, _ = computeOverlap2dByVertices(vertices_i, vertices_j_1)
                                if ret:
                                    z_record_list.append(np.max(np.array(vertices_j_1)[:,2]))
                if len(z_record_list) == 0:
                    z_last_composant = 0
                else:
                    z_last_composant = np.max(z_record_list)
            z_i = z_last_composant + (boite_i.getTopSurfaceHeight() - boite_i.getBottomSurfaceHeight())/2.0
            X[composant_i, 2] = z_i
            placed_list.append(composant_i)
        elif csv_dict["Item_Geometry"][composant_i] == "lforme":
            lforme_i = LForme(tuple(X[composant_i]),
                              csv_dict["Param1"][composant_i], csv_dict["Param2"][composant_i],
                              csv_dict["Param3"][composant_i], csv_dict["Param4"][composant_i],
                              csv_dict["Param5"][composant_i])
            lforme_i.rotation3d(*rotation3d_dict[P[composant_i]])
            num_face_i, face_dict_i = lforme_i.getBottomVertices()
            lforme_iBottomIdx = lforme_i.getBottomSurfaceIndex()
            if len(placed_list)!=0:
                for composant_j in placed_list:
                    if csv_dict["Item_Geometry"][composant_j] == "boite":
                        # L-B
                        # TODO:
                        boite_j = Boite(tuple(X[composant_j]),
                                        csv_dict["Param1"][composant_j], csv_dict["Param2"][composant_j],
                                        csv_dict["Param3"][composant_j])
                        boite_j.rotation3d(*rotation3d_dict[P[composant_j]])
                        _, face_dict_j = boite_j.getTopVertices()

                        vertices_j = face_dict_j["0"]

                        if lforme_iBottomIdx == 0 or lforme_iBottomIdx == 4:
                            vertices_i = face_dict_i["0"]
                            ret, _ = computeOverlap2dByVertices(vertices_i, vertices_j)
                            if ret:
                                z_record_list.append(np.max(np.array(vertices_j)[:,2]))
                        elif lforme_iBottomIdx == 1 or lforme_iBottomIdx == 3:
                            vertices_i_0 = face_dict_i["0"]
                            ret, _ = computeOverlap2dByVertices(vertices_i_0, vertices_j)
                            if ret:
                                z_record_list.append(np.max(np.array(vertices_j)[:,2]))
                            else:
                                vertices_i_1 = face_dict_i["1"]
                                ret, _ = computeOverlap2dByVertices(vertices_i_1, vertices_j)
                                if ret:
                                    z_record_list.append(np.max(np.array(vertices_j)[:,2]))
                        elif lforme_iBottomIdx == 2 or lforme_iBottomIdx == 5:
                            vertices_i_0 = face_dict_i["0"]
                            ret, _ = computeOverlap2dByVertices(vertices_i_0, vertices_j)
                            if ret:
                                # if face 2 or 5 of composant i have overlap with composant j
                                z_record_list.append(np.max(np.array(vertices_j)[:, 2]))
                            else:
                                vertices_i_1 = face_dict_i["1"]
                                ret, _ = computeOverlap2dByVertices(vertices_i_1, vertices_j)
                                if ret:
                                    actual_height = np.max(np.array(vertices_j)[:, 2])
                                    equivalent_height = actual_height - abs(
                                        np.max(np.array(vertices_i_0)[:, 2]) - np.max(np.array(vertices_i_1)[:,2])
                                    )
                                    z_record_list.append(equivalent_height)
                    elif csv_dict["Item_Geometry"][composant_j] == "lforme":
                        # L-L
                        lforme_j = LForme(tuple(X[composant_j]),
                                                csv_dict["Param1"][composant_j], csv_dict["Param2"][composant_j],
                                                csv_dict["Param3"][composant_j], csv_dict["Param4"][composant_j],
                                                csv_dict["Param5"][composant_j])
                        lforme_j.rotation3d(*rotation3d_dict[P[composant_j]])
                        num_face_j, face_dict_j = lforme_j.getTopVertices()
                        lforme_jBottomIdx = lforme_j.getBottomSurfaceIndex()
                        if lforme_iBottomIdx == 0 or lforme_iBottomIdx == 4:
                            vertices_i = face_dict_i["0"]
                            if lforme_jBottomIdx == 0 or lforme_jBottomIdx == 4:
                                vertices_j_0 = face_dict_j["0"]
                                ret, _ = computeOverlap2dByVertices(vertices_i, vertices_j_0)
                                if ret:
                                    z_record_list.append(np.max(np.array(vertices_j_0)[:, 2]))
                                else:
                                    vertices_j_1 = face_dict_j["1"]
                                    ret, _ = computeOverlap2dByVertices(vertices_i, vertices_j_1)
                                    if ret:
                                        z_record_list.append(np.max(np.array(vertices_j_1)[:, 2]))
                            elif lforme_jBottomIdx == 2 or lforme_jBottomIdx == 5:
                                vertices_j = face_dict_j["0"]
                                ret, _ = computeOverlap2dByVertices(vertices_i, vertices_j)
                                if ret:
                                    z_record_list.append(np.max(np.array(vertices_j)[:, 2]))
                            elif lforme_jBottomIdx == 1 or lforme_jBottomIdx == 3:
                                vertices_j_0 = face_dict_j["0"]
                                ret, _ = computeOverlap2dByVertices(vertices_i, vertices_j_0)
                                if ret:
                                    z_record_list.append(np.max(np.array(vertices_j_0)[:, 2]))
                                else:
                                    vertices_j_1 = face_dict_j["1"]
                                    ret, _ = computeOverlap2dByVertices(vertices_i, vertices_j_1)
                                    if ret:
                                        z_record_list.append(np.max(np.array(vertices_j_1)[:, 2]))
                        elif lforme_iBottomIdx == 2 or lforme_iBottomIdx == 5:
                            vertices_i_0 = face_dict_i["0"]
                            vertices_i_1 = face_dict_i["1"]


                            if lforme_jBottomIdx == 0 or lforme_jBottomIdx == 4:
                                vertices_j_0 = face_dict_j["0"]
                                vertices_j_1 = face_dict_j["1"]
                                ret00, _ = computeOverlap2dByVertices(vertices_i_0, vertices_j_0)
                                ret01, _ = computeOverlap2dByVertices(vertices_i_0, vertices_j_1)
                                ret10, _ = computeOverlap2dByVertices(vertices_i_1, vertices_j_0)
                                ret11, _ = computeOverlap2dByVertices(vertices_i_1, vertices_j_1)
                                # sub cases are not completely seperated, so we do not use "elif" but use "if"
                                if ret00:
                                    z_record_list.append(np.max(np.array(vertices_j_0)[:, 2]))
                                if ret01:
                                    z_record_list.append(np.max(np.array(vertices_j_1)[:, 2]))
                                if ret10:
                                    actual_height = np.max(np.array(vertices_j_0)[:, 2])
                                    equivalent_height = actual_height - abs(
                                        np.max(np.array(vertices_i_0)[:, 2]) - np.max(np.array(vertices_i_1)[:, 2])
                                    )
                                    z_record_list.append(equivalent_height)
                                if ret11:
                                    actual_height = np.max(np.array(vertices_j_1)[:,2])
                                    equivalent_height = actual_height - abs(
                                        np.max(np.array(vertices_i_0)[:, 2]) - np.max(np.array(vertices_i_1)[:, 2])
                                    )
                                    z_record_list.append(equivalent_height)
                            elif lforme_jBottomIdx == 2 or lforme_jBottomIdx == 5:
                                vertices_j = face_dict_j["0"]
                                ret, _ = computeOverlap2dByVertices(vertices_i_0, vertices_j)
                                if ret:
                                    z_record_list.append(np.max(np.array(vertices_j)[:, 2]))
                                else:
                                    ret, _ = computeOverlap2dByVertices(vertices_i_1, vertices_j)
                                    if ret:
                                        actual_height = np.max(np.array(vertices_j)[:, 2])
                                        equivalent_height = actual_height - abs(
                                            np.max(np.array(vertices_i_0)[:, 2]) - np.max(np.array(vertices_i_1)[:, 2])
                                        )
                                        z_record_list.append(equivalent_height)

                            elif lforme_jBottomIdx == 1 or lforme_jBottomIdx == 3:
                                vertices_j_0 = face_dict_j["0"]
                                vertices_j_1 = face_dict_j["1"]
                                ret00, _ = computeOverlap2dByVertices(vertices_i_0, vertices_j_0)
                                ret01, _ = computeOverlap2dByVertices(vertices_i_0, vertices_j_1)
                                ret10, _ = computeOverlap2dByVertices(vertices_i_1, vertices_j_0)
                                ret11, _ = computeOverlap2dByVertices(vertices_i_1, vertices_j_1)
                                if ret00 or ret01:
                                    z_record_list.append(np.max(np.array(vertices_j_0)[:, 2]))
                                if ret10 or ret11:
                                    actual_height = np.max(np.array(vertices_j_0)[:, 2])
                                    equivalent_height = actual_height - abs(
                                        np.max(np.array(vertices_i_0)[:, 2]) - np.max(np.array(vertices_i_1)[:, 2])
                                    )
                                    z_record_list.append(equivalent_height)


                        elif lforme_iBottomIdx == 1 or lforme_iBottomIdx ==3:
                            vertices_i_0 = face_dict_i["0"]
                            vertices_i_1 = face_dict_i["1"]
                            if lforme_jBottomIdx == 0 or lforme_jBottomIdx == 4:
                                vertices_j_0 = face_dict_j["0"]
                                vertices_j_1 = face_dict_j["1"]
                                ret00, _ = computeOverlap2dByVertices(vertices_i_0, vertices_j_0)
                                ret01, _ = computeOverlap2dByVertices(vertices_i_0, vertices_j_1)
                                ret10, _ = computeOverlap2dByVertices(vertices_i_1, vertices_j_0)
                                ret11, _ = computeOverlap2dByVertices(vertices_i_1, vertices_j_1)

                                if ret00 or ret10:
                                    z_record_list.append(np.max(np.array(vertices_j_0)[:,2]))
                                elif ret01 or ret11:
                                    z_record_list.append(np.max(np.array(vertices_j_1)[:, 2]))

                            elif lforme_jBottomIdx == 2 or lforme_jBottomIdx == 5:
                                vertices_j = face_dict_j["0"]
                                ret, _ = computeOverlap2dByVertices(vertices_i_0, vertices_j)
                                if ret:
                                    z_record_list.append(np.max(np.array(vertices_j)[:, 2]))
                                else:
                                    ret, _ = computeOverlap2dByVertices(vertices_i_1, vertices_j)
                                    if ret:
                                        z_record_list.append(np.max(np.array(vertices_j)[:, 2]))


                            elif lforme_jBottomIdx == 1 or lforme_jBottomIdx == 3:
                                vertices_j_0 = face_dict_j["0"]
                                vertices_j_1 = face_dict_j["1"]
                                ret00, _ = computeOverlap2dByVertices(vertices_i_0, vertices_j_0)
                                ret01, _ = computeOverlap2dByVertices(vertices_i_0, vertices_j_1)
                                ret10, _ = computeOverlap2dByVertices(vertices_i_1, vertices_j_0)
                                ret11, _ = computeOverlap2dByVertices(vertices_i_1, vertices_j_1)
                                if ret00 or ret10 or ret10 or ret11:
                                    z_record_list.append(np.max(np.array(vertices_j_0)[:, 2]))
                if len(z_record_list) == 0:
                    z_last_composant = 0
                else:
                    z_last_composant = np.max(z_record_list)
            z_i = z_last_composant + (lforme_i.getTopSurfaceHeight() - lforme_i.getBottomSurfaceHeight())/2.0
            X[composant_i, 2] = z_i
            placed_list.append(composant_i)


    return X


def translateSubDNA(sub_dna, longeur, dna_size=10):
    """
    create a number in [0, length]
    :param sub_dna: a binary 1-d array
    :param length:
    :return: a number in [0, length]
    """
    res = sub_dna.dot(2 ** np.arange(dna_size)[::-1])
    res = res / float(2 ** dna_size - 1)
    return res * longeur


def translateDNA(pop, length, width, height, dna_size=10):
    X_all = np.zeros((pop.shape[0], pop.shape[1], 3))
    P_all = np.zeros((pop.shape[0], pop.shape[1]))
    for i in range(pop.shape[0]):
        X_dna_i = pop[i]
        X_dna_i_x = X_dna_i[:, :dna_size]
        X_dna_i_y = X_dna_i[:, dna_size:2 * dna_size]
        X_dna_i_z = X_dna_i[:, 2 * dna_size:3 * dna_size]
        P_dna_i = X_dna_i[:, 3 * dna_size:4 * dna_size]

        X_i_x = translateSubDNA(X_dna_i_x, length, dna_size=dna_size)
        X_i_y = translateSubDNA(X_dna_i_y, width, dna_size=dna_size)
        X_i_z = translateSubDNA(X_dna_i_z, height, dna_size=dna_size)

        P_i = translateSubDNA(P_dna_i, 64, dna_size=dna_size)
        P_i = P_i.astype(np.int)
        # make sure we can obtain a number between 0 and 63 with equal probability
        P_i[np.where(P_i == 64)] = 63
        X_i = np.transpose(np.vstack((X_i_x, X_i_y, X_i_z)))
        X_all[i] = X_i
        P_all[i] = P_i

    return X_all, P_all


def identify_pareto(scores, pop_ids):
    """
    chercher les points situe au pareto front
    :param scores:
    :return: les index des points
    """
    pop_size = scores.shape[0]
    # on suppose tous les points sont dans pareto front
    belong_pareto_front = np.ones(pop_size, dtype=bool)
    for i in range(pop_size):
        for j in range(pop_size):
            if all(scores[j] <= scores[i]) and any(scores[j] < scores[i]):
                # si un point i n'est pas dans pareto front, on le supprime.
                belong_pareto_front[i] = 0
                break
    return pop_ids[belong_pareto_front]


def calculate_crowding(scores):

    pop_size = scores.shape[0]
    scores_dims = scores.shape[1]

    # create crowding matrix of population (row) and score (column)
    crowding_matrix = np.zeros((pop_size, scores_dims))

    # normalise scores (ptp is max-min)
    normed_scores = (scores - scores.min(0)) / (scores.ptp(0)+1e-6)

    # calculate crowding distance for each score in turn
    for col in range(scores_dims):
        crowding = np.zeros(pop_size)

        # end points have maximum crowding
        crowding[0] = 1
        crowding[pop_size - 1] = 1

        # Sort each score (to calculate crowding between adjacent scores)
        sorted_scores = np.sort(normed_scores[:, col])

        sorted_scores_index = np.argsort(
            normed_scores[:, col])

        # Calculate crowding distance for each individual
        crowding[1:pop_size - 1] = sorted_scores[2:pop_size] - sorted_scores[0:pop_size - 2]

        # resort to orginal order (two steps)
        re_sort_order = np.argsort(sorted_scores_index)
        sorted_crowding = crowding[re_sort_order]

        # Record crowding distances
        crowding_matrix[:, col] = sorted_crowding

    # Sum crowding distances of each score
    crowding_distances = np.sum(crowding_matrix, axis=1)

    return crowding_distances


def reduce_by_crowding(scores, number_to_select):
    """
    This function selects a number of solutions based on tournament of
    crowding distances. Two members of the population are picked at
    random. The one with the higher croding dostance is always picked
    """
    pop_ids = np.arange(scores.shape[0])
    crowding_distances = calculate_crowding(scores)
    picked_pop_ids = np.zeros((number_to_select))
    picked_scores = np.zeros((number_to_select, scores.shape[1]))

    for i in range(number_to_select):

        pop_size = pop_ids.shape[0]

        fighter1ID = np.random.randint(0, pop_size)
        fighter2ID = np.random.randint(0, pop_size)

        # If fighter # 1 is better
        if crowding_distances[fighter1ID] >= crowding_distances[
            fighter2ID]:

            # add solution to picked solutions array
            picked_pop_ids[i] = pop_ids[
                fighter1ID]

            # Add score to picked scores array
            picked_scores[i, :] = scores[fighter1ID, :]

            # remove selected solution from available solutions
            pop_ids = np.delete(pop_ids, (fighter1ID), axis=0)

            scores = np.delete(scores, (fighter1ID), axis=0)

            crowding_distances = np.delete(crowding_distances, (fighter1ID), axis=0)
        else:
            picked_pop_ids[i] = pop_ids[fighter2ID]

            picked_scores[i, :] = scores[fighter2ID, :]

            pop_ids = np.delete(pop_ids, (fighter2ID), axis=0)

            scores = np.delete(scores, (fighter2ID), axis=0)

            crowding_distances = np.delete(
                crowding_distances, (fighter2ID), axis=0)

    # Convert to integer
    picked_pop_ids = np.asarray(picked_pop_ids, dtype=int)

    return picked_pop_ids

def build_pareto_population(pop, scores, pop_size):
    """

    :param pop:
    :param scores:
    :return:
    """
    unselected_pop_ids = np.arange(pop.shape[0])
    all_pop_ids = np.arange(pop.shape[0])
    pareto_front_ids = np.array([])
    while len(pareto_front_ids) < pop_size:
        temp_pareto_front_ids = identify_pareto(scores[unselected_pop_ids],
                                                unselected_pop_ids)
        # check size of total pareto front if larger than pop_size, reduce new pareto front by crowding
        combined_pareto_size = len(pareto_front_ids) + len(temp_pareto_front_ids)
        # print("len_pareto_front_ids:{}, len_pareto_front_ids:{}".format(len(pareto_front_ids), len(temp_pareto_front_ids)))
        if combined_pareto_size > pop_size:
            number_to_select = pop_size - len(pareto_front_ids)
            selected_individus = reduce_by_crowding(scores[temp_pareto_front_ids], number_to_select)
            temp_pareto_front_ids = temp_pareto_front_ids[selected_individus]

        # Add latest pareto front to full Pareto front
        pareto_front_ids = np.hstack((pareto_front_ids, temp_pareto_front_ids))

        # Update unslected population ID by using sets to find IDs in all
        # ids that are not in the selected front
        unselected_set = set(all_pop_ids) - set(pareto_front_ids)
        unselected_pop_ids = np.array(list(unselected_set))

    # convert to integer
    pop = pop[pareto_front_ids.astype(int)]
    return pop, pareto_front_ids.astype(int)

def select_by_tournament(scores, number_to_select):
    """
    This function selects a number of solutions based on tournament
    select solutions with minor score
    score_array: 1d-array
    """
    pop_ids = np.arange(scores.shape[0])
    picked_pop_ids = np.zeros((number_to_select))
    picked_scores = np.zeros((number_to_select))

    for i in range(number_to_select):

        pop_size = pop_ids.shape[0]

        fighter1ID = np.random.randint(0, pop_size)
        fighter2ID = np.random.randint(0, pop_size)

        # If fighter # 1 is better
        if scores[fighter1ID] <= scores[fighter2ID]:

            # add solution to picked solutions array
            picked_pop_ids[i] = pop_ids[fighter1ID]

            # Add score to picked scores array
            picked_scores[i] = scores[fighter1ID]

            # remove selected solution from available solutions
            pop_ids = np.delete(pop_ids, (fighter1ID), axis=0)

            scores = np.delete(scores, (fighter1ID), axis=0)

        else:
            picked_pop_ids[i] = pop_ids[fighter2ID]

            picked_scores[i] = scores[fighter2ID]

            pop_ids = np.delete(pop_ids, (fighter2ID), axis=0)

            scores = np.delete(scores, (fighter2ID), axis=0)


    # Convert to integer
    picked_pop_ids = np.asarray(picked_pop_ids, dtype=int)

    return picked_pop_ids

def respect_orientation_req(individus, csv_dict, dna_size=10):
    nb_composant = individus.shape[0]
    P_dna = individus[:, 3*dna_size:4*dna_size]
    P = translateSubDNA(P_dna, 64, dna_size=dna_size)
    P = P.astype(np.int)
    # make sure we can obtain a number between 0 and 63 with equal probability
    P[np.where(P == 64)] = 63

    for i in range(nb_composant):
        if csv_dict["Bottom_surface_No"][i] != -1:
            # -1 means no orientaion requirements
            if csv_dict["Item_Geometry"][i].lower() == "boite":
                boite = Boite((0,0,0), csv_dict["Param1"][i], csv_dict["Param2"][i], csv_dict["Param3"][i])
                boite.rotation3d(*rotation3d_dict[P[i]])
                currentBottomSurface = boite.getBottomSurfaceIndex()
                if currentBottomSurface != csv_dict["Bottom_surface_No"][i]:
                    return False
            if csv_dict["Item_Geometry"][i].lower() == "cylinder":
                cylinder = Cylinder((0,0,0), csv_dict["Param1"][i], csv_dict["Param2"][i], csv_dict["Param3"][i])
                cylinder.rotation3d(*rotation3d_dict[P[i]])
                currentBottomSurface = cylinder.getBottomSurfaceIndex()
                if currentBottomSurface != csv_dict["Bottom_surface_No"][i]:
                    return False
            if csv_dict["Item_Geometry"][i].lower() == "lforme":
                lforme = LForme((0,0,0), csv_dict["Param1"][i], csv_dict["Param2"][i], csv_dict["Param3"][i],csv_dict["Param4"][i], csv_dict["Param5"][i])
                lforme.rotation3d(*rotation3d_dict[P[i]])
                currentBottomSurface = lforme.getBottomSurfaceIndex()
                if currentBottomSurface != csv_dict["Bottom_surface_No"][i]:
                    return False

    return True

def timer(func):
    """Print the runtime of the decorated function"""
    @functools.wraps(func)
    def wrapper(*args, **kwargs):
        start = time.time()
        value = func(*args, **kwargs)
        end = time.time()
        run_time = end - start
        log.debug("Time by using function {}: {}s".format(func.__name__, run_time))
        return value
    return wrapper

@timer
def initialize_population(pop_size, nb_composant, csv_dict, dna_size=10):
    pop = np.zeros((pop_size, nb_composant, dna_size*4))

    for i in range(pop_size):
        pop_i = np.random.randint(2, size=(nb_composant, dna_size * 4))
        while(not respect_orientation_req(pop_i, csv_dict, dna_size=dna_size)):
            pop_i = np.random.randint(2, size=(nb_composant, dna_size * 4))
        pop[i] = pop_i

    return pop


def drawPlacementOnAxes(X, P, C, csv_dict, ax, composants_color_list):
    camion = Camion.atOrigine(C[0], C[1], C[2])

    ax.cla()
    ax.set_xlim(0, C[0])
    ax.set_ylim(0, C[1])
    ax.set_zlim(0, C[2])
    ax.add_collection3d(camion.getPoly3d(facecolors=(1, 1, 1, 0), edgecolors="black"))
    nb_composant = X.shape[0]
    for i in range(nb_composant):
        ax.text(*X[i, :], str(i), 'x')
        if csv_dict["Item_Geometry"][i].lower() == "cylinder":
            cylinder = Cylinder(tuple(X[i]), csv_dict["Param1"][i], csv_dict["Param2"][i], csv_dict["Param3"][i])
            cylinder.rotation3d(*rotation3d_dict[P[i]])
            cylinder.drawCylinderOnAxes(ax, facecolors=composants_color_list[i])
        if csv_dict["Item_Geometry"][i].lower() == "boite":
            boite = Boite(tuple(X[i]), csv_dict["Param1"][i], csv_dict["Param2"][i], csv_dict["Param3"][i])
            boite.rotation3d(*rotation3d_dict[P[i]])
            ax.add_collection3d(boite.getPoly3d(facecolors=composants_color_list[i]))
        if csv_dict["Item_Geometry"][i].lower() == "lforme":
            lforme = LForme(tuple(X[i]), csv_dict["Param1"][i], csv_dict["Param2"][i], csv_dict["Param3"][i],
                            csv_dict["Param4"][i], csv_dict["Param5"][i])
            lforme.rotation3d(*rotation3d_dict[P[i]])
            ax.add_collection3d(lforme.getPoly3d(facecolors=composants_color_list[i]))
    plt.pause(0.1)

def get_overload_value(X, B, composants_weight, load_bearing_capacity, len_ignore=0.0):
    X_copy = X.copy()
    upper_surface_list = X_copy[:, 2] + B[:, 2] / 2.0
    z_set = set(upper_surface_list)
    placement_order = []
    while (len(z_set) != 0):
        lowest_z = min(z_set)
        idx = upper_surface_list == lowest_z
        msg_str = []
        for i, j in enumerate(list(idx)):
            if j == True:
                msg_str.append(i)
        #         print("lowest z: {}, Number of lowest composant: {}".format(lowest_z, msg_str))
        placement_order.extend(msg_str)
        z_set.remove(lowest_z)

    # print("Placement order: ", placement_order)

    placed_list = []
    # load_bearing_composants ids
    load_bearing_list = [None for i in range(len(placement_order))]
    # load_bearing area
    load_bearing_area_list = [None for i in range(len(placement_order))]

    for composant_i in placement_order:
        if len(placed_list) != 0:
            overlap_list = []
            overlap_area_list = []
            for composant_j in placed_list:
                ret, overlap_area, overlap_length, overlap_width = overlap2d(X_copy[composant_i], B[composant_i, 0], B[composant_i, 1],
                                              X_copy[composant_j], B[composant_j, 0], B[composant_j, 1])
                if (ret and overlap_length>len_ignore and overlap_width>len_ignore):
                    overlap_list.append(composant_j)
                    overlap_area_list.append(overlap_area)

            if len(overlap_list) != 0:
                highest_z = np.max(upper_surface_list[overlap_list])
                idx = upper_surface_list[overlap_list] == highest_z
                load_bearing_list[composant_i] = list(np.array(overlap_list)[idx])
                load_bearing_area_list[composant_i] = list(np.array(overlap_area_list)[idx])

        placed_list.append(composant_i)

    # print("load_bearing list: ", load_bearing_list)
    # print("load_bearing_area_list: ", load_bearing_area_list)
    load_bearing_weights = np.zeros((X_copy.shape[0]))
    placement_order.reverse()
    for composant_i in placement_order:
        # if exist one or more composants support composant i
        if load_bearing_list[composant_i] != None:
            for i, composant_j in enumerate(load_bearing_list[composant_i]):
                total_overlap_area = np.sum(np.array(load_bearing_area_list[composant_i]))
                # something like pressure
                load_bearing_weight_of_j = ( composants_weight[composant_i] + load_bearing_weights[composant_i]) * \
                                           (load_bearing_area_list[composant_i][i] / total_overlap_area)
                load_bearing_weights[composant_j] = load_bearing_weights[composant_j] + load_bearing_weight_of_j

    # print("load_bearing_weights: ", load_bearing_weights)
    # print("load_bearing_capacity: ", load_bearing_capacity)

    res = 0
    if np.any(load_bearing_weights > load_bearing_capacity):
        not_meet_requirements_idx = load_bearing_weights > load_bearing_capacity
        res = np.sum(load_bearing_weights[not_meet_requirements_idx] - load_bearing_capacity[not_meet_requirements_idx])
        return res
    else:
        return res

def F(X, B, C, w_pen=1.0, w_pro=1.0):
    """
    fonction objectifs
    :param X:  matrice de positions des centres des boites
    :param B: matrices de dimensions des boites
    :return:
    """
    m = X.shape[0]
    d = X.shape[1]
    part_penetration = 0
    part_protrusion = 0


    for i in range(m - 1):
        for j in range(i + 1, m):
            Gamma_ij = 1
            Delta_ij_list = []
            for k in range(d):
                Gamma_ij = Gamma_ij * max(0.0, min(X[i, k] + B[i, k] / 2.0, X[j, k] + B[j, k] / 2.0) -
                                          max(X[i, k] - B[i, k] / 2.0, X[j, k] - B[j, k] / 2.0))
                Delta_ij_list.append(max(0.0, (B[i, k] + B[j, k]) / 2.0 - abs(X[i, k] - X[j, k])))
            Delta_ij = min(Delta_ij_list)

            part_penetration = part_penetration + Gamma_ij * Delta_ij
            # print("Partie penetration entre composant {} et {}: {}".format(i, j, Gamma_ij*Delta_ij))

    for i in range(m):
        Gamma_bar_part1 = 1
        Gamma_bar_part2 = 1
        Delta_bar_list_i = []
        for k in range(d):
            Gamma_bar_part1 = Gamma_bar_part1 * B[i, k]
            Gamma_bar_part2 = Gamma_bar_part2 * max(0.0, min(C[k], X[i, k] + B[i, k] / 2.0) - max(0.0, X[i, k] - B[
                i, k] / 2.0))

            Delta_bar_list_i.append(max(0.0, abs(X[i, k] - C[k] / 2.0) + (B[i, k] - C[k]) / 2.0))

        Gamma_bar_i = Gamma_bar_part1 - Gamma_bar_part2
        # print("composant {}: {}".format(i, Gamma_bar_i))
        Delta_bar_i = max(Delta_bar_list_i)

        part_protrusion = part_protrusion + Gamma_bar_i * Delta_bar_i
        # print("Partie protrusion de composant {}: {}".format(i, Gamma_bar_i*Delta_bar_i))


    return w_pen * part_penetration + w_pro * part_protrusion


def convert2Prime(X,P,  csv_dict):
    # TODO:

    nb_composant = X.shape[0]
    X_prime_list = []
    B_prime_list = []
    composants_weight_prime_list = []
    load_bearing_capacity_prime_list = []
    for i in range(nb_composant):
        if csv_dict["Item_Geometry"][i] == "boite":
            boite = Boite(tuple(X[i]),
                          csv_dict["Param1"][i], csv_dict["Param2"][i],
                          csv_dict["Param3"][i])
            boite.rotation3d(*rotation3d_dict[P[i]])
            center_i, L, W, H = boite.getCurrentLWH()
            X_prime_list.append(np.array(center_i))
            B_prime_list.append(np.array([L, W, H]))
            composant_weight = csv_dict["Weight"][i]
            load_bearing_capacity = csv_dict["Load_bearing_capacity"][i]
            composants_weight_prime_list.append(composant_weight)
            load_bearing_capacity_prime_list.append(load_bearing_capacity)
            
        elif csv_dict["Item_Geometry"][i] == "lforme":
            lforme = LForme(tuple(X[i]),
                              csv_dict["Param1"][i], csv_dict["Param2"][i],
                              csv_dict["Param3"][i], csv_dict["Param4"][i],
                              csv_dict["Param5"][i])
            lforme.rotation3d(*rotation3d_dict[P[i]])
            center_i0, L0, W0, H0 = lforme.getCurrentLWH_VerticalBoite0()
            X_prime_list.append(np.array(center_i0))
            B_prime_list.append(np.array([L0, W0, H0]))
            center_i1, L1, W1, H1 = lforme.getCurrentLWH_VerticalBoite1()
            X_prime_list.append(np.array(center_i1))
            B_prime_list.append(np.array([L1, W1, H1]))
            # we distribute their weights and load bearing capacity according to their volume.
            composant_weight = csv_dict["Weight"][i]
            load_bearing_capacity = csv_dict["Load_bearing_capacity"][i]
            composant_weight0 = composant_weight*(L0*W0*H0)/((L0*W0*H0)+(L1*W1*H1))
            load_bearing_capacity0 = load_bearing_capacity*(L0*W0*H0)/((L0*W0*H0)+(L1*W1*H1))
            composant_weight1 = composant_weight - composant_weight0
            load_bearing_capacity1 = load_bearing_capacity- load_bearing_capacity0
            composants_weight_prime_list.append(composant_weight0)
            composants_weight_prime_list.append(composant_weight1)
            load_bearing_capacity_prime_list.append(load_bearing_capacity0)
            load_bearing_capacity_prime_list.append(load_bearing_capacity1)

    X_prime = np.array(X_prime_list)
    B_prime = np.array(B_prime_list)
    composants_weight_prime = np.array(composants_weight_prime_list)
    load_bearing_capacity_prime = np.array(load_bearing_capacity_prime_list)
    # log.debug("X_prime = {}".format(X_prime))
    # log.debug("B_prime = {}".format(B_prime))
    # log.debug("composants_weight_prime = {}".format(composants_weight_prime))
    # log.debug("load_bearing_capacity_prime = {}".format(load_bearing_capacity_prime))
    return X_prime, B_prime, composants_weight_prime, load_bearing_capacity_prime


def compute_overloaded_values(X_all, P_all, csv_dict):
    """
    Compute overlaaded values when giving a set of placements
    :param X_all: set of center coordinates, it should be obtained by applying translateDNA on a population
    :param P_all: set of rotation manner, it should be obtained by applying translateDNA on a population
    :param csv_dict: dictionary which saved all parameters of composants.
    :return: overloaded values, 1d-array
    """
    pop_size = X_all.shape[0]
    overloaded_values = np.zeros((pop_size))
    for i in range(pop_size):
        X_i = X_all[i]
        P_i = P_all[i]
        X_i = addGravity(X_i, P_i, csv_dict)
        # drawPlacement(X_i, P_i, np.array([12, 12, 12]), csv_dict)
        X_prime, B_prime, composants_weight_prime, load_bearing_capacity_prime = convert2Prime(X_i, P_i, csv_dict)
        overloaded_value = get_overload_value(X_prime, B_prime, composants_weight_prime, load_bearing_capacity_prime)
        overloaded_values[i] = overloaded_value

    # log.debug("Overloaded_values: {}".format(overloaded_values))
    return overloaded_values

def compute_F_values(X_all, P_all, C, csv_dict):
    pop_size = X_all.shape[0]
    F_values = np.zeros((pop_size))
    for i in range(pop_size):
        X_i = X_all[i]
        P_i = P_all[i]
        X_i = addGravity(X_i, P_i, csv_dict)
        X_prime, B_prime, composants_weight_prime, load_bearing_capacity_prime = convert2Prime(X_i, P_i, csv_dict)
        F_value = F(X_prime, B_prime, C)
        F_values[i] = F_value
    # log.debug("F_values: {}".format(F_values))
    return F_values

def compute_vide_ratio(X, B):
    """
    compute vide ratio, attention: this version haven't considered intersection between composants.
    :param X:
    :param B:
    :param C:
    :return:
    """
    m = X.shape[0]
    d = X.shape[1]
    V_intersection = 0
    V_total = 0

    for i in range(m - 1):
        for j in range(i + 1, m):
            Gamma_ij = 1
            for k in range(d):
                Gamma_ij = Gamma_ij * max(0.0, min(X[i, k] + B[i, k] / 2.0, X[j, k] + B[j, k] / 2.0) -
                                          max(X[i, k] - B[i, k] / 2.0, X[j, k] - B[j, k] / 2.0))

            V_intersection = V_intersection + Gamma_ij
    for i in range(m):
        V_one_composant = 1
        for k in range(d):
            V_one_composant = V_one_composant * B[i, k]
        V_total = V_total + V_one_composant

    # compute V_c
    B_c = np.max(X+B/2, axis = 0) - np.min(X-B/2, axis=0)
    V_c = 1
    for k in range(d):
        V_c = V_c*B_c[k]
    vide_ratio = 0
    vide_ratio = 1 - (V_total-V_intersection)/(V_c)
    return vide_ratio




def compute_contact_area(X, B, C, len_ignore=0.0):
    """
    fonction objectifs: compute all contact area between one composant and another,
    and add contact area between composants and lower surface of container.
    """
    X_copy = X.copy()
    B_copy = B.copy()

    ## suppose lower surface of container is a composant with height equal to 0
    X_copy = np.vstack((np.array([C[0] / 2, C[1] / 2, 0]), X_copy))
    B_copy = np.vstack((np.array([C[0], C[1], 0]), B_copy))
    m = X_copy.shape[0]
    d = X_copy.shape[1]
    contact_area_total = 0
    for i in range(m - 1):
        for j in range(i + 1, m):
            contact_area_length_list = []
            for k in range(d):
                contact_area_length = min(X_copy[i, k] + B_copy[i, k] / 2.0, X_copy[j, k] + B_copy[j, k] / 2.0) - \
                                          max(X_copy[i, k] - B_copy[i, k] / 2.0, X_copy[j, k] - B_copy[j, k] / 2.0)
                contact_area_length_list.append(contact_area_length)
            contact_area = 0
            if (np.all(np.array(contact_area_length_list) >= 0)):
                # if two composant contact

                contact_area_length_list = list(filter(lambda a: a >len_ignore, contact_area_length_list))
                # if any length less than len_ignore, we think that they not contact each other,
                if (len(contact_area_length_list) == 2):
                    contact_area = contact_area_length_list[0] * contact_area_length_list[1]
                contact_area_total += contact_area
    return contact_area_total


def computeFAndOverloadedValues(X_all, P_all, C, csv_dict):
    pop_size = X_all.shape[0]
    F_values = np.zeros((pop_size))
    overloaded_values = np.zeros((pop_size))
    for i in range(pop_size):
        X_i = X_all[i]
        P_i = P_all[i]
        X_i = addGravity(X_i, P_i, csv_dict)
        X_all[i] = X_i
        X_prime, B_prime, composants_weight_prime, load_bearing_capacity_prime = convert2Prime(X_i, P_i, csv_dict)
        F_value = F(X_prime, B_prime, C)
        overloaded_value = get_overload_value(X_prime, B_prime, composants_weight_prime, load_bearing_capacity_prime)
        overloaded_values[i] = overloaded_value
        F_values[i] = F_value
    # log.debug("F_values: {}".format(F_values))
    return F_values, overloaded_values, X_all

def compute_scores(X_all, P_all, C, csv_dict):
    pop_size = X_all.shape[0]
    scores = np.zeros((pop_size, 2))
    for i in range(pop_size):
        X_i = X_all[i]
        P_i = P_all[i]
        X_i = addGravity(X_i, P_i, csv_dict)
        X_all[i] = X_i
        X_prime, B_prime, composants_weight_prime, load_bearing_capacity_prime = convert2Prime(X_i, P_i, csv_dict)
        contact_area = -compute_contact_area(X_prime,B_prime,C, len_ignore=0)
        vide_values = compute_vide_ratio(X_prime, B_prime)
        scores[i, 0] = contact_area
        scores[i, 1] = vide_values
    return scores
def crossover(parent, pop, cross_rate=0.8):
    if np.random.rand() < cross_rate:
        # si il y a des croissement avec individus idx
        idx = np.random.randint(0, pop.shape[0], size=1)
        cross_points = np.random.randint(0, 2, size=(pop.shape[1], pop.shape[2])).astype(np.bool)
        parent[cross_points] = pop[idx, cross_points]
    return parent


def mutate(child, mutation_rate=3e-4):
    for i in range(child.shape[0]):
        for j in range(child.shape[1]):
            if np.random.rand() < mutation_rate:
                child[i, j] = 1 - child[i, j]
    return child

@timer
def create_new_generation(pop, iteration, csv_dict, dna_size=10, no_less_than=400, no_more_than=500, mutation_rate=None):
    log.debug("Begin creating new generation ...")
    while(pop.shape[0]<no_less_than):
        pop_copy = pop.copy()
        for parent in pop:
            # croissement entre les parents
            child = crossover(parent, pop)
            pop_copy = np.append(pop_copy, np.expand_dims(child, axis=0), axis=0)
        # supprimer les individus qui sont pareils. maintenant pop_size peut etre n'est pas pop_size
        pop = np.unique(pop_copy, axis=0)
        shuffle_idx = np.arange(pop.shape[0])
        np.random.shuffle(shuffle_idx)
        pop = pop[shuffle_idx]
        pop_list = []
        for child in pop:
            if mutation_rate == None:
                denominateur = int(iteration / 100)
                if denominateur == 0:
                    denominateur = 1
                else:
                    denominateur = denominateur
                child = mutate(child, mutation_rate=1e-3 / denominateur)
            else:
                child = mutate(child, mutation_rate=mutation_rate)
            if respect_orientation_req(child, csv_dict, dna_size=dna_size):
                pop_list.append(child)
        pop = np.array(pop_list)

    if pop.shape[0]>no_more_than:
        pop = pop[:no_more_than]
    log.debug("After creation of new generation, pop.shape = {}".format(pop.shape))
    return pop



def select_by_constraints(pop, parent_iteration, C, csv_dict, ax, composants_color_list,  dna_size=10, min_pop_size=250,
                          constraint_F=1e-3):
    _sub_iter = 0
    parent_start = time.time()
    while(True):
        break_flag = False
        start = time.time()
        log.info(">>>[Constraint test] sub_iteration: {}".format(_sub_iter))
        num_random = 0
        if _sub_iter == 0 and parent_iteration % 100 == 1 and parent_iteration != 0:
            # num_random = random.uniform(0, 1)
            num_random = 1
        if num_random > 0.5:
            pop = create_new_generation(pop, parent_iteration, csv_dict, no_less_than=300, no_more_than=500, mutation_rate=0.01)
        else:
            pop = create_new_generation(pop, parent_iteration, csv_dict, no_less_than=300, no_more_than=500)
        POP_SIZE = pop.shape[0]
        X_all, P_all = translateDNA(pop, C[0], C[1], C[2], dna_size=DNA_SIZE)

        # overloaded_values = compute_overloaded_values(X_all, P_all, csv_dict)
        # F_values = compute_F_values(X_all, P_all, C, csv_dict)
        F_values, overloaded_values, X_all = computeFAndOverloadedValues(X_all, P_all, C, csv_dict)

        meet_requirements_ids = np.logical_and(overloaded_values == 0, F_values < constraint_F)
        pop_meet_requireemnts = pop[meet_requirements_ids]
        F_values_meet_req = F_values[meet_requirements_ids]
        num_meet_requirements = np.sum(meet_requirements_ids.astype(int))
        log.debug("num_meet_load bearing requirements = {}".format(num_meet_requirements))
        if (num_meet_requirements < min_pop_size):
            not_meet_requirements_ids = np.logical_not(meet_requirements_ids)
            # log.debug(not_meet_requirements_ids)
            # log.debug(not_meet_requirements_ids.shape)
            # log.debug(F_values.shape)
            overloaded_values_not_meet_requirements = overloaded_values[not_meet_requirements_ids]
            F_values_not_meet_requirements = F_values[not_meet_requirements_ids]
            pop_not_meet_requirements = pop[not_meet_requirements_ids]
            scores = np.zeros((POP_SIZE - num_meet_requirements, 2))
            scores[:, 0] = overloaded_values_not_meet_requirements
            scores[:, 1] = F_values_not_meet_requirements

            log.debug("Scores[load_bearing, F_value] sample: \n{}".format(scores[:10]))

            pop_selected, next_generation_ids = build_pareto_population(pop_not_meet_requirements, scores,
                                                                        250 - num_meet_requirements)
            pop = np.append(pop_meet_requireemnts, pop_selected, axis=0)
        else:
            pop = pop_meet_requireemnts
            F_values = F_values_meet_req
            break_flag = True


        end = time.time()
        log.info("Time for one selection by constraints: {}s".format(end-start))
        parent_time_gap = end-parent_start
        minutes = int(parent_time_gap/60)
        seconds = parent_time_gap - minutes*60
        if minutes == 0:
            log.info("  [Parent_iteration = {}, sub_iteration = {}], spent time: {}s\n\n".format(parent_iteration,
                                                                                           _sub_iter, seconds))
        else:
            log.info("  [Parent_iteration = {}, sub_iteration = {}], spent time: {}min {}s\n\n".format(parent_iteration,
                                                                                                 _sub_iter,
                                                                                                 minutes,
                                                                                                 seconds))
        X_sample = X_all[0]
        P_sample = P_all[0]
        # X_sample = addGravity(X_sample, P_sample, csv_dict)
        drawPlacementOnAxes(X_sample, P_sample, C, csv_dict, ax, composants_color_list)
        _sub_iter = _sub_iter + 1
        if break_flag:
            break

    return pop





if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description="Camion_optimisation"
    )
    parser.add_argument("--debug", type=int, default=1,
                        help="whether show debug info")
    parser.add_argument("--load_npy", type=int, default=1,
                        help="whether load npy file")
    parser.add_argument("--container_length", type=float, default=12.0,
                        help="container's length")
    parser.add_argument("--container_width", type=float, default=12.0,
                        help="container's width")
    parser.add_argument("--container_height", type=float, default=12.0,
                        help="container's height")
    parser.add_argument("--container_npy_file", type=str, default="C.npy",
                        help="npy file path of container")
    parser.add_argument("--items_csv_file", type=str, default="colis.csv",
                        help="csv file path of items")
    parser.add_argument("--len_ignore", type=float, default=0.0,
                        help="length that can be ignored when considering overlap area")
    parser.add_argument("--pop_size", type=int, default=200,
                        help="population size that we keep during optimization process")
    parser.add_argument("--dna_size", type=int, default=10,
                        help="DNA length")
    parser.add_argument("--continue_training", type=int, default=1,
                        help="0 means we randomly initialize pop array and begin, 1 means we use the latest pop array")
    # parser.add_argument("--pop_npy_file", type=str, default=None,
    #                     help="When enable continue_training, we can chose pop npy file")
    parser.add_argument("--pop_npy_file", type=str, default="pop_init5.npy",
                        help="When enable continue_training, we can chose pop npy file")
    parser.add_argument("--pop_npy_dir", type=str, default=None,
                        help="folder which we save pop npy files, \
                        When this argument is None, it will search pop file at local root path")
    # parser.add_argument("--use_pop_file_iteration", type=int, default=1,
    #                     help="whether use iteration number of the pop file or not")
    parser.add_argument("--use_pop_file_iteration", type=int, default=0,
                        help="whether use iteration number of the pop file or not")

    parser.add_argument("--use_pop_init_npy", type=int, default=1,
                        help="1 means we skip constraint selection when iteration = 0 and directly use pop_init_npy_file,\n" + \
                             "0 means we use constraint selection normally")
    parser.add_argument("--pop_init_npy_file", type=str, default="pop_init.npy",
                        help="when enable use_pop_init_npy, we need to add pop_init npy file path")

    parser.add_argument("--max_iteration", type=int, default=5000,
                        help="max iteration")
    parser.add_argument("--save_pop_per_iteration", type=str, default=100,
                        help="every # of iteration, save pop array")

    args = parser.parse_args()
    if args.debug:
        log.setLevel(logging.DEBUG)
    else:
        log.setLevel(logging.INFO)

    if args.load_npy:
        C = np.load(args.container_npy_file).astype(np.float)
    else:
        C = np.array([args.container_length, args.container_width, args.container_height]).astype(np.float)



    LEN_IGNORE = args.len_ignore
    POP_SIZE = args.pop_size
    # on utlise un dna qui a un longeur 10 pour exprimer un x,
    # on effet, pour exprimer (x,y,z) , il faut un dna avec longeur 3*dna_size
    # le dernier dna qui a longeur `dna_size` est pour exprimer l'orientation
    DNA_SIZE = args.dna_size
    MAXITERATION = args.max_iteration

    csv_dict = read_colis_list_csv(args.items_csv_file)

    enterprise_item_dict, item_enterprise_dict = getItemEntrepriseDictAndEntrepriseItemDict(csv_dict)
    # TODO: need to mv this in argument list
    enterprise_order = ["rouge", "orange", "jaune"]

    ideal_dechargement_order = getIdealDechargementOrderFromEntrepriseOrder(enterprise_order, enterprise_item_dict)
    # print(ideal_dechargement_order)
    NB_COMPOSANT = len(csv_dict["Item_Geometry"])
    # initialize population array

    if args.continue_training:
        if args.pop_npy_file != None:
            pop = np.load(args.pop_npy_file)
            if re.match(r"pop_\d{5}.npy", args.pop_npy_file) != None:
                num_str = re.sub(r'.npy', "", args.pop_npy_file)
                num_str = re.sub(r'pop_', "", num_str)
                current_iteration = int(num_str)
            else:
                current_iteration = 0
        else:
            if args.pop_npy_dir == None:
                pop_npy_dir = "./"
            else:
                pop_npy_dir = args.pop_npy_dir
            pop, current_iteration = load_population(pop_npy_dir)

        if not args.use_pop_file_iteration:
            current_iteration = 0
    else:
        pop = initialize_population(200, NB_COMPOSANT, csv_dict, dna_size=DNA_SIZE)
        current_iteration = 0
        # np.save("pop_init5.npy", pop)
        # log.debug("Time for initializing population array: {} s".format(end-start))
    # start = time.time()
    X_all, P_all = translateDNA(pop, C[0], C[1], C[2], dna_size=DNA_SIZE)
    # end = time.time()
    # log.debug("Time for translating dna: {} s".format(end-start))

    # show all values in matrix
    np.set_printoptions(threshold=sys.maxsize)

    fig = plt.figure(figsize=(10, 10))
    # ax1 = fig.add_subplot(111, projection="3d")
    ax1 = fig.add_subplot(221, projection="3d")
    ax2 = fig.add_subplot(222)
    ax3 = fig.add_subplot(223)
    ax4 = fig.add_subplot(224)

    composant_color_list = []
    for i in range(NB_COMPOSANT):
        composant_color_list.append(random_mpl_color_rgba())
    _iter = current_iteration
    log.info("Begin from iteration = {}".format(_iter))
    contact_area_min_values = []
    vide_idx_values = []
    while(_iter < MAXITERATION):
        if _iter == 0 and args.use_pop_init_npy:
            pop = np.load(args.pop_init_npy_file)
        else:
            pop = select_by_constraints(pop, _iter, C, csv_dict, ax1, composant_color_list, dna_size=DNA_SIZE,
                                        min_pop_size=POP_SIZE+50)
        if _iter == 0:
            np.save("pop_init.npy", pop)

        X_all, P_all = translateDNA(pop, C[0], C[1], C[2], dna_size=DNA_SIZE)
        scores = compute_scores(X_all, P_all, C, csv_dict)
        pop, next_generation_ids = build_pareto_population(pop, scores, POP_SIZE)
        if _iter % args.save_pop_per_iteration == 0 and _iter !=0:
            np.save("pop_{:0>5d}.npy".format(_iter),pop)

        X_all = X_all[next_generation_ids]
        P_all = P_all[next_generation_ids]
        F_values, overloaded_values, X_all = computeFAndOverloadedValues(X_all, P_all, C, csv_dict)
        scores = scores[next_generation_ids]

        individus_at_1st_pareto_idx = identify_pareto(scores, np.arange(POP_SIZE))
        scores_at_1st_pareto = scores[individus_at_1st_pareto_idx]
        sorted_scores = scores_at_1st_pareto[np.argsort(scores_at_1st_pareto[:, 1])]
        solution_chosen = individus_at_1st_pareto_idx[np.argmin(scores_at_1st_pareto[:, 1], axis=0)]
        X = X_all[solution_chosen]
        P = P_all[solution_chosen]
        X = addGravity(X,P,csv_dict)
        score_chosen = scores[solution_chosen]
        F_value_chosen = F_values[solution_chosen]
        contact_area_min_values.append(score_chosen[0])
        vide_idx_values.append(score_chosen[1])

        text_str = "F_value:{:.4f};\nContact_area: {:.4f};  Vide ratio:{:.4f}".format(
            F_value_chosen, score_chosen[0], score_chosen[1])
        ax1.text2D(0.05, 0.95, text_str, transform=ax1.transAxes)

        ax2.set_xlabel("Iterations")
        ax2.set_ylabel("contact area values")
        ax2.plot(current_iteration + np.arange(len(contact_area_min_values)), contact_area_min_values, 'b-', linewidth=1)
        ax3.set_xlabel("Iterations")
        ax3.set_ylabel("vide ratio values")
        ax3.plot(current_iteration + np.arange(len(contact_area_min_values)), vide_idx_values, 'b-', linewidth=1)

        # plot
        ax4.set_xlabel("vide ratio")
        ax4.set_ylabel("contact area value")
        ax4.scatter(scores[:, 1], scores[:, 0], color=random_mpl_color_rgba())
        # ax4.scatter(scores[individus_at_1st_pareto_idx, 1], scores[individus_at_1st_pareto_idx, 0], marker="^", color=random_mpl_color_rgb())
        ax4.plot(sorted_scores[:,1], sorted_scores[:,0])
        plt.pause(0.1)


        fig.savefig("L-constraint-select-res.png")
        _iter = _iter + 1














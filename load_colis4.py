import os
import re
import numpy as np
import itertools
import math
import argparse
import pandas as pd
import utils.tools as tools
import matplotlib

matplotlib.use('TkAgg')
import matplotlib.pyplot as plt
import colis4 as colis


def load_population(rootdir="./", npy_file=None):
    """

    :param rootdir: parent folder of npy file
    :param file: npy file that we want load, if not set, it will load last pop_{05d}.npy file
    :return:
    """
    if rootdir == None:
        rootdir = "./"
    if npy_file != None:
        pop_file_path = os.path.join(rootdir, npy_file)
        pop = np.load(pop_file_path)
    else:
        list = os.listdir(rootdir)
        num_list = []
        npy_list = []
        for item in list:
            if re.match(r"pop_\d{5}.npy", item) != None:
                num_str = re.sub(r'.npy', "", item)
                num_str = re.sub(r'pop_', "", num_str)
                num_list.append(int(num_str))
                npy_list.append(item)

        if len(num_list) != 0:
            idx = np.argmax(np.array(num_list))
            pop_file = npy_list[idx]
            pop_file_path = os.path.join(rootdir, pop_file)
            pop = np.load(pop_file_path)
        else:
            raise Exception("No population numpy file in folder {}".format(os.path.abspath(rootdir)))

    return pop

def overlap2d(center_1, length_1, width_1, center_2, length_2, width_2):
    overlap_length = max(0, min(center_1[0] + length_1/2, center_2[0]+length_2/2) -
                        max(center_1[0] - length_1/2, center_2[0] - length_2/2))
    overlap_width = max(0, min(center_1[1] + width_1/2, center_2[1]+width_2/2) -
                        max(center_1[1] - width_1/2, center_2[1] - width_2/2))
    overlap_area = overlap_length * overlap_width
    ret = False
    if overlap_area != 0:
        ret = True
    return ret, overlap_area



if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description="Camion_optimisation"
    )
    parser.add_argument("--load_npy", type=int, default=1,
                        help="whether load npy file")
    parser.add_argument("--container_length", type=float, default=12.0,
                        help="container's length")
    parser.add_argument("--container_width", type=float, default=12.0,
                        help="container's width")
    parser.add_argument("--container_height", type=float, default=12.0,
                        help="container's height")
    parser.add_argument("--container_npy_file", type=str, default="C.npy",
                        help="npy file path of container")
    parser.add_argument("--items_npy_file", type=str, default="B_15_items.npy",
                        help="npy file path of items")
    parser.add_argument("--weights_npy_file", type=str, default="composants_weights.npy",
                        help="npy file path of composants' weights ")
    parser.add_argument("--load_bearing_capacity_npy_file", type=str, default="composants_load_bearing_capacity.npy",
                        help="npy file path of weights which composants can bear")
    parser.add_argument("--ideal_dechargement_order_csv", type=str, default="ideal_dechargement_order.csv",
                        help="csv file path of ideal dechargement order ")
    parser.add_argument("--len_ignore", type=float, default=0.0,
                        help="length that can be ignored when considering overlap area")
    parser.add_argument("--pop_size", type=int, default=200,
                        help="population size that we keep during optimization process")
    parser.add_argument("--dna_size", type=int, default=10,
                        help="DNA length")
    parser.add_argument("--continue_training", type=int, default=0,
                        help="0 means we randomly initialize pop array and begin, 1 means we use the latest pop array")
    parser.add_argument("--pop_npy_file", type=str, default=None,
                        help="When enable continue_training, we can chose pop npy file")
    parser.add_argument("--pop_npy_dir", type=str, default=None,
                        help="folder which we save pop npy files, \
                        When this argument is None, it will search pop file at local root path")
    parser.add_argument("--take_out_direction", type=str, default="-x",
                        help="direction how we take out items. Choices: +x, x, -x, +y, y, -y, -z")
    parser.add_argument("--save_fig_name", type=str, default="result.png",
                        help="figure file name")
    args = parser.parse_args()

    if args.load_npy:
        C = np.load(args.container_npy_file)
        B = np.load(args.items_npy_file)
        composants_weight = np.load(args.weights_npy_file)
        composants_load_bearing_capacity = np.load(args.load_bearing_capacity_npy_file)

    else:
        C = np.array([args.container_length, args.container_width, args.container_height]).astype(np.float)
        B = np.array([[2., 4., 4.],
                    [3., 5., 3.],
                    [5., 1., 4.],
                    [4., 2., 2.],
                    [1., 4., 3.],
                    [4., 4., 4.],
                    [3., 1., 5.],
                    [3., 1., 2.],
                    [1., 3., 2.],
                    [4., 2., 3.],
                    [3., 5., 1.],
                    [5., 4., 2.],
                    [5., 2., 3.],
                    [5., 4., 5.],
                    [5., 3., 2.]])
        composants_weight = np.array([8, 8, 4, 4, 3, 6, 4, 1, 2, 4, 6, 9, 6, 10, 10]).astype(float)
        composants_load_bearing_capacity = np.array([25, 15, 7.5, 1, 10,
                                                     3, 20, 11, 3, 20,
                                                     32, 38, 7, 8, 12]).astype(float)


    camion = colis.Camion.atOrigine(C[0], C[1], C[2]);




    entreprise_order, ideal_dechargement_order, ideal_entreprise_item_dict, ideal_item_entreprise_dict = tools.read_dechargement_order_csv(
                                                        args.ideal_dechargement_order_csv)
    tools.print_dechargement_order(entreprise_order, ideal_entreprise_item_dict)

    pop = load_population(rootdir=args.pop_npy_dir, npy_file=args.pop_npy_file)

    LEN_IGNORE = args.len_ignore
    ## creer la population initiale
    DNA_SIZE = args.dna_size
    DIRECTION = args.take_out_direction
    pop_size = pop.shape[0]
    nb_composant = pop.shape[1]

    permutation_list = list(itertools.permutations([0, 1, 2]))
    permutation_array = np.asarray(permutation_list, dtype=np.int)
    X_all, P_all = colis.translateDNA(pop, C[0], C[1], C[2], dna_size=DNA_SIZE)

    B_all = colis.P2B(P_all, B, permutation_array)
    scores = colis.compute_scores(X_all, B_all, C, len_ignore=LEN_IGNORE)
    F_values = colis.compute_F_values(X_all, B_all, C, len_ignore=LEN_IGNORE)
    dechargement_distances = colis.compute_dechargement_dist(X_all, B_all, entreprise_order, ideal_dechargement_order,
                                                       ideal_item_entreprise_dict,
                                                       direction=DIRECTION, len_ignore=LEN_IGNORE)



    individus_at_1st_pareto_idx = colis.identify_pareto(scores, np.arange(pop_size))
    scores_at_1st_pareto = scores[individus_at_1st_pareto_idx]
    sorted_scores = scores_at_1st_pareto[np.argsort(scores_at_1st_pareto[:, 1])]
    print(individus_at_1st_pareto_idx)
    if individus_at_1st_pareto_idx.shape[0] > 10:
        individus_at_1st_pareto_idx = colis.reduce_by_crowding(scores_at_1st_pareto, 5)
        scores_at_1st_pareto = scores[individus_at_1st_pareto_idx]

    print(individus_at_1st_pareto_idx)
    num_items = individus_at_1st_pareto_idx.shape[0]
    fig_row = int(math.sqrt(num_items + 1))
    fig_cols = int((num_items + 1) / fig_row) + 1

    fig = plt.figure(figsize=(10 / fig_row * fig_cols, 10))

    boite_color_list = []
    for i in range(nb_composant):
        boite_color_list.append(colis.random_mpl_color_rgba())
    for item_idx in range(num_items):
        ax_idx = fig.add_subplot(fig_row, fig_cols, item_idx + 1, projection="3d")
        # ax_idx.set_aspect(1)
        ax_idx.set_xlabel("X")
        ax_idx.set_ylabel("Y")
        ax_idx.set_zlabel("Z")
        ax_idx.set_xlim(-0, C[0])
        ax_idx.set_ylim(-0, C[1])
        ax_idx.set_zlim(-0, C[2])
        ax_idx.grid(False)
        ax_idx.add_collection3d(camion.getPoly3d(facecolors=(1, 1, 1, 0), edgecolors="black"))
        idx = individus_at_1st_pareto_idx[item_idx]
        X = X_all[idx]
        B_idx = B_all[idx]
        X = colis.addGravity(X, B_idx, len_ignore=LEN_IGNORE)
        scores_idx = scores[idx]
        F_value_idx = F_values[idx]
        raw_dechargement_order = colis.get_dechargement_order(X, B_idx, direction=DIRECTION, len_ignore=LEN_IGNORE)
        ideal_dechargement_order = colis.sort_dechargement_order(ideal_dechargement_order)
        actual_dechargement_order, actual_entreprise_item_dict, actual_item_entreprise_dict = \
            colis.split_dechargement_order(raw_dechargement_order, ideal_dechargement_order, entreprise_order)
        dechargement_dist = colis.get_dechargement_dist(actual_dechargement_order, actual_item_entreprise_dict,
                                                  actual_entreprise_item_dict,
                                                 ideal_dechargement_order, ideal_item_entreprise_dict,
                                                 entreprise_order)


        text_str = "F_value:{:.4f}\nContact area: {:.4f};  Vide ratio:{:.4f}".format(F_value_idx,
            scores_idx[0], scores_idx[1])
        text_str += "\nIdeal_dechargement order: \n{}\nActual_dechargement_order:\n{}\nDechargement distance: {}".format(
            ideal_dechargement_order, actual_dechargement_order, dechargement_dist)

        ax_idx.text2D(0.05, 0.95, text_str, transform=ax_idx.transAxes)
        boite_list = []
        for i in range(X.shape[0]):
            boite = colis.Boite((X[i, 0], X[i, 1], X[i, 2]), B_idx[i, 0], B_idx[i, 1], B_idx[i, 2])
            boite_list.append(boite)
            ax_idx.add_collection3d(boite.getPoly3d(facecolors=boite_color_list[i]))
            ax_idx.text(*X[i, :], str(i), 'x')

    ax_idx = fig.add_subplot(fig_row, fig_cols, fig_row * fig_cols)
    ax_idx.set_xlabel("vide ratio")
    ax_idx.set_ylabel("Contact area")
    ax_idx.scatter(scores[:, 1], scores[:, 0], color=colis.random_mpl_color_rgba())
    ax_idx.plot(sorted_scores[:, 1], sorted_scores[:, 0])
    plt.show()
    fig.savefig(args.save_fig_name)
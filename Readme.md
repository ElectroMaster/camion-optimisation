## Preparation
```buildoutcfg
pip install -r requirements.txt
```
## Run
And in this folder, open terminal and enter the command
```buildoutcfg
python colis4.py
```
To refer to the help documentation, you can enter the command
```
python colis4.py -h
```
## Visualization
After optimization, you will have several pop_{%5d}.npy file, you can enter
```
python load_colis4.py
```
to see the results.
To refer to the help documentation, you can enter the command
```
python load_colis4.py -h
```
![](./figs/result-order-dechargement.png)

current Result:
![](./documents/figs/L-constraint-select-res2.png)

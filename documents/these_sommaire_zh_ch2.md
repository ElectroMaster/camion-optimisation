# 第二章 针对pb de placement 的建模和优化算法
## 2.1 Intro
优化问题被分为三类
- 组合优化问题 pb d'optimisation combinatoire： 有有限解
- 连续优化问题 pb d'optimisation continue
- 前两种混合

Collette和Siarry的优化方法分类
![](./figs/classification_opt.png)
## 2.2 optimisation multi-objectif
![](./figs/opt_multi.png)
在此类多目标优化问题中，没有唯一的solution optimale，但是有一组solution optimales。解决此类问题的关键在于找出这一组solution optimales的集合。在这个集合里，没有任何一个solution可以说比另外一个更优。

multi-objectif 问题又被成为MO问题。解决MO问题的关键在于minimiser  simultanement un ensemble de p fonctions objectifs :

$$\min \limits_{\boldsymbol{x} \in E} f(\boldsymbol{x}) = \min \limits_{\boldsymbol{x} \in E} (f_1(\boldsymbol{x}), f_2(\boldsymbol{x}), ... , f_p(\boldsymbol{x}))$$

在MO问题中，我们使用principe de dominance 来比较两点。

**def1 (definition de la dominance)**

une solution $\boldsymbol{x} \in E$ domine $\boldsymbol{x'} \in E$ si elle verifie

$$\forall i \in \{1, ..., p\}, f_i(\boldsymbol{x}) \leq f_i(\boldsymbol{x'})$$
$$\exists i \in \{1, ..., p\}, f_i(\boldsymbol{x}) < f_i(\boldsymbol{x'})$$

On note cette relation de dominance $ \boldsymbol{x} \preceq \boldsymbol{x'} $

**def2 (definition de Pareto-optimale)**

Une solution $\boldsymbol{x}$ est dite efficace ou Pareto-optimale si elle n’est dominée par aucune autre solution appartenant à $E$. L’ensemble de ces solutions sont également appelées solutions non dominées.

大白话就是最优解定义。

**def3 (Front de Pareto)**

L’image des solutions efficaces forme dans l’espace des objectifs
un ensemble de points non dominés, communément appelé front de Pareto.

对于某些复杂问题，可以考虑realxer la contrainte de dominance

**def4 (definition de l'$\epsilon$-dominance)**

une solution $\boldsymbol{x} \in E$ $\epsilon$-domine $\boldsymbol{x'} \in E$ si elle verifie

$$\forall i \in \{1, ..., p\}, f_i(\boldsymbol{x}) \leq f_i(\boldsymbol{x'})$$
$$\exists i \in \{1, ..., p\}, f_i(\boldsymbol{x}) < f_i(\boldsymbol{x'})-\epsilon_{i}$$

On note cette relation de dominance $ \boldsymbol{x} \preceq \boldsymbol{x'} $



使用$\epsilon$-dominance可以增长points non domine（最优解）的个数。通常情况下
$$\epsilon_{i} = \epsilon(f_i^{max} - f_i^{min})$$

$f_i^{max}$, $f_i^{min}$通常曲子points ideal et Nadir.

**def5 (dominance stricte)**

Une solution $\boldsymbol{x} \in E$ domine strictement $\boldsymbol{x'} \in E$ si elle verifie: $f_i(\boldsymbol{x})<f_i(\boldsymbol{x'}), \forall i \in \{1,...,p\}$. On notte cette relation de dominance $\boldsymbol{x} \preceq \boldsymbol{x'}$

**Définition 6 (Définition du front de Pareto faible)**

 L’ensemble des solutions faiblement non
dominées d’un ensemble P est l’ensemble des solutions de cet ensemble P qui ne sont pas strictement dominées. Cet ensemble définit le front de Pareto faible.


**Définition 7 (Définition de la dominance pour un problème contraint)**

Une solution $\boldsymbol{x}$ contrainte-domine une solution $\boldsymbol{x'}$ , $\boldsymbol{x} \preceq_c \boldsymbol{x'}$, si l’une des trois conditions suivantes est vérifiée :

1. la solution $\boldsymbol{x}$ est réalisable alors que la solution $\boldsymbol{x}''$ ne l’est pas ;
2. les solutions $\boldsymbol{x}$ et $\boldsymbol{x'}$ sont toutes les deux irréalisables, mais la solution $\boldsymbol{x}$ présente un indice de violation de contraintes inférieur ;
3. les solutions $\boldsymbol{x}$ et $\boldsymbol{x}''$ sont toutes les deux réalisables, et la solution $\boldsymbol{x}$ domine la solution $\boldsymbol{x'}$ au
sens classique.

对于多目标问题，如果解集是不可实现的，那优化就是mono-objectif其目的是对尽量减少违反的约束。

用rang可以区分多目标个体。

###２.2.1 多目标问题解决方法　
解决方法大致分为两类

- non-pareto方法。non-pareto方法是将多目标问题转换成单目标优化。
- pareto方法，而pareto方法则不会改变问题原本的objectif。典型的pareto方法有algorithmes evolutionaire(进化算法)， algorithmes stochastiques（随机算法）等。



**non-pareto methodes**
最简单的就是权重方法，
$$\min \limits_{\boldsymbol{x} \in E} \sum_{i=1}^p {w_i}{f_i(\boldsymbol{x})}$$

大部分情况下，poid需要用户手动设置，但是也有方法可以决定poids 不同的$w_i$可以去掉pareto front中凸起的部分。

当然也有其他的一些non-pareto方法，略

## 2.3 Algorithmes evolutionaire
百度百科是这么解释遗传算法的，简单理解就是迭代算法，根据评价来决定是不是所需的结果。
![](./figs/algo_evolu_baidu.jpg)

进化算法可以分为以下几类
- les strategies d'evolution (ES) 进化策略，第一代进化算法
- les algotithmes genetiques (GA) 遗传算法，最为知名的算法
- l'evolution differentielle (DE) 差分进化。
- les algorithmes mémétiques (MA) 摸因算法，混合了搜索策略
- 其他


**优点**
1. leur mise en œuvre est généralement simple,
2. ils sont robustes. Ils ne sont pas aussi sensibles que les méthodes d’optimisation déterministes,
3. ils permettent d’intégrer différents types de variables lors de l’optimisation,
4. les calculs peuvent être facilement parallélisables, contrairement à la plupart des autres méthodes,
5. ils permettent de traiter les problèmes multi-objectifs.

### 2.3.1 遗传算法
包含两个阶段
- 探索阶段 phase exploratoire 或 寻找阶段 phase de recherche
- 强化阶段 phase d'intensification
#### 2.3.1.1 Principes des  algorithmes genetiques
算法步骤如图所示

![](./figs/algo_genetique.png)

mutation: 变异


该类算法包括
- algo genetiques mono-objectifs
- algo genetiques multi-objectifs
- algo genetiques generationnels

#### 2.3.1.2 多目标遗传算法
著名算法 略

##### 2.3.1.3 遗传算子
遗传算子是指一类用于一些个体上以产生新的个体的操作集合。非常重要。他们控制着个体的产生。

######  2.3.1.3.1 operateurs de selection et elitisme
选择和精英算子，该算子主要目标是选择具有高潜力，好品质的个体，同时消除其他个体以保持人口规模。

个体识别有选择算子完成，已经有较为成熟的选择方法，最为著名的就是比例选择方法，也成为轮盘选择(selection par roulette)

![](./figs/selection_roulette.png)

也存在一些其他的选择选择算子，如比赛选择(selection par tournoi)通过比较两种解决方案来选择更好的个体。

为了避免丢掉最好的解决方案，许多精英算则也被提出。 通过不丢失任何的最佳解决方案，精英算则显著提高了遗传算子的收敛性。

精英算子通常以以下两种方式被实现，在算法SPEA2中，有效解决方案被存储在一个population archive里。在算法NSGA-II中，通过显式的选择父种群和子种群的最有解决方案。

###### 2.3.1.3.2 operateurs de croisement et de mutation

交叉变异算子是用来从晒算出的方案里产生新的解决方案的。每一个操作都以一种特殊变量来定义（实数，二进制，排列等）
有多少种变量就有多少种算子，这些算子表现为用户定义的概率，许多困难都体现在这些参数的社会中。除了这些算子之外，用户还可以添加自己的算子，就像我们第四章所做的那样。

<p style="color:white;background-color:green;">关于这些算子请详见附录153页</p>

###### 2.3.1.3.3 operateurs de diversite
多样性算子，为了避免种群收敛至同一个单一的个体上，提出了一种多样性保持机制。该方法目的是提出一组均匀分布在pareto front上的点并计算点之间的距离。正常情况下，该机制通过点在目标空间的分布生成修改方案。

NSGA-II提出了distance phenotypique来试图在目标平面产生均匀化分布。SPEA2通过clusters来评价分布密度。

但是这些方法都没有考略变量空间的多样性（基因的多样性），然而这种多样性可能是设计者的重要决策因素。在某些情况下存在在目标空间非常接近，但是在变量空间相距甚远的情况，如下图所示，然而大多数进化算法侧重于目标空间的多样性而忽略了基因（变量空间）多样性。

![](./figs/diversite_op.png)

针对此类问题，某些人提出了一些方法。略。

### 2.3.2 使用的遗传算法
本节介绍后面使用的一些遗传算法
#### 2.3.2.1 NSGA-II
根据range de Goldberg， NSGA-II算法寻求在保持目标空间多样性（diversite dans l'espace des objectifs）的同时，快速获得问题有效解决方案。

精英机制（elitisme）允许该算法不丢失有效的解决方案。

selection par tournoi 用于选择将繁殖的个体，gestion des contraintes 建立在 criteres de contraintes dominance, 其中indice de violation de contraintes讲为满足的约束进行分组，使用selection par tournoi 可以清算产生可实现和不可实现方法。

#### 2.3.2.2 Omni-optimizer
omni-optimizer 被设计用于解决各种各样的优化问题，包括有一个或多个最优解的mono-objectifs, muti-objectifs优化问题。

omni-optimizer也包含了一些新的机制用于在某些情况下更好的收敛。


NSGA-II和omni-optimizer两者之间的不同如下。
- criteres de l'$\epsilon$-dominance 被用来对解的分类
- 选择算子仅用于参与比赛选择(dans le tournoi)的个体
- 用于变量空间和目标空间的距离评价标准被用来保持diversite denotypiaue et phenotypiques sur la surfaces de compromis.

### 2.3.3 讨论
遗传算法，是个强大nb的算法，当前趋势是将各种互有长处的算法进行结合。

## 2.4 评价surface de compromis的质量。
**surface de compromis** 是指多目标优化算法产生的一组近似frontiere de Paretto. 理想情况下，他们应该相互重合。

不同的优化算法会产生不同的优化方案集合，通过surface de compromis之间的比较可以评价优化算法的质量。许多评价基准(metriques)或措施被提出 。
每个metrique的目标都是提供一个surface de ccompromis可测量的数字特征，在这些特征里，与front de Pareto的越近，则越好。其中la diversite des solution 占据了重要的地位。

两种评价标准被使用，metrique unaire或absolues。metrique absolue在已知front de pareto时是可能的。 metriaue binaire 或 relative 允许比较两个surface de compromis.


这里给出的indicateurs是在目标空间上测量的。

假设一个多目标函数有 $p$ 个目标. 假设$Q$ surface de compromis的解集. 将 $|Q|$ 记为le cardinal de l’ensemble des points de la surface de compromis. 假设$| P^* |$  是位于或至少接近front de Pareto的解集,  所有和front de Pareto相关的元素都有上标 ∗ .

Par la suite, les indices $i$, $j$ et $k$ se référeront respectivement aux variables, objectifs et individus.
L’indice $l$ 被用于遍历位于 surface de compromis ou du front de Pareto的个体.
On appelle $\boldsymbol{x_k}$ le vecteur de décision à n-dimensions de l’individu $k$ et $\boldsymbol{f_k}$ le vecteur des fonctions
objectifs à $p$-dimensions de l’individu $k$.

$$ \boldsymbol{x_k} = (x_{k,1}, ... , x_{k,i}, ..., x_{k, n})$$
$$ \boldsymbol{f_k} = (f_{k,1}, ... , f_{k,i}, ..., f_{k, n})$$

### 2.4.1 Indicateurs unaires
用于分析sufrace de compromis 或者将之与front de pareto进行比较，当front de paretto已知时。

#### 2.4.1.1 计算 la surface de compromis et le front de Pareto距离

$$\mathcal{D} = \frac{1}{|Q|}\sum_{k=1}^{|Q|} d_k$$

avec
$$d_k = \min \limits_{l \in \{1,...,|P^* |\}} \sqrt{\sum_{j=1}^{p}(f_{k,j} - f_{l,j}^{*  })^2}$$

#### 2.4.1.2 esure de la représentation du front de Pareto
该metrique用于评价被surface de compromis覆盖掉的front de Pareto的部分，与前面想必只有微小不同。
$$\mathcal{R} = \frac{1}{|P^* |}\sum_{k=1}^{|P^* |} d_k$$

avec
$$d_k = \min \limits_{l \in \{1,...,|P^* |\}} \sqrt{\sum_{j=1}^{p}(f_{k,j} - f_{l,j}^{*  })^2}$$

#### 2.4.1.3 Metrique d'espacement $\mathcal{S}$

la métrique d’espacement $\mathcal{S}$ (Spacing) 用于分析组成surface de compromis点的均匀分布性。

$$\mathcal{S} = \sqrt{\frac{1}{|Q|} \sum_{k=1}^{|Q|}(d_k - \bar{d})^2} $$

avec
$$d_k = \min \limits_{l \in \{1,..., |Q|\}k} \sum_{j=1}^{p}|f_{l,j} - f_{k,j}|$$
et
$$\bar{d} = \sum_{k=1}^{|Q|} \frac {d_k}{|Q|}$$

#### 2.4.1.4 Mesure de diversite phenotypiques.

前面的metriaue表现了一种不便，她不能够评价接近front de Pareto平面的有点。于是乎就有了下面这种表示：

$$\Delta = \frac{\sum_{j=1}^{p} d_{j}^{e} + \sum_{k=1}^{|Q|}|d_k - \bar{d}|}{\sum_{j=1}^{p}d_{j}^{e} +  |Q|\bar{d}}$$

avec $d_j^e$ les distances entre les points extrémaux de la surface de compromis et ceux du front de Pareto. $d_k$ 和 $\bar{d}$的定义与espacement S中的定义相同。

#### 2.4.1.5 Mesure de la distance phénotypique maximale
cette mesure évalue la longueur de la diagonale de l’hyperboîte définie par les points extrêmes identifiés lors d’une simulation.
$$ D = \sqrt{\sum_{j=1}^{p}(\max \limits_{k \in \{1,..., |Q|\}} f_{k,j} - \min \limits_{k \in \{1,..., |Q|\}} f_{k,j})^2}$$

**normalisation**: Cet indicateur a par la suite été normalisé à l’aide des points extrêmes du front du Pareto, de manière à obtenir des valeurs comprises dans l’intervalle $[0; 1]$

$$ \bar{D} = \sqrt{\frac{1}{p}\sum_{j=1}^{p}\Biggl(\frac{\max \limits_{k \in \{1,..., |Q|\}} f_{k,j} - \min \limits_{k \in \{1,..., |Q|\}} f_{k,j}}{\max \limits_{k \in \{1,..., |Q|\}} f_{k,j}^{* } - \min \limits_{k \in \{1,..., |Q|\}} f_{k,j}^{* }}\Biggr)^2}$$


#### 2.4.1.5 Mesure de l'hypervolume
Pour chaque
solution $i \in Q$, un hypercube $v_i$ est construit à partir d’un point de référence $\boldsymbol{W} \in \mathbb{R}^p$ , la solution
$i$ se trouvant à l’extrémité de la diagonale de $v_i$ et partant de $\boldsymbol{W}$. L’hypervolume est ensuite calculé
comme le volume de l’union des différents hypercubes :
$$HV(Q) = volume\Bigl(\bigcup_{i=1}^{|Q|} v_i \Bigr)$$


如果我们知道了front de Pareto 或者是他的近似，那么这个metrique就可以nnormaliser，以便更为轻松的比较结果
$$HVR(Q) = \frac{HV(Q)}{HV(P*)}$$


### 2.4.2 Indicateurs binaires
Indicateurs binaires用于鳔胶两个surface de compromis之间。这些参数可以用来比较进化算法的性能。最常用的就是metrique $\mathcal{C}$

#### 2.4.2.1 Metrique $\mathcal{C}$
metrique $\mathcal{C}$ 用于比较A，B两个surfaces de compromis, Elle évalue la proportion de solutions de B qui sont dominées par les solutions de A.计算如下。

$$\mathcal{C}(A,B) = \frac{|\{b \in B | \exists a \in \mathcal{A}: a \preceq b\}|}{|B|}$$

### 2.4.3 讨论
这里的测量的参数都是在目标空间的，也有一些基于变量空间计算的，特别是关于diversite。尽管在文献中很少使用，但是关于diversite的测量可以知道是否使用用一组相似的解决方案获得了surface de compromis. 产期以来，多目标算法选择取决于能够提出surface de compromis 点是否能够沿着front de Pareto更好的分布的能力。

在许多问题中，最终解决方案取决于目标，但同时也取决于变量。la diversité génotypique比la diversité phénotypique更难计算。在许多情况下，优化问题使用不同类型的变量，因此没有单一的衡量标准。

##　2.5 结论
进化算法尤其是遗传算法在解决pb de placement上取得了巨大的成功。这些统计算法需要metrique以辅助分析和比较提出的方案所得到的结果。

下一章使用符合遗传算法来得到一组有小姐。，最终第四章提出的遗传算法来实行一组全局优化解

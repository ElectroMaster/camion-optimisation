## Rapport d'avancement
> Author: CAI
> Date: 17 mai, 2019

### 1. Introduction
On a choisit algorithme génétique pour résoudre le problème d'agencement, en premier étape, on seulement considère des colis de formes parallélépipède rectangles.
### 2. Algorithme génétiques
#### 2.1 Initialisation des populations  
On considère chaque résolution du problème est un individus. Donc chaque individus doit inclure les positions et les orientations des tous composants. Chaque individus est défini par des gènes. Spécifiquement, dans mon code, on a 10 composants et un contenant. La résolution des problèmes sont des positions et des orientations des 10 composants. Pour l'exprimer, on propose chaque individus a 10 gènes. Et chaque gène doit avoir les informations de position et d'orientation d'un composant. Donc **le premier étape** est de coder des composants dans gènes.
##### 2.1.1 Codage d'un composant
On a choisit le point de référence de ces composants comme le centre de ces boites pour positionner des composants. i.e. pour j-ième composant, on a son position $(x_j, y_j, z_j)$.  Commes les longeurs des côtés des composants sont fixes et on considères il existe seulement des rotations orthogonales, il y a 6 possibilités, on a choisit un nombre $n_{j,perm}, n_{j, perm} \in \{0,1,2,3,4,5\}$ pour exprimer l'orientation de composant $j$.
|$n_{j, perm}$|orientation|
|:---:|:---:|
|0|$(l, w, h)$|
|1|$(l, h, w)$|
|2|$(w, l, h)$|
|3|$(w, h, l)$|
|4|$(h, l, w)$|
|5|$(h, w, l)$|
Donc composant $j$ peut être définit par un tuple $(x_j, y_j, z_j, n_{j, perm})$. On propose ici un gène qui est constitué par 4 sous-gènes de longeur 10  pour exprimer ce tuple. On utilise code binaire pour coder.

![](./figs/gene_composant.png)

On peut maintenant exprimer individus $i$ par une matrice $X_i$, $X_i \in \{0, 1\}^{10 \times 40}$
$$
X_i =
\begin{bmatrix}
\begin{array}{ccccc|ccccc|ccccc|ccccc}
1 & 0 & \cdots & 0 & 1 & 0& 0 & \cdots & 0 & 1 & 1 & 0 & \cdots & 0 & 0 & 1 & 0 & \cdots & 0 & 1\\
\vdots & \vdots & \ddots & \vdots & \vdots & \vdots & \vdots & \ddots & \vdots & \vdots & \vdots & \vdots & \ddots & \vdots & \vdots & \vdots & \vdots & \ddots & \vdots & \vdots\\
1 & 0 & \cdots & 0 & 1 & 1 & 0 & \cdots & 0 & 1 & 1 & 0 & \cdots & 0 & 1 & 1 & 0 & \cdots & 0 & 1\\
\end{array}
\end{bmatrix}
$$

Dans notre code, on a 200 individus pour un population.


### 2.2 Sélection
D'après la principe d'un algorithme génétique, il faut supprimer des individus qui ne s'adapte pas des exigences. Donc il faut déterminer le degré d'adaptation. On utliser la fonction $F$ pour exprimer degré d'adaptation.

$$\begin{array}{lcl}
F(\boldsymbol{x}) & = & w_{pen} \sum_{i=1}^{m-1} \sum_{j=i+1}^{m} f_{ij}(\boldsymbol{x}) + w_{pro} \sum_{i=1}^{m} g_i(\boldsymbol{x})\\
& = & w_{pen} \sum_{i=1}^{m-1} \sum_{j=i+1}^{m} \Gamma_{ij}(\boldsymbol{x}) \Delta_{ij}({\boldsymbol{x}}) + w_{pro} \sum_{i=1}^{m} \bar{\Gamma}_{i}(\boldsymbol{x}) \bar{\Delta}_{i}(\boldsymbol{x})
\end{array}$$

Si tous les composants sont bien séparer et tous les composants sont dans la contenance, alor F serait 0. Donc on veut F le plus bas que possible. Donc on peut exprimer le degré d'adaptation d'un individus $i$ en utlisant la sélection  par tournoi:

$$\mathcal{D}_i = \frac{argmax(\{F_1, ..., F_{200}\}) - F_i}{\sum_{k=1}^{200}(argmax(\{F_1, ..., F_{200}\}) - F_k)}$$

Donc $F$ le plus petit, $\mathcal{D}$ le plus grand.

### 2.3 Critère d'arrête

Les critères d’arrêt de cette méthode itérative sont au nombre de trois : un nombre maximum d’itérations, une précision sur la norme du gradient ainsi qu’un seuil au-dessous duquel. Dans notre code on a seulement considérer la précision $F$ (lorsque $F < 1e^{-3}$)  et le nombre maximum d'itérations. Peut être on peut ajouter la partie gradient après.

### 2.4 Croisement
On suppose probabilité de croissement ici est 0.8. Lorsqu'il y des croissement entre individus i et individus j, alors l'élément dans gène d'individus $i$ a 50% de probabilité de remplacer par élément dans gène d'individus $j$.

### 2.5 Mutation
Chaque élément d'un gène d'individus $i$ a $1e^{-3}$ de probabilité de changer spontanément.

## 3 Analyse des résultats
D'après la figure ci-dessous, on peut voir cette méthode est efficace et bien convergé. Mais on trouve que les positionnements des composant n'est pas encore compacte maintenant. Parcequ'on n'a pas encore considérer des contraintes de compacte. On doit continuer à travailler sur cette partie-là.
![](../result.png)

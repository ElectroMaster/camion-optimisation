## Rapport d'avancement 4
> Author: CAI Pengfei
> Encadrant: Emilie Poirson
> Date: 27 mai, 2019

### 1. Introduction
On a choisit algorithme génétique pour résoudre le problème d'agencement. Cette fois, on a trois grand mis à jours: En premier, on essaie de faire mutation et croisement en gardant la population parent. En deuxième, on utilise NSGA-II algorithme pour sélectionner la population enfant. En troisième, on a ajouter `load.py` pour visualiser les résultats qu'on a obtenu.
### 2. Ce que l'on a fait
#### 2.1 Garder la population parent  
Auparavant, on n'a pas gardé la population parent. Maintenant, on utilise croisement pour augmenter la population, on mélange la population enfant obtenu par croisement avec la population parent.  Puis, chaque gène d'un individus peut fait un mutation. Cette fois on diminue le probabilité de mutation de  $3e^{-3}$ à $3e^{-4}$ En fin, on supprime les individus doublé.
![](./figs/colis2_flow.png)
#### 2.2 NSGA-II
##### 2.2.1 Front de Pareto
**def1 (definition de la dominance)**

une solution $\boldsymbol{x} \in E$ domine $\boldsymbol{x'} \in E$ si elle verifie
$$\begin{array}{rcl}
\forall i \in \{1, ..., p\}, f_i(\boldsymbol{x}) & \leq & f_i(\boldsymbol{x'}) \\
\exists i \in \{1, ..., p\}, f_i(\boldsymbol{x}) & < & f_i(\boldsymbol{x'})
\end{array}$$

On note cette relation de dominance $ \boldsymbol{x} \preceq \boldsymbol{x'} $

**de$F_2$ (definition de Pareto-optimale)**

Une solution $\boldsymbol{x}$ est dite efficace ou Pareto-optimale si elle n’est dominée par aucune autre solution appartenant à $E$. L’ensemble de ces solutions sont également appelées solutions non dominées.


**def3 (Front de Pareto)**

L’image des solutions efficaces forme dans l’espace des objectifs
un ensemble de points non dominés, communément appelé front de Pareto.



![](./figs/pareto_front.png)

##### 2.2.2 Crowding Selection
Lorsque qu'on utilise un front de Pareto, tous les points dans front de Pareto sont des solutions qui dominent tous les autre solutions. Mais si on n'a pas besoin tous les points, on peut utiliser distance phénotypique (anglais: *crowding distance*) pour choisir une part des points.
![](./figs/crowding_distance.png)

D'abord on calcule des distance phénotypique, puis on utilise la sélection par tournoi pour choisir les points. Deux points sont choisies au hasard et celle avec la plus grande distance phénotypique sera sélectionnée. Ce processus est répété jusqu'à ce que le nombre requis est atteint.
![](./figs/crowding_selection.png)


##### 2.2.3 Selection élististe

1. Identifier le premier front de Pareto ($F_1$)
2. Si nombre des points de $F_1$ est plus grand que le nombre maximale autorisé, on peut réduire la taille de $F_1$ en utilisant crowding sélection.
3. Si nombre des points de $F_1$ inférieur à la taille de la population requise, on supprime les points de $F_1$, puis on répéter le processus 1 pour obtenir $F_2$. Si la somme des  nomres des points de $F_1$ et $F_2$ est supérieur à nombre requise, on utilise crowding sélection pour réduire la taille, si non on revient à processus 1 ...

4. On répéter la sélection de Pareto jusqu'à ce que la taille de la population souhaité soit atteinte.


#### 2.3 Affichage des résultats
Lorsque le processus d'itération.
![](./figs/colis2-processus-debut.png)

![](./figs/colis2-processus-fin.png)

On stocke les matrices de la poulation, puis on utilise `load.py` pour visualiser les résultats.
![](./figs/colis2-resultat.png)

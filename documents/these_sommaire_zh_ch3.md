# Chaptitre 3 Optimisation multi-objectif d'agencement de locaux

## 3.1 Introduction

pb d'agencement de compartiments又被称为 pb d'agencement de locaux. 简称船的舱室布置。

已经开发了两种办法来处理不同尺寸的舱室布置问题。
- 离散的处理空间
  该方法将空间拆成基本单元，然后讲compartiment(舱室)放置在这些区域单元上，大部分建模方法都是用了这种方法。
- 连续的处理空间
  Lee et al提出了一种连续的空中表示，能够考虑问题的几何形状，尤其是非正交形状的船（navire）。在这个问题中，舱室的几何形状无关紧要，但是需要具有最小的地板面积。连续的空间表示很好的适用了这个问题。

## 3.2 objectifs du pb
目标是在船的甲板上分配所有的舱室，为了优化设计者定义的标准。一些简单的约束
- 所有的舱室必须位于船内。
- 舱室不得与走廊(couloir)或结构舱壁(cloisons de structure)重叠
- 分配每个舱室的空间必须大于每个舱室所需的面积。


  一种顺序逐个放置舱室的方法以确保满足这些约束。舱室的布局主要满足两个标准
  - 最小化les flux entre compartiments
  - 最大化满足设计者关于某些隔间之间接近的要求(affinités 亲和力 entre compartiments）。也被转换成最小化问题。

  <p style="color:white;background-color:green;">数学表示为</p>
$$f_1 = \sum_{i=1}^{m-1} \sum_{j=i+1}^{m} (f_{ij} \times d_{ij})$$

$$f_2 = \sum_{i=1}^{m-1} \sum_{j=i+1}^{m} (C_a - b_{ij} \times c_{ij})$$

avec
- $m$ : 舱室个数
- $f_{ij}$: flux entre materiel entre compartiments i et j. 值由设计师定义
- $d_{ij}$: 舱室i和j之间的距离
- $C_a$: 舱室之间最大毗邻距离， 常数，值为5
- $c_{ij}$舱室i和j之间毗邻距离系数，由设计师自行定义
- $b_{ij}$舱室i和j之间毗邻距离因子。该因子与距离$d_{ij}$相关。
- $$b_{ij} = 1- \frac{1}{6}[6\frac{d_{ij}}{d_{max}}]$$
avec $d_{max} = \max\{d_{ij}\}$,  $0 \leq b_{ij} \leq 1$


问题已知matrice de flux $\boldsymbol{F} $和matrice d'adjacence $\boldsymbol{C}$ 其元素分别为$f_{ij}$和$c_{ij}$。

- flux的值都被normaliser到0和1的范围内。值越接近1，相关舱室彼此则越接近。如果flux $f_{ij}$值为0，那么舱室i和j的位置则不会影响函数$f_1$；

- matric d'adjacence $\boldsymbol{C}$的值表示的是表达每对舱室之间接近的愿望（souhait）, $0 \leq c_{ij} \leq C_a$

- 矩阵$\boldsymbol{D}$，$\boldsymbol{B}$则取决于选择的配置的方案。每次选择一种新的配置方案，他们都要重新被计算。矩阵D再次集合了个舱室之间的距离。矩阵B包含了配置方案的facteurs d'adjacence。用于判断一对舱室接近与远离的程度，取值范围为0到1。为了避免不必要的计算，仅仅计算对于$d_{ij}$来说，$f_{ij}$和$c_{ij}$不为0的项。显然矩阵$\boldsymbol{D}$,$\boldsymbol{B}$和$\boldsymbol{C}$都是matrice symetrique.
- $f_{ij}$可以与$f_{ji}$不同。

<p style="color:white; background-color:pink">Lee et al.的方法</p>

他们的方法是mono-objectif的。问题被简化成最小化函数F。
$$F = w_1 \sum_{i=1}^{m-1} \sum_{j=i+1}^{m} (f_{ij} \times d_{ij}) + w_2\sum_{i=1}^{m-1} \sum_{j=i+1}^{m} (C_a - b_{ij} \times c_{ij})$$

**对于多目标问题，则没有这样的加权形式**

## 3.3 建模步骤
### 3.3.1 已知条件

第一个步骤是固定甲板的几何形状。图3.1和图3.2是简化后的甲板，coque:船壳， cloison：隔板，couloir：走廊。
![](./figs/navire.png)
图3.2右侧表示的可用面积随x轴变化的函数。

### 3.3.2 信息编码

编码可以以字符串的形式建立可能的解决方案。

与解决方案有关的信息在染色体（chromosome）上编码。

这些chromosome有两部分组成
- 第一部分给出了舱室在甲板上的引入顺序。
- 第二部分则给出了走廊的位置信息。

一些遗传操作将会在chromosome上进行操作。根据部分不同，操作也会不同。第一段有m个元素，对应于舱室的排列，第二段则包括每个舱室相关联的区域。第二部分包含了每个甲板走廊的水平和竖直位置，电梯和楼梯位置允许不同甲板之间的通信，他们沿着每个走廊均匀分布，表3.1给出了a的编码方案。

![](./figs/codage_navire.png)

### 3.3.3 codage和agencement的一致性
图3.4解释了chromosome的解码过程，第一步是定位不同甲板的couloir，随后需要计算每个子区域的面积累积函数。舱室随后就被确定。启发式放置讲compartiment一个接一个放置于不同的子区域中。这种舱室分布考虑了甲板的几何形状和障碍物。如果子区域的可用面积小鱼舱室面积，则将其放置于下一个。途中半连续虚线箭头表表示定位舱室的减半扫描方向。这种放置方法产生了一些空的空间，这将允许扩大一些舱室，最后可能无法放置所有的舱室，在这种情况下，解决方案是irealisable。将不能被放置的composant面积总和作为优化算法的penalite。
![](./figs/decodage_navire.png)

### 3.3.4 evaluation des individus
对于每种解决方案，启发式放置都是讲舱室放置于不同甲板上并且计算每个舱室的重心位置，这些位置被用来计算各舱室之间的距离，一旦这些距离倍计算出来，目标也就能被评估。

建模步骤如下：
1. chromosome编码
2. 分配各舱室的几何形状
3. 计算各舱室的重心
4. 计算各舱室之间的距离，
5. 计算函数（Évaluation des fonctions）f1和f2，

### 3.3.5 计算各舱室之间的距离。
有两种可能的方法用于评估舱室之间的距离，
<p style="color:white;background-color:green;">第一种方法是直接计算distance de Manhattan. </p>

为此只需要知道两个舱室的坐标就足够了。这种方法是近似的，但是可以快速的提供结果。
此时舱室$C_i$和$C_j$之间的距离为：
$$d_{ij} = |x_i-x_j| + |y_i-y_j| + |z_i - z_j|$$
其中$(x_i, y_i, z_i)$代表舱室$C_i$的坐标


<p style="color:white;background-color:green;">第二种方法是精确计算舱室之间的距离. </p>

该方法也是我们后来采用的，为此有必要计算每队舱室之间相互作用中最短路径长度。我们采用了Dij59算法，它是在两个舱室之间创建一组可嗯呢个的路径并找出最短的那条路径。

对于每种解决方案，我们列出船的走廊通道，通常来说每个舱室前面创建一个point de passage，在走廊的交叉点以及电梯前面也会创建这样的point de passage.这些点被称为noeuds.

然后这些连接定义了不同节点之间的最短路径，许多规则被用于创建这些连接。同一个couloir上的所有节点彼此连接从而这些节点可以访问走廊的和电梯，

不同甲板通过电梯和楼梯建立连接，也可以添加一些其他的规则，这些连接可以定义船内的所有路径。

这些结果以matrice triangulaire superieure M的形式表示。其中$M_{ij}$表示的是节点i和节点j之间的距离。最后Dij59算法通过该矩阵来寻找两个舱室之间的最短路径。

在多项式复杂度中，这个精确算法需要返回节点列表以表示连接两个舱室或分隔两个舱室。图3.5变现了这种距离计算的差异，与地种方法相比，这种该计算时间是第一种方法的3倍。

如图虚线是舱室3和舱室10曼哈顿距离，实线是实际两个舱室之间的距离。

![](./figs/distance_compartiment.png)

##　3.4 多目标优化

### 3.4.1 优化变量

couloir的位置被确定，这意味着每个子区域的面基函数都要计算一次，因此优化变量就仅仅是管理各个甲板上的个舱室引入顺序的排列。

排列的数量是有限的，但这个数字很大，是舱室数量的阶乘。面对如此大的数目，很明显这种方法不适合在有限的时间内找到正确的解。只有随机方法可以找到解决方案。

### 3.4.2 优化算法选择
我们选择多目标遗传算法NSGA-II算法来进行优化。算法的目的是最小化之前所说的函数$f_1$和$f_2$。在排列上使用遗传算子，选择的operateur de croisement是LHR03提出的。

该算子基于一下原则，最佳解决方案相比于其他解决方案必须传递更多的信息。再多目标情况下，我们使用rang des solution来决定排列中的多少信息传递给了enfants.其他的一些算子如OX或PMX也将被用到。

变异算子（operateur de mutation 将会以一定概率在排列中随机交换两个舱室）。
一些参数如表3.2所示，解决方案最多是$900 \times 128 = 115200$个，比原来的纯排列少多了。另外较高的prob de croisement使得新生代生成许多不同的解决方案。突变概率设为10%.
![](./figs/param_pont.png)


### 3.4.3 surface de compromis
我们提出比较遗传算法的四次执行情况，对于每次执行，随机数种子(le germe des series des nombres aleatoire)都是不同的。因此每次随机生成的初始种群与其他种群不同。同样的在不同遗传算子上的随机数也是如此。


略。。

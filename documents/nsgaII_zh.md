## NSGA-II  多目标优化算法

### 摘要
传统多目标算法存在以下问题：
- 拥有计算复杂度$O(MN^3)$, M为目标数量，N为种群大小
- 使用非精英机制
- 需要指定共享参数

NSGA-II算法
- 拥有计算复杂度$O(MN^2)$
- 提出了一个选择算子，创建了一个交配池用于combing parent and child 种群。并从中选取最好的N个解决方案。
- 通过对五个问题测试结果表明，与PAES和SPEA两个多目标算法相比，NSGA-II更好。


### 1. 引言
对于多目标优化问题，很难找到一个解同时优化所有的目标，因此找到一个位于pareto-optimal front附近的一组解具有更大的实际意义。

### 2. 精英多目标进化算法
Zitzler, Deb和Theile等人的研究表明精英机制(elitism)能够刚好的帮助多目标进化算法收敛。在具有精英机制的多目标进化算法中，Zitzler和Thiele等人的SPEA算法以及Knowles和Corne等人的PAES 以及Rudolph的elitist GA算法最为著名。
#### 2.1 SPEA
Zitzler和Thiele等人提出了一个精英多标准进化算法，他们提出在每一代都维持一个外部种群，用于储存目前从初始种群中发现的所有non-dominated solution. 外部种群参与遗传操作。 在每一代，一个复合了外部种群和当前种群的种群（combined population）首先被构建。

复合种群中的所有non-dominated solutions都基于他们所dominate的解决方案的数量被赋予了适应度(fitness)。
dominated solution相比于non-dominated solution 被赋予更差的适应度。

这种赋值方式确保搜索方式是朝向non-dominated solution的。 一个deterministic clustering 技术被用于确保non-dominated solutions的多样性。
#### 2.2 PAES
Knowles和Corne等人基于进化策略提出了一种简单的多目标进化算法PAES， 在PAES中只有一个父代（parent）和子代（child），附带和子代进行比较，如果子代dominate父代，那么子代则成为下一代的附带，然后迭代继续。
但如果父代dominate子代，那么则丢弃当前子代并试图生成新的子代。 但是如果父代和子代不互相dominate，那么将会通过比较他们迄今位置发现的最佳解决方案存档进行选择。 子代和存档进行比较，若子代dominate任何存档的成员，那么子代将会成为新的父代，并且存档中被dominated的方案将会被移除。 若不dominate存档中的任何成员，那么父代和子代将会检查和存档中的接近程度。如果子代位于存档成员中参数空间中最不拥挤的区域，则将其设为新一代父代，并将其添加至存档中。

#### 2.3 elitist multi-objective EA
Rudolph提出了一个简单的多目标计划算法，该方法基于父代个体和子代种群的比较。

子代种群的non-dominated solution 将会被用于和父代的non-dominated solution进行比较来形成一个overall non-dominated set of solutions.  这个set将会变成下一次迭代的父代种群。 如果该set的size小于期望的种群数量，那么其他的自带种群个体将会被包括。 但该方法没有考虑维持pareto最优解的多样性。


### 3 Elitist Non-dominated Sorting Genetic Algorithm (NSGA-II)
NSGA算法在1994年被提出，我们现在提出了NSGA-II算法，对其做出了改进
#### 3.1 一种快速non-dominated 排序方式
为了对有N个个体的种群根据non-dmoination等级进行排序，每种解决方案必须和种群中其他的解决方案进行比较来判断是否是否是dominated的。 对于每种方案需要$O(MN)$次比较。其中M代表目标个数。 所以对于所有的个体，总的计算复杂度是$O(MN^2)$。 在这个阶段所有位于第一pareto front的个体被找出，为了找到位于第二pareto front的个体需要暂时忽略第一pareto front上的solutions，然后重复先前的步骤。最差情况下，如果每个solution都分别属于单独的pareto front那么计算复杂度为$O(M N^3)$。 接下来我们提出的排序方式只需要$O(MN^2)$的复杂度。

该排序方式和先前的排序方式类似，但我们使用了一个更好的book-keeping策略。
![](./figs/fast-nondominated-sort.png)

如果想找到其他的最优解则忽视掉P'的成员然后重复上述操作。
但在我看来，这tmd就是最小值算法。。。

后续步骤请参见 jupyters/nsgaII，那个说的比较容易理解

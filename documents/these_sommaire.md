`problème de placement` = `Cutting & Packing problems (C&P)` = `Layout problems`

**buts**: maximiser le chargement.

- **composant**: plus souvent des objets physiaues.
    - attributs: les propriétés des éléments; constants ou variables.  normalement ne changent pas lors de la résolutions.
    - geometrie: longeur, modele CAO(en. computer aided design)
- **contenant**: dans lequel l'ensemble des composants doit etre positionne. **dimentsion** peut être varier ou fixes, si fixes: le buts est de maximiser le chargement, si varier le buts est de minimiser les dimensions du contenant.
si il y a plusieur composants il faut trouver le nombres minimum de contenant pouvant accueillir des composants;
    - attributs


- **inconnus du pb**: les varaible de positionnement.

- **contraintes**:
    - normalement appartenance au contenant.
- **objectifs**:
    - les fonctions mathématique explicite.

## definition
Étant donné un ensemble de composants ainsi qu’un ensemble de contenants, un problème de place-
ment consiste à trouver l’ensemble des variables de positionnement des composants afin de minimiser
un ensemble d’objectifs, tout en respectant des contraintes.

### techniques de placement
les méthodes de placement légal et relaxé.

### Représentations des variables
- permutations
- variables discètes
- variables rélles
- objectifs pour qualifier les solutions;
- Contraintes
    - contraintes non-chevauchement,
    - contraintes de position ou orientation
    - contraintes de proximité
    - contraintes de restriction de domaines
### Évaluation des contraintes de non-chevauchement et d’appartenance
pour un problème avec m composants, $ m^2  + m/2$ tests sont nécessaires.
les représentations sous formes voxels et d'octree  permettent de détecter facilement les collisions entre composants.

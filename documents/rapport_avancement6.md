## Rapport d'avancement 6
> Author: CAI Pengfei
> Encadrant: Emilie Poirson
> Date:18 Juin, 2019

### 1. Introduction
La semaine dernier, j'ai eassayé d'ajouté poids pour notre code.

### 2. Ce que l'on a fait
Comme on a ajouté la gravité pour les composants. Donc maintenant c'est possible de considérer les poids des composants. On propose que chaque composant a deux attributs: poid et poid support. Donc une contrainte maintenant est que le poid que chaque composant support doit inférieur au poid support duquel. On a deux étapes pour faire cette partie.  Premier étape est de tester si un placement vérifie bien cette contrainte ou pas. Duexième étape est  d'utiliser la sélection par tournoi pour mettre tous les résolution vérifier cette contrainte.

#### 2.1 Premier étape
Dans notre code on utilise la fonction `load_bearing_test` pour réaliser cette étape. Vous pouvez aussi consulter `<root_dir>/documents/jupyters/008_poids.ipynb` pour voir le resultat. L'idée est similaire au l'idéé de qu'on ajout la gravité.  On utilise aussi 'couche' pour résoudre ce problème. Une différence est que on séparer les composants dans différents couches par leurs surfaces supéfieur. Puis on peut déterminer l'ordre de placement. On créer une circulation pour faire ça, pour chaque itération on ajout un composant dans conteneur, puis on teste si il existe la superposition entre le surface supérieur d'un composant et les surfaces supérieurs des composants qu'on a mis au contenant.  Puis on peut obtenir deux listes `load_bearing list`, `load_bearing_area_list`.
Dans notre example on utilise 6 composants comme notre example, et leur poids sont `weights_list = [40.  20.  10.   5.   2.5  2.5]`

```
X = [[ 5.  0.  8.]
 [ 3.  7. 14.]
 [ 3.  5.  2.]
 [ 4.  7.  6.]
 [ 8.  8.  1.]
 [ 6.  7.  1.]]

 B = [[4 8 8]
 [4 8 4]
 [4 4 4]
 [2 4 4]
 [2 4 2]
 [2 4 2]]

 C = [10. 10. 10.]
 ```

<image src="./figs/6-item.png" style="display:block;width:400px;"></image>


On a `Placement order:  [4, 5, 2, 3, 0, 1]`, Cela sigifie que on met les composants d'ordre `[4, 5, 2, 3, 0, 1]`.  `load_bearing list:  [[2], [0], None, [2], None, None]` sigifie que composant 0 est support
é par composant 2; Composant 1 est supporté par composant 0; composant 2 est supporté par sol, etc.
`load_bearing_area_list:  [[2.0], [2.0], None, [4.0], None, None]`signifie que l'aire de surface de superposition entre les composants.

On crée `load_bearing_weights` intialement par 0 pour stocker les poids que les composants supporte. Puis on inverse `placemnt_ordre`, et par ordre de cette liste on calcule. Par exemple si composant $i$ est supporté par composant $j$ et $k$. on note le poid du composant $i$ est $W_i$, l'aire entre composant $i$ et $j$ est $S_{ij}$, l'aire entre composant $i$ et $k$ $S_{ik}$. Le poid que composant $i$ supporte est $W_{\bar{i}}$. Donc on peut calculer le poid que composant j supporte.

$$W_{\bar{j}} = \frac{W_{\bar{i}} + W_i}{S_{ij}+{S_{ik}}} \times S_{ij}$$

Donc on peut obtenir les poids supporte de chaque composant. `load_bearing_weights:  [20.  0. 65.  0.  0.  0.]`.

**Une autre example**
<image src="./figs/5-item.png" style="display:block;width:400px;"></image>

```
X = [[1. 2. 1.]
 [3. 2. 1.]
 [2. 2. 3.]
 [4. 2. 5.]
 [6. 2. 2.]]

B = [[2. 4. 2.]
 [2. 4. 2.]
 [4. 4. 2.]
 [8. 3. 2.]
 [4. 4. 4.]]

C = [10. 10. 10.]

weights_list = [40.  20.  10.   5.   2.5]
```

On a resultat:
```
Placement order:  [0, 1, 2, 4, 3]
load_bearing list:  [None, None, [0, 1], [2, 4], None]
load_bearing_area_list:  [None, None, [8.0, 8.0], [12.0, 12.0], None]
load_bearing_weights:  [7.5 7.5 5.  0.  5. ]
```

#### 2.2 Deuxième étape
On doit intégrer le contraite du poid au notre algorithme génétique maintenant. Avant cela on propose  `overloaded_value` pour faire cette étape.   On a `loadload_bearing_weights` pour noter les poids que les composant supporte, et on a aussi `load_bearing_capacity` pour noter les poids que les composants peut supporter. Il est en effet une distance euclidienne de norme 1 entre les deux mais un peut différent.

$$d = \sqrt{\sum_{i} \max(0, x_i-y_i)}$$

Où $x_i$ est valeur de `loadload_bearing_weights` et $y_i$ est valeur de `load_bearing_capacity`.

En considérant que une placement respecte au contrainte du poid plus facilement que respecte au contrainte d'appartenance. Donc on met cette contrainte au premier ordre. Puis presque tous les choses sont même pour le contrainte d'appartenance. On utlise aussi la sélection par tournoi, si il existe des individus ne satisefait au exigences on conserve les individus qui saitisefait au exigences et sélecte les individus qui ne saitsfiat au exigences par leur `overloaded_values` d'ordre croisement. Mias cette fois pour assurer tous les résolution vérifie bien notre contrainte du poid on utlise une sous-circulations pour faire la sélectioon jusqu'à tous les résolution respecte notre exigence.  

## 3 Analyse des résultats
On utilise 6 composants pour faire notre expérimentation. Leur longeur de côté est Matrice $B$.
```
B = [[2. 4. 2.]
 [2. 4. 2.]
 [4. 4. 2.]
 [8. 3. 2.]
 [4. 4. 4.]]
 ```
 Leur poids sont: `weights_list = [40.  20.  10.   5.   2.5]`
 Les poids qu'ils peut supporter sont: `load_bearing_capacity:  [1. 25.  9.  6.  3.  3.]`

 Le résultat idéal est un cube. Composant 0 ne supporte aucun autres composants. Composant 1 est au bas.

 Et voilà le résultat.

 **Processus d'optimisation**:

 <image src="./figs/chart-poids.png" style="display:block;width:500px;"></image>

 **Résultat après 700 itération**:

 <image src="./figs/resultat-poids.png" style="display:block;width:800px;"></image>


On peut voir on a obtenir une résultat très proche au résultat idéal. C'est rarement,  la plupart situation est que on peut seulement obtenir les résultat proche.

## 4 Ce que je vais faire
En analysant que les figures qu'on a obtenue, on trouve que un problème est que le petit composant ne peut pas descendre quand l'aire de superposition entre deux composant est très peu. Donc on peut éssayer ajouter la tolérence et voir si la performance augmente ou pas.

D'autre part on va considérer l'ordre de déchargement.

## Rapport d'avancement 10
> Author: CAI Pengfei
> Encadrant: Emilie Poirson
> Date:30 Juillet, 2019

### 1. Introduction

La semaine dernier, j'ai corrigé un bug, et j'ai ajouter un entrée dans interface pour aider l'utilisateur à déterminer s'il faut assurer laquelle surface située en-dessous. Et J'ai proposé une idée pour ajouter la gravité pour un placement.

### 2. Ce que l'on a fait 

#### 2.1 Bug corrigé

Au début une erreur se produit lorsque nous voulons changer la valeur du poid d'un composant. Après chercher, je trouve c'est un problème que j'ai oublié ajouter `str` pour transformer une valeur en chaîne de caractères.

#### 2.2 Ajout une entrée

Cette entrée est pour aider utilisateur à déterminer s'il faut assurer laquelle surface située en-dessous. Donc j'ai ajouter une méthode `drawSurfaceIndexOnAxes` pour classes `Boite`, `Lforme` et `Cylinder` pour afficher le numéro des surfaces d'un composant, puis l'utilisateur peut déterminer s'il faut ajouter cette contrainte. Par défaut ce valeur est `-1`, c'est-à-dire, on n'a pas ce contrainte.

<image src="./figs/bottom-surface.png" style="display:block;width:700px;"></image>

Pour un composant de L-forme, on a 8 surfaces (0, 1, 2, ..., 7)
<image src="./figs/lforme_indexes.png" style="display:block;width:300px;"></image>
Pour un composant de boite, on a 6 surfaces (0, 1, 2, ..., 5)
<image src="./figs/boite_indexes.png" style="display:block;width:300px;"></image>
Pour un composant de cylindre, on a 2 surfaces (0, 1)
<image src="./figs/cylinder_indexes.png" style="display:block;width:300px;"></image>
J'ai aussi ajouté la méthode `drawVerticesIndexOnAxes` pour afficher des numéros des sommets d'un composant.

#### 2.3 L'idée pour ajouter la gravité.
En effet, j'ai essayé plein d'idée, mais la plupart n'est pas adapté au notre algorithme. Par exemple No-fit-Polygone (NFP) est très compliqué pour un composant 3D et on doit changer toute l'algorithme. Autre idée est divisé L-forme en deux petites boites, mais c'est difficile pour assurer la position relative des deux boîtes inchangée. Après quelques éssaies, j'ai abondonné certaines idées. Finalement, je revient au idée initiales, c'est un peu similaire à idée qu'on utilise pour ajouté la gravité pour des composants des boites. On a deux étapes, le premier est d'obtenir l'ordre de placement non-traité. Le deuxième étapes est détecter la superposition des surfaces puis changer hauteurs de leurs centres.

Maintenant j'ai fini la premier étape. Auparavant, on a utilisé directement les hauteurs de leur centre pour obtenir l'ordre de placement non-traité. C'est-à-dire qu'on trie des composants par les hauteurs de leurs centres. Mais maintenant on a deux types de composants donc on ne peut pas directement utiliser les hauteurs. Mais le but est le même, on veut aussi trier les composants par un critère. Donc je utilise une façon pour trier les composants, le principe est un peu similaire à l'algorithme `quick sort` qu'on utilise souvent pour trier un liste:

Imaginer on a `nb_composant` composants.

```
# avant trie des composants
raw_placement_order = [0, 1, 2, ..., nb_composant-1]
pour i dans [0, 1, 2, ..., nb_composant-2]:
    pour idx dans [0, 1, 2, ..., nb_composant-i-2]:
        j = raw_placement_order[idx]
        jplus1 = raw_placement_order[idx+1]
        changer_flag = false
        # on veut comparer deux compoants, composant j  et composant jplus1
        si composantj est sur composantjplus1:
            changer_flag = true
        si changer_flag == true:
            # si composant j est sur composantjplus1, on va échanger la position de composantj  et
            # composantjplus1 pour assurer composantj est au-dessous de composantjplus1
            temp = raw_placement_order[idx+1]
            raw_placement_order[idx+1] = raw_placement_order[idx]
            raw_placement_order[idx] = temp

```

Donc le problème maintenant a devenu comment on peut juger que composantj est sur composantjplus1. On a certain cas:
**B-B**
Composant j est une boîte, composant j+1 est aussi une Boîte. Si la hauteur de la surface inférieur du composant j est plus grand que la hauteur de la surface inférieur du composant j+1, alor composant j est sur composant j+1

**B-L**
Composant j est une boîte, composant j+1 est un composant de L-forme.
sous-situations:
- le numéro de la surface inférieur du composant j+1 est 0 ou 4. Dans ce cas, si la hauteur de la surface inférieur du composant j est plus grand que la hauteur de la surface inférieur du composant j+1, alor composant j est sur composant j+1
- le numéro de la surface inférieur du composant j+1 est 2 ou 5. Dans ce cas, si la hauteur de la surface supérieur du composant j est plus grand que la hauteur de la surface supérieur du composant j+1, alor composant j est sur composant j+1
- le numéro de la surface inférieur du composant j+1 est 1 ou 3. Dans ce cas, composant j+1 peut considérer comme une boîte. si la hauteur de la surface inférieur du composant j est plus grand que la hauteur de la surface inférieur du composant j+1, alor composant j est sur composant j+1

**L-B**
Composant j est un composant de L-forme, composant j+1 est une boîte.
sous-situations:
- le numéro de la surface inférieur du composant j est 0 ou 4. Dans ce cas, si la hauteur de la surface inférieur du composant j est plus grand que la hauteur de la surface supérieur du composant j+1, alor composant j est sur composant j+1
- le numéro de la surface inférieur du composant j est 2 ou 5. Dans ce cas, si la hauteur de la surface supérieur du composant j est plus grand que la hauteur de la surface supérieur du composant j+1, alor composant j est sur composant j+1
- le numéro de la surface inférieur du composant j est 1 ou 3. Dans ce cas, composant j peut considérer comme une boîte. si la hauteur de la surface inférieur du composant j est plus grand que la hauteur de la surface inférieur du composant j+1, alor composant j est sur composant j+1

**L-L**
Composant j est un composant de L-forme, composant j+1 est un composant de L-forme.
- le numéro de la surface inférieur du composant j est 0 ou 4.
    - le numéro de la surface inférieur du composant j +1 est 0 ou 4.  Dans ce cas, si la hauteur de la surface inférieur du composant j est plus grand que la hauteur de la surface supérieur du composant j+1, alor composant j est sur composant j+1
    - le numéro de la surface inférieur du composant j+1 est 2 ou 5. Dans ce cas si la hauteur de la surface inférieur du composant j est plus grand que la hauteur de la surface supérieur du composant j+1, alors composant j est sur composant j+1
    - le numéro de la surface inférieur du composant j+1 est 1 ou 3. Dans ce cas, si la hauteur de la surface inférieur du composant j est plus grand que la hauteur de la surface inférieur du composant j+1, alors composant j est sur composant j+1

- le numéro de la surface inférieur du composant j est 2 ou 5.
    - le numéro de la surface inférieur du composant j +1 est 0 ou 4.  Dans ce cas, si la hauteur de la surface supérieur du composant j est plus grand que la hauteur de la surface inférieur du composant j+1, alor composant j est sur composant j+1
    - le numéro de la surface inférieur du composant j+1 est 2 ou 5. Dans ce cas si la hauteur de la surface supérieur du composant j est plus grand que la hauteur de la surface supérieur du composant j+1, alors composant j est sur composant j+1
    - le numéro de la surface inférieur du composant j+1 est 1 ou 3. Dans ce cas, si la hauteur de la surface supérieur du composant j est plus grand que la hauteur de la surface supérieur du composant j+1, alors comosant j est sur composant j+1

- le numéro de la surface inférieur du composant j est 1 ou 3.
    - le numéro de la surface inférieur du composant j +1 est 0 ou 4.  Dans ce cas, si la hauteur de la surface inférieur du composant j est plus grand que la hauteur de la surface inférieur du composant j+1, alor composant j est sur composant j+1
    - le numéro de la surface inférieur du composant j+1 est 2 ou 5. Dans ce cas si la hauteur de la surface supérieur du composant j est plus grand que la hauteur de la surface supérieur du composant j+1, alors composant j est sur composant j+1
    - le numéro de la surface inférieur du composant j+1 est 1 ou 3. Dans ce cas, si la hauteur de la surface inférieur du composant j est plus grand que la hauteur de la surface inférieur du composant j+1, alors comosant j est sur composant j+1

**Resultat**
Pour un placement ci-dessous, on peut obtenir l'ordre de placement: `[1,5,2,4,6,7,0,3,8,9]`.
<image src="./figs/placement_test1.png" style="display:inline;width:500px;"></image>
<image src="./figs/placement_test1_res.png" style="display:inline;width:300px;"></image>

Pour le deuxième étape, j'ai seulement commencé mais pas fini.

#### 2.4 Initialisation de la population
Comme on a ajouté une nouvelle contrainte, on ne peut pas initialiser une population complétement aléatoirement. Donc on choisit de générer chaque individus aléatoirement puis on va détecter s'il vérifie notre contrainte qui dit laquelle surface est le surface inférieur. Si cet individus vérifie, alors on le conserve comme un individus de la population, s'il ne vérifie pas, alors on regénérer un nouveau individus puis redétecter, etc. Jusqu'à qu'on obtenir une population de 200 individus. Pour atteindre ce but, j'ai ajouté la méthode `getBottomSurfaceIndex()` pour les classes `Boite`, `Cylinder` et `LForme`.

#### 2.5 Ajout de la minuterie
Pour estimer la performance de notre algorithme, j'ai ajouté un décorateur pour compter le temps d'un fonction. Auparavant, on imprimer les sorties dans terminals. Mais avec le processus d'optimisation, le text devient long, on ne peut pas voir les sorties au début. Donc on change le façon. Maintenant on utlise module `logging` pour imprimer les sorties dans un log ficher.

### 3. Ce que je vais faire
Je vais continuer la deuxième étape pour réaliser l'ajout de la gravité. Le principe idée est utliser la superposition des surfaces pour détecter l'adjacence des deux composants puis changer les hauteurs de leurs centres. C'est compliqué, la difficulté est principalement situé aux situations L-L et L-B.

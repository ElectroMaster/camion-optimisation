## Rapport d'avancement

### 1. Introduction

Le problème qu'on va résoudre est un problème d'agencement. Les éléments qu'on considère sont des composants (colis) et un contenant (camion). Ce problème est un problème multi-objectifs comme on veut trouver des solution efficace compacte, dimunuer le coût cheminement, etc. Plusieurs contraintes sont présentes ici, par exemples, non-chevauchement c'est-à-dire, il n'y a pas d'intersection des composants;appartenance, c'est-à-dire tous les composants doivent reseter intérieur d'un contenant. Ce problème est complexe parce que c'est un problèmes 3d, de plus des composants et contenants peut être sous formes quelconque complexes.

Donc pour progresser étape par étape, on peut d'abord considérer simplement des parallélépipède rectangles aligné sur le systèmes d'axes. Donc les variables d'optimisation sont directement des variables de positions des composants.

### 2. Définition des objets en 3D
Dans un premier temps, on considère un ensembles de $m$ parallélépipèdes rectangles (boîtes) libre aligné sur le systèmes d'axes.  Le point de référence de ces boîtres est choisi comme le centre de ces boites.

- Soit $\boldsymbol{x}$ la matrice de positions des centres des boîtes, $\boldsymbol{x} \in \mathbb{R}^{m \times 3}$. La ligne $i$ représente les coordonnées du centre de boîte $i$. La varaible $x_{ik}$ signifie i-ème boîte suivant la dimension $k$ et $k \in \{1, 2, 3\}$.


$$\boldsymbol{x} = \begin{bmatrix}
x_{11} & x_{12} & x_{13} \\
\vdots & \vdots & \vdots \\
x_{i1} & x_{i2} & x_{i3} \\
\vdots & \vdots & \vdots \\
x_{m1} & x_{m2} & x_{m3}
\end{bmatrix}$$

- Soit $\boldsymbol{b}$ la matrice de dimensions des boîtes, $\boldsymbol{b} \in \mathbb{R}^{m \times 3}$. La varaible $b_{ik}$ signifie le longeur d'un cote du boîte $i$.

$$\boldsymbol{b} = \begin{bmatrix}
b_{11} & b_{12} & b_{13} \\
\vdots & \vdots & \vdots \\
b_{i1} & b_{i2} & b_{i3} \\
\vdots & \vdots & \vdots \\
b_{m1} & b_{m2} & b_{m3}
\end{bmatrix}$$


![](./figs/composant.png)

- soit $\boldsymbol{c}$ le vecteur  de dimension du contenant, $\boldsymbol{c} \in \mathbb{R}^3$, on suppose tous les boîtes puissent être contenues dans le contenant, donc on a $0 < b_{ik}<c_{ik}, \forall i \in \{1,...,m\}, \forall k \in \{1,2,3\}$

### 3. Représentation des objets en 3D

On veut optimiser l'agencement d'un ensembles des boîtes,l'orientation de ces boîtes peut être une variable d’optimisation. Suivant, les rotations sont orthogonales, on peut utiliser des permutations pour les exprimer, il y a 6 possibilités.

![](./figs/cube_orientation.png)

Donc une idée est utiliser l'algorithme génétique.
Une autre possibilité est d'utiliser les angles d'Euler. Cette façon est utile pour des rotations continues.


### 4. Fonctions objectifs
Problem d'agencement est un problème multi-objectifs. Pour résoudre ce genre de problèmes, l'algorithmes évolutionnaires sont des bons choix. Parmi les algorithmes évolutionnaires, **l'algorithmes génétiques** est célèbre qui imite le processus naturel de l'évolution et permet de traiter des problèmes multi-objectifs.

![](./figs/algo_genetique.png)


Donc une idée est utiliser l'algorithmes génétiques:

![](./figs/algo_placement.png)

On peut utiliser des heuristiques de placement telles que **"Bottom-Left"** pour générer la population initiale.  Donc avant l'évaluation de chaque question les contraintes d'agencement sont vérifié. Une avantage de cette façon est que elle peut accélérer la convergence des fonctions objectifs et éviter obetenir des solutions irréalisable. Mais, il existe aussi des conditions que les contraintes ne sont pas êtres satisfaites au début, donc on peut utiliser algorithmes de séparation pour modifier les solutions à rendres les solutions admissibles. **La solution sera admissible dès lors que la valeur de la fonction renvoyée par l’algorithme de séparation est nulle**. Si l’un des critères d’arrêt est satisfait, l’algorithme s’arrête. Sinon, les opérations de sélection par tournoi, de croisement, mutation et échange sont exécutées en vue de produire une nouvelle génération. Le critère d’arrêt peut être un nombre maximum de générations, une stagnation de l’évolution de l’hypervolume, ou tout autre critère défini par l’utilisateur.

#### 4.1 Algorithme de séparation


D'abord on peut considérer deux contraintes, contraintes de non chevauchement et contraintes d'appartenance. D'après le contenue du cours optimisation, on sait que le but est de miniser d'un fonction qui signifie le non-respect des contraintes.

Cette fonction s'écrit de deux fonctions: la première traduit le non-respect des contraintes de non-chevauchement entre chaque paire de composants, la seconde exprime le non-respect des contraintes d’appartenance pour chaque composant.

$$F(\boldsymbol{v}) = w_{pen} \sum_{i=1}^{m-1} \sum_{j=i+1}^{m} f_{ij}(\boldsymbol{v}) + w_{pro} \sum_{i=1}^{m} g_i(\boldsymbol{v})$$


**La partie non-respect des contraintes de non-chevauchement**
- On utlise $\gamma_{ijk}(\boldsymbol{x})$ pour caractériser la largeur de l'intervalle d'intersection des segement suivant l'axe $X_k$
$$\gamma_{ijk}(\boldsymbol{x}) = \max \{0, \min\{x_{ik} + \frac{b_{ik}}{2}, x_{jk} + \frac{b_{jk}}{2}\} - \max \{x_{ik} - \frac{b_{ik}}{2}, x_{jk} - \frac{b_{jk}}{2}\}\}$$

- On utilise $\delta_{ijk}(\boldsymbol{x})$ pour exprimer la profondeur de pénétration entre deux composant i et j suivant l'axe $X_k$
$$\delta_{ijk}(\boldsymbol{x}) = \max \{0, \frac{b_{ik}}{2}+ \frac{b_{jk}}{2}-|x_{ik} - x_{jk}|\}$$.

**La partie non-respect des contraintes d'appartenance**
- On utilise $\bar{\gamma}_{ik}(\boldsymbol{x})$ pour exprimer la longeur de composant qui est à l'extérieur de contenant suivant l'axe $X_k$
$$\bar{\gamma}_{ik}(\boldsymbol{x}) = b_{ik} - \max\{0, \min\{c_k, x_i + \frac {b_{ik}}{2}\} - \max\{0, x_{ik} - \frac{b_{ik}}{2}\}\}$$

- On utilise $\bar{\delta}_{ik}(\boldsymbol{x})$ pour quantifier la translation nécessaire pour que le
segment i soit intégralement dans le contenant suivant l'axe $X_k$.

$$\bar{\delta}_{ik}(\boldsymbol{x}) = \max \{0, |x_{ik} - \frac{c_k}{2}| + \frac{b_{ik} - c_k}{2}\}$$

Donc on peut introduire des nouvelles Fonctions specifique dans dimension 3d

**La partie non-respect des contraintes de non-chevauchement 3d**
- $\Gamma_{ij}$ est le volume d‘intersection des boîte i et boîte j
- $\Delta_{ij}$ la distance minimale de translation des composant pour que ceux-ci ne se chevauchent plus

**La partie non-respect des contraintes d'appartenance 3d**
- $\bar{\Gamma}_i$ évalue le volume situé à l'extérieur du contenant.
- $\bar{\Delta}_i$ évalue la translation nécessaire pour que la boîte soit dans le contenant.

![](./figs/gradient_multi.png)

**Donc** on peut exprimer la fonction-objectif mathématiquement:

$$\begin{array}{lcl}
F(\boldsymbol{x}) & = & w_{pen} \sum_{i=1}^{m-1} \sum_{j=i+1}^{m} f_{ij}(\boldsymbol{x}) + w_{pro} \sum_{i=1}^{m} g_i(\boldsymbol{x})\\
& = & w_{pen} \sum_{i=1}^{m-1} \sum_{j=i+1}^{m} \Gamma_{ij}(\boldsymbol{x}) \Delta_{ij}({\boldsymbol{x}}) + w_{pro} \sum_{i=1}^{m} \bar{\Gamma}_{i}(\boldsymbol{x}) \bar{\Delta}_{i}(\boldsymbol{x})
\end{array}$$

Cette fonction est utiliser pour minimiser la somme des produits volumes d'intersection et assurer l'appartenance des composants en même temps. En autre phrase elle a pour mission de rendre admissible autant que possible les solutions proposées par l’optimiseur global. Les coefficients $w_{pen}$ et $w_{pro}$ sont des paramètres positifs permettant de régler de l’influence des deux fonctions.

### 4.2 Sélection
L’objectif premier de l’opérateur de sélection
est de sélectionner les individus à fort potentiel / de bonnes qualités et d’éliminer les autres tout en conservant la taille de la population. La méthode connu est l'opérateur de sélection par roulette.

<img src="./figs/tournoi.png" style="width:300px;display:block;">

### 4.3 Croisement et mutation

Les deux opérateurs sont utilisés pour générer de nouvelles solutions à partir des solutions sélectionnées. les opérateurs connus de croisement est la méthode SBX. Pour la mutation  la méthode mutation polynomiale est un bon choix.

## 5 référence
Guillaume Jacquenot. Méthode générique pour l’optimisation d’agencement géométrique et fonction-
nel. Mécanique [physics.med-ph]. Ecole Centrale de Nantes (ECN), 2010. Français. tel-00468463

## Rapport d'avancement 5
> Author: CAI Pengfei
> Encadrant: Emilie Poirson
> Date: 07 Juin, 2019

### 1. Introduction
Pour ajouter le contrainte de poid, on d'abord ajoute la gravité pour faciliter les étapes suivants. Donc cette semaine, on a ajouter la gravité pour éviter le chevauchement entre les composants. D'autre part, on a considéré  maximiser les surfaces dont l'un composant touche l'autre comme un objectif. On a essayé aussi deux méthodes, une méthode est de mettre fonction $F$ qui exprime l'appartenance d'un contenant comme une contrainte, une autre méthode est de mettre l'appartenance comme un objectif.

### 2. Ce que l'on a fait
#### 2.1 Gravité
D'abord on trie les centre des gravité des composants du plus bas au plus haut. Chaque hauteur dedans signifie un couche. Pour les composants dont leurs centres de gravité qui sont de même hauteurs, on les considère ils sont dans même couche. Puis on met les composant couche par couche. On a stocké tous les composants qu'on a mis dans un liste `placed_list`. Pour chaque composant $j$, on teste si il existe un supperposition entre la surface inférieur du composant $j$ et la surface supérieur du composant qui est dans `placed_list`. Donc si il existe la supperposition certains composants, on choisit  le composant $i$ qui est le plus haut entre les quels. On change la coordonné du composant $j$ de $(x_j, y_j, z_j)$ à $(x_j, y_j, z_j + h_j/2 + x_i+ h_i/2)$. Si il n'existe pas composant $i$ qui a la superposition avec le composant $j$. Alors on change la coordonné du composant $j$ de $(x_j, y_j, z_j)$ à $(x_j, y_j, z_j + h_j/2)$. C'est-à-dire on le met au sol.

<image src="./figs/15-item.png" style="width:400px;">

<image src="./figs/15-item-gravity.png" style="width:400px;">

#### 2.2 Nouvelle fonction objective
Cette fois, on ajout les surfaces de superpositions entre les composants comme une nouvelle fonction objectve. Pour considerer les surfaces superposition entre les composant et le sol, on considère le sol comme un composant qui a le même longeur et largeur en comparant avec le contenant, mais il a un hauteur 0.

On le note $F_{CA}$ , CA signifie *contact area*.
$$F_{CA} = - contactArea$$

#### 2.3 Changement pour la fonction $F$
Pour la fonction $F$, au début, elle comprenne deux partie, une partie pour exprimer le chevauchement, une autre partie pour exprimer l'appartenance. Comme on a ajouté la gravité, donc il n'exite pas le chevauchement entre deux composant. Donc on supprime la partie qui exprime le chevauchement.

#### 2.4 Comparaison entre deux méthodes
##### 2.4.1 Deux méthodes
On a deux méthodes pour faire notre algorithme maintenant. Une méthode est de considérer la fonction $F$, la fonction $F_{CA}$ et la fonction $r$ (vide ratio) comme les fonctions objectives, donc c'est un problème multi-objectif non contrainte. On a seulement besoin de chercher les solutions Pareto optimale.



Une autre méthode est d'utiliser considère la fonction $F$ comme une contrainte. Donc c'est un problème multi-objectifs mono-contrainte. Pour le faire, il faut considérer la fonction $F$ séparament. Pour une itération, après créer une nouvelle génération il faut d'abord choisir les individus d'après les valeurs de $F$ des composants, s'il vérifie notre exigence $F<1e^-3$, on le conserve; s'il ne vérifie pas notre exigence. on choisit les individus de valeurs $F$ du plus petit au plus grand en utilisant la sélection par tournoi, jusqu'à un nombre (par exemple 250). Puis on utilise Pareto selection pour chercher les solutions optimales (200 individus) pour deux fonctions objectives $F_{CA}$ et $r$.

##### 2.4.2 Analyse des résultats
Pour la première méthode,
![](./figs/chart-F-obj.png)

![](./figs/resultat-F-obj.png)

Pour la deuxième méthode,
![](./figs/chart-mono-contrainte.png)

![](./figs/resultat-mono-contrainte.png)
##### 2.4.3 Conclusion
Donc on peut voir très clairement que la première méthode n'est pas très bonne, les composants sont plustôt en brochette, elle ne peut assurer que les solutions vérifie le contrainte.  Mais la deuxième vérifie bien  les contraintes.

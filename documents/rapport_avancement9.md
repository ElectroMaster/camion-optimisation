## Rapport d'avancement 9
> Author: CAI Pengfei
> Encadrant: Emilie Poirson
> Date:16 Juillet, 2019

### 1. Introduction 
La semaine dernier, j'ai éssaiyé d'implémenter un IHM, Comme le temps est très court et j'ai aussi quelques questions, donc je n'ai pas encore fini. Mais la plupart fonctionnement est déjà ajouté.

### 2. Ce que l'on a fait
J'ai presque tout mis en œuvre: créer un nouveau fichier csv, puis ajouter ou modifier des composants de trois formes, notamment Box, Cylindre et L-Form. Vous pouvez également lire des fichiers csv existants et ajouter, modifier ou supprimer des composants.
#### 2.1 UX Design
Je commence par la conception d'interface. Donc je utilise MOCKINGBOT pour faire la conception. Les figures ci-dessous sont des pages que j'ai désigné.
- D'abord on doit avoir un page d'acceuil

<image src="./figs/ux-design1.png" style="display:block;width:700px;"></image>

- Puis quand utilisateur crée un nouveau csv ou lire un csv fichier existant, utilisateur doit avoir une vue globale, c'est-à-dire utilisateur doit savoir combien composants q'on a déjà et les paramètres des composants. Donc je choisi d'afficher par un tableau.  utilisateur peut modifier le composant en cliquant sur l'icône en forme de crayon ou le supprimer en cliquant sur l'icône en forme de poubelle.  utilisateur peut ajouter des composants en cliquant le boutton "add new item". utilisateur peut aussi stocker ce tableau en cliquant "save now".

<image src="./figs/ux-design2.png" style="display:block;width:700px;"></image>

- Quand utilisateur modifie ou cree un nouveau composant, utilisateur doit avoir un example pour dire le sens de chaque paramètre. Il doit aussi savoir le forme qu'il a crée. Donc on a deux figures, un est example, l'autre est le composant que utilisateur a créé. Utilisateur peut aussi abondonner ce composant en cliquant "back", si il veut savoir le forme origine du composant, il peut cliquer "cancel" pour tout réinitialiser. Il peut cliquer "Add" pour ajouter ce composant dans le tableau.

<image src="./figs/ux-design3.png" style="display:block;width:700px;"></image>

- Ici c'est le fenêtre pour afficher les informations de créateurs.
<image src="./figs/ux-design4.png" style="display:block;width:700px;"></image>

#### 2.2 Implémentations
- Page d'aceuil, on peut tourner logo par souris.

<image src="./figs/interface1.png" style="display:block;width:700px;"></image>

- On peut également le voir et ne pas le modifie. si on clique le boutton de forme oeuil.

<image src="./figs/interface2.png" style="display:block;width:700px;"></image>


<image src="./figs/interface3.png" style="display:block;width:700px;"></image>

<image src="./figs/interface4.png" style="display:block;width:700px;"></image>

- Le resultat
<image src="./figs/interface-result.png" style="display:block;width:700px;"></image>
### 3. Ce que j'ai pas fait
J'ai pas encore ajouter l'entree d'utilisateur qui determine lequel surface ne peut pas toucher des autres composants.

#### 4.3.1.2 cas multidimensionnel 多维情况

d = 2 时，composant为边与坐标轴平行的长方形，当d=3时composant为长方体，同样其边与坐标轴平行。不管怎样我们将其称为boite。boite i由两个向量定义:
- vecteur de dimension
- vecteur de position

假设$\boldsymbol{x}$表示这些boites的重心的位置（matrice de position），那么这个矩阵就有m行d列。$\boldsymbol{x} \in \mathbb{R}^{m \times d}$。 第i行表示的第i个boite的坐标。k代表当前维。那么变量$x_{ik}$代表了第i个boite的k维坐标。

假设$\boldsymbol{b}$表示的是boite的各边长(dimension des boites),$\boldsymbol{b} \in \mathbb{R}^{m \times d}$. 用$\boldsymbol{c}$表示contenant的尺寸向量（vecteur de dimension）,$\boldsymbol{c} \in \mathbb{R}^{ d}$。

**假设所有的boites能够装入容器**:
$$0<b_{ik}<c_k, \forall i \in\{1,...,m\}, \forall k \in \{1,..., d\}$$

<p style="color:white;background-color:red;">下面介绍结果：</p>

假设boite i 和 boite j维度为d，各边长与坐标轴平行。他们的位置向量分别为$\boldsymbol{x_i}$和$\boldsymbol{x_j}$，他们的尺寸向量分别为$\boldsymbol{b_i}$和$\boldsymbol{b_j}$。我们现在就可以计算两个boite的segment d'intersection 以及 如profondeur和penetration de penetration。当这两者为零时，意味着两个boite完全不重合，当他们不为0时则重合。

然后我们就有公式4.10
![](./figs/eq_multi_dimension.png)
梯度与之前4.7和4.9公式类似只不过多了k角标。
![](./figs/gradient_eq_1d.png)

![](./figs/gradient_eq_2.png)


我们引入新函数$\Gamma_{ij}$,$\Delta_{ij}$, $\bar{\Gamma_i}$, $\bar{\Delta_i}$来对原来的$\gamma_{ij}$,$\delta_{ij}$, $\bar{\gamma_i}$, $\bar{\delta_i}$进行转换
- $\Gamma_{ij}$将线段变成面积/体积。
- $\Delta_{ij}$则是是两个boite分离的最小移动距离，（取各维度的$\delta$最小值，只要有一个维度不重合，那么就ok了）
- 注意这里的公式不是简单的相乘，是所有的boites的面积/体积-在外面的部分
- 这里则取了最大值，因为要使得所有的部分均包含在contenant里面。

**note**： gamma表示重合部分(intervalle d'intersection)，delta表示最小移动分离部分(profondeur de penetration)

![](./figs/gradient_multi.png)

**梯度计算**:$\nabla_p$用于表示对composant p的梯度向量$\nabla_p = (\frac{\partial}{\partial x_{p1}},..., \frac{\partial}{\partial x_{pd}})$
首先我们列出了一些梯度为0的项
![](./figs/gradient_zero.png)

梯度函数$\Gamma_{ij}$和$\Delta_{ij}$对composant i 和composant j求导，可以写成


![](./figs/gradient_composant.png)

梯度函数$\bar{\Gamma_i}$和$\bar{\Delta_i}$对composant i 求导：
![](./figs/gradient_composant2.png)

#### 4.3.1.3 最小化函数构造
这是我们之前写的目标函数
$$F(\boldsymbol{v}) = w_{pen} \sum_{i=1}^{m-1} \sum_{j=i+1}^{m} f_{ij}(\boldsymbol{v}) + w_{pro} \sum_{i=1}^{m} g_i(\boldsymbol{v})$$
作者测试了5种目标函数，分别为
![](./figs/fonction_test.png)

函数F1可以很好的评估重叠约束，同时也是连续可微分函数。
F2 仅使用穿透深度来避免组件完全位于其他组件内的过早收敛情况。
F3 利用F1和F2的有点，F4和F5则使用了不同权重的穿透深度。
梯度函数则可以写成

![](./figs/gradient_fonction_obj.png)

算法详见论文

结果证明F3最好

6中可能朝向
![](./figs/cube_orientation.png)

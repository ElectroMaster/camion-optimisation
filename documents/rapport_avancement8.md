## Rapport d'avancement 8
> Author: CAI Pengfei
> Encadrant: Emilie Poirson
> Date:8 Juillet, 2019
### 1. Introduction
La semaine dernier, j'ai réfléchie beaucoup et je trouve que la forme non-parallélépipédique est plus intéressant. Donc je commence par l'ajout des formes: cylindre et canapé.  J'ai aussi désigné un nouveau csv fichier que nous le rentre dans notre code.

### 2. Ce que l'on a fait
#### 2.1 Ajout de la forme  non-parallélépipédique
J'ai éssaie d'ajouter des nouvelles classes `Lforme` et `Cylindre` dans `forme.py`. De plus, j'ai réussit d'ajouter des fonctionnement qui permet tourner des objets.
<image src="./figs/Lforme_show.png" style="display:block;width:400px;"></image>
#### 2.2 Conception du CSV fichier "colis_list.csv"
```
Item_index, Entreprise, Item_forme, param1, param2, param3, param4, param5, param6, weight, load_bearing_capacity
0,B,boite,2,4,4,,,,8,25
1,D,boite,3,5,3,,,,8,15
2,C,boite,5,1,4,,,,4,7.5
...
11,A,Lform,5,4,2,4,1,,9,38
...
15,E,cylindre,2,2,4,,,,9,20
...
```
On a plusieur choix pour les géométries des composants: `boite`, `Lforme` et `cylindre`. Si ce composant est de forme `boite`, alors on a besoin seulement 3 paramètres, `param1` signifie `length`, `param2` signifie `width`, `param3` signifie `height`. Si ce composant est de forme `Lforme`, on a besoin 5 paramètres. `param1` signifie `L_shorter_outer_len`, `param2` signifie `L_width`, `param3` signifie `L_longer_outer_len`, `param4` signifie `L_shorter_inner_len`, `param5` signifie `L_longer_inner_len`. Si ce composant est de forme `cylindre`, alors on a besoin seulement 3 paramètres, `param1` signifie `diameter_x`, `param2` signifie `diameter_y`, `param3` signifie `length`.
<image src="./figs/formes.png" style="display:block;width:700px;"></image>

## Rapport d'avancement 12
> Author: CAI Pengfei
> Encadrant: Emilie Poirson
> Date:12 Août, 2019

### 1. Introduction

La semaine dernier, j'ai réussi à résoudre la problème d'appartenance et le problème du poids support pour des composants de L-forme ou boîtes.

### 2. Ce que l'on a fait
#### 2.1 Idéé
Pour résoudre ce problème, je trouve que pour re-utiliser des algorithme avant, la plus meilleur méthode est de diviser un composant de L-forme en deux boîtes. Donc ça revient à la situation qu'il n'y a que des boîtes.  Mais attention, il faut diviser un composant de L-forme verticalement et dynamiquement. Il est très important pour résoudre  le problème du poids support.

<div align="center">
<image src="./figs/L-forme-division.png" style="display:block;width:300px;"></image>
<b>Figure 2.1 Division verticalement</b>
</div>

#### 2.2 Processus
Après qu'on a cette idée, on n'a pas besoin de re-réaliser le processus d'optimisation, le problème est transformé en comment diviser des composants de L-forme. Comme auparavant, on a utilisé $X$ et $B$,  $X$ est une matrice concerne aux centres des boîtes, $B$ est une matrice qui stocke des longeur, largeur et hauteur des boîtes. Maintenant on doit re-obtenir ces deux matrices. Pour ne pas confondre, on utilise $X'$et $B'$ pour exprimer des $X$ et $B$ avant, on utlise $X$ pour exprimer des centres des composants, y compris des composants de L-forme et des boîtes.

<div align="center">

<image src="./figs/lforme_indexes.png" style="display:block;width:300px;"></image>
<b>Figure 2.1 Composant de L-forme</b>

<image src="./figs/lforme_pseudo_indexes.png" style="display:block;width:300px;"></image>
<b>Figure 2.2 Composant de L-forme avec pseudo sommets</b>
</div>


En effet, pour diviser un composant de L-forme, le premier étape est qu'on détermine des sommets des deux sous-boîtes, Puis on peut obtenir des centres et longeur, largeur et hauteur actuel d'une boîtes.  On divise aussi les poids et les piods support d'un composant de L-forme d'après les volumes des deux sous-boîtes.

#### 2.3 Résultat
Le processus d'optimisation est vraiment lentement, au début ça prend 60 min pour obtenir le résultat. Donc j'optimise mon algorithme, maintenant ça prend 40 min pour obtenir le résultat.
<div align="center">

<image src="./figs/L-constraint-select-res.png" style="display:block;width:300px;"></image>
<b>Figure 2.3 Sélection par des contraintes
(appartenance et poids support)</b>

<image src="./figs/L-constraint-select-log.png" style="display:block;width:300px;"></image>
<b>Figure 2.4 log</b>
</div>

### 3 Ce que je vais faire
Je vais éssayer résoudre le problème de compacité, dont minimiser le vide-ratio et maximiser les surfaces contactes.

## Rapport d'avancement 11
> Author: CAI Pengfei
> Encadrant: Emilie Poirson
> Date:5 Août, 2019

### 1. Introduction

La semaine dernier, j'ai réussi à ajouter la gravité pour un placement.

### 2. Ce que l'on a fait

#### 2.1 La gravité
J'ai déjà expliqué l'idée pour ajouter la gravité dans le rapport avant. Il est divisé en deux étapes. Premièrement, on doit déterminer l'ordre de placement non-traité. Puis on doit détecter la superposition des surfaces puis changer hauteurs de leurs centres. J'ai fini la première étape le 30 Juillet, donc la semaine dernier, j'ai principalement réalisé la deuxième étape.

#### 2.2 L'idée principale

Comme on a l'ordre de placement, on doit placer des composants. Imaginez qu'on a déjà placé $j$ composants. Et maintenant on doit placé $j$+1-ième composant. On propose ce $j$+1-ième composant est composant $r$. On doit détecter la superposition entre la surface inférieure de  $j$+1-ième composant et les surfaces supérieur des composants qu'on a déjà placé. Par exemple, si on a détecté que il y a une superposition entre la surface inférieure de composant $r$ et la surface supérieure de composant $k$, $w$, $h$. Lorsque cette condition, on doit utiliser la hauteur de la surface supérieure la plus haute. Puis on va changer la hauteur de centre de composant $r$ d'après cette hauteur.
#### 2.3 Cas différents
L'idée est simple, mais les sous-conditions sont compliquées.  Pour détecter la superposition, on a certains cas.  
<div align="center">
<image src="./figs/boite_indexes.png" style="display:block;width:300px;"></image>
<b>Figure 2.1 Boîte</b>
<image src="./figs/lforme_indexes.png" style="display:block;width:300px;"></image>
<b>Figure 2.2 Composant de L-forme</b>
</div>



##### 2.3.1 B-B
Composant $r$ est une boite, composant $k$ est une boite. Composant $k$ est au-dessous du composant $r$.
C'est le cas le plus simple. Si il existe la superposition on va stocker la hauteur de la surface supérieure de composant k! dans `z_record_list`.

##### 2.3.2 B-L
Composant $r$ est une boite, composant $k$ est un composant de L-forme. Composant $k$ est au-dessous du composant $r$. C'est un peu compliqué.
- Si l'indice de la surface inférieure de composant $k$ est 0 ou 4.
    - Premièrement, on doit détecter si'il existe la superposition entre la surface inférieure de composant $r$ et la surface supérieure plus hautede composant $k$. Si il existe la superposition on va stocker la hauteur de cette surface supérieure plus haute de composant $k$ dans `z_record_list`.
    - Deuxièmement, on doit détecter s'il existe la superposition entre la surface inférieure de composant $r$ et la surface supérieure plus base de composant $k$. Si il existe la superposition on va stocker la hauteur de cette surface supérieure plus base de composant $k$ dans `z_record_list`.

- Si l'indice de la surface inférieure de composant $k$ est 2 ou 5. Dans ce cas, composant $k$ a seulement une surface supérieur. On doit détecter s'il existe la superposition entre la surface inférieure de composant $r$ et la surface supérieure de composant $k$. Si il existe la superposition on va stocker la hauteur de cette surface supérieure de composant $k$ dans `z_record_list`.

- Si l'indice de la surface inférieure de composant $k$ est 1 ou 3. Dans ce cas, composant $k$ a seulement une surface supérieur. Cette surface inférieure est de forme L. Pour faciliter l'implémentation des codes, on divise cette surface en deux rectangle. On doit détecter s'il existe la superposition entre la surface inférieure de composant $r$ et la surface supérieure de composant $k$. Si il existe la superposition on va stocker la hauteur de cette surface supérieure de composant $k$ dans `z_record_list`.
##### 2.3.3 L-B
Composant $r$ est est un composant de L-forme composant $k$ est une boîte. Composant $k$ est au-dessous du composant $r$. c'est le cas un peu compliqué.
- si l'indice de la surface inférieure de composant $r$ est 0 ou 4. Dans ce cas composant $r$ a seulement une surface inférieur. On doit détecter s'il existe la superposition entre la surface inférieure de composant $r$ et la surface supérieure de composant $k$. Si il existe la superposition on va stocker la hauteur de cette surface supérieure de composant $k$ dans `z_record_list`.
- si l'indice de la surface inférieure de composant $r$ est 1 ou 3. Dans ce cas composant $r$ a seulement une surface inférieur. Cette surface est de forme L, Pour faciliter l'implémentation, on divise cette surface en deux rectangles. On doit détecter s'il existe la superposition entre la surface inférieure de composant $r$ et la surface supérieure de composant $k$. Si il existe la superposition on va stocker la hauteur de cette surface supérieure de composant $k$ dans `z_record_list`.
- si l'indice de la surface inférieure de composant $r$ est 2 ou 5. Dans ce cas composant $r$ a deux surfaces inférieur, surface inférieure plus haute et surface inférieure plus base.
    - On doit détecter s'il existe la superposition entre la surface inférieure plud base de composant $r$ et la surface supérieure de composant $k$. Si il existe la superposition on va stocker la hauteur de cette surface supérieure de composant $k$ dans `z_record_list`.
    - On doit détecter s'il existe la superposition entre plus haute la surface inférieure de composant $r$ et la surface supérieure de composant $k$. On propose la hauteur de la surface plus haute est h. La distance entre la surface plus haute et la surface plus base est $\Delta h$
    Si il existe la superposition on va stocker $h-\Delta h$ dans `z_record_list`.


##### 2.3.4 L-L
Composant $r$ est est un composant de L-forme composant $k$ est un composant de L-forme. Composant $k$ est au-dessous du composant $r$. c'est le cas le plus compliqué.
- si l'indice de la surface inférieure de composant $r$ est 0 ou 4. Dans ce cas composant $r$ a seulement une surface inférieur.
    - si l'indice de la surface inférieure de composant $k$ est 0 ou 4, dans ce cas composant $k$ a deux surfaces supérieurs.
        - Premièrement, on doit détecter si'il existe la superposition entre la surface inférieure de composant $r$ et la surface supérieure plus hautede composant $k$. Si il existe la superposition on va stocker la hauteur de cette surface supérieure plus haute de composant $k$ dans `z_record_list`.
        - Deuxièmement, on doit détecter s'il existe la superposition entre la surface inférieure de composant $r$ et la surface supérieure plus base de composant $k$. Si il existe la superposition on va stocker la hauteur de cette surface supérieure plus base de composant $k$ dans `z_record_list`.
    - si l'indice de la surface inférieure de composant $k$ est 1 ou 3, dans ce cas composant $k$ a seulement une surface supérieurs. On doit détecter si'il existe la superposition entre la surface inférieure de composant $r$ et la surface supérieure de composant $k$. Si il existe la superposition on va stocker la hauteur de cette surface supérieure de composant $k$ dans `z_record_list`.
    - si l'indice de la surface inférieure de composant $k$ est 2 ou 5, dans ce cas composant $k$ a seulement une surface supérieurs. On doit détecter si'il existe la superposition entre la surface inférieure de composant $r$ et la surface supérieure de composant $k$. Si il existe la superposition on va stocker la hauteur de cette surface supérieure de composant $k$ dans `z_record_list`.



- si l'indice de la surface inférieure de composant $r$ est 1 ou 3. Dans ce cas composant $r$ a seulement une surface inférieur. Cette surface est de forme L, Pour faciliter l'implémentation, on divise cette surface en deux rectangles.
    - si l'indice de la surface inférieure de composant $k$ est 0 ou 4, dans ce cas composant $k$ a deux surfaces supérieurs.
        - Premièrement, on doit détecter si'il existe la superposition entre la surface inférieure de composant $r$ et la surface supérieure plus hautede composant $k$. Si il existe la superposition on va stocker la hauteur de cette surface supérieure plus haute de composant $k$ dans `z_record_list`.
        - Deuxièmement, on doit détecter s'il existe la superposition entre la surface inférieure de composant $r$ et la surface supérieure plus base de composant $k$. Si il existe la superposition on va stocker la hauteur de cette surface supérieure plus base de composant $k$ dans `z_record_list`.
    - si l'indice de la surface inférieure de composant $k$ est 1 ou 3, dans ce cas composant $k$ a seulement une surface supérieurs. On doit détecter si'il existe la superposition entre la surface inférieure de composant $r$ et la surface supérieure de composant $k$. Si il existe la superposition on va stocker la hauteur de cette surface supérieure de composant $k$ dans `z_record_list`.
    - si l'indice de la surface inférieure de composant $k$ est 2 ou 5, dans ce cas composant $k$ a seulement une surface supérieurs. On doit détecter si'il existe la superposition entre la surface inférieure de composant $r$ et la surface supérieure de composant $k$. Si il existe la superposition on va stocker la hauteur de cette surface supérieure de composant $k$ dans `z_record_list`.
- si l'indice de la surface inférieure de composant $r$ est 2 ou 5. Dans ce cas composant $r$ a deux surfaces inférieur, surface inférieure plus haute et surface inférieure plus base.

    - si l'indice de la surface inférieure de composant $k$ est 0 ou 4, dans ce cas composant $k$ a deux surfaces supérieurs.
        - Premièrement, on doit détecter si'il existe la superposition entre la surface inférieure plus base de composant $r$ et la surface supérieure plus hautede composant $k$. Si il existe la superposition on va stocker la hauteur de cette surface supérieure plus haute de composant $k$ dans `z_record_list`.
        - Deuxièmement, on doit détecter s'il existe la superposition entre la surface inférieure plus base de composant $r$ et la surface supérieure plus base de composant $k$. Si il existe la superposition on va stocker la hauteur de cette surface supérieure plus base de composant $k$ dans `z_record_list`.
        - Troisièmement, on doit détecter s'il existe la superposition entre la surface inférieure plus haute de composant $r$ et et la surface supérieure plus haute de composant $k$. On propose la hauteur de la surface plus haute de composant $k$ est $h_k$. La distance entre la surface plus haute et la surface plus base de composant $r$ est $\Delta h_r$
        Si il existe la superposition on va stocker $h_k-\Delta h_r$  dans `z_record_list`.

        - Quatrièmement, on doit détecter s'il existe la superposition entre la surface inférieure plus haute de composant $r$ et la surface supérieure plus base de composant $k$. On propose la hauteur de la surface plus base de composant $k$ est $h_k$. La distance entre la surface plus haute et la surface plus base de composant $r$ est $\Delta h_r$
        Si il existe la superposition on va stocker $h_k-\Delta h_r$  dans `z_record_list`.
    - si l'indice de la surface inférieure de composant $k$ est 1 ou 3, dans ce cas composant $k$ a seulement une surface supérieurs.
        - Premièrement, on doit détecter si'il existe la superposition entre la surface inférieure plus base de composant $r$ et la surface supérieure de composant $k$. Si il existe la superposition on va stocker la hauteur de cette surface supérieure plus haute de composant $k$ dans `z_record_list`.
        - Deuxièmement, on doit détecter s'il existe la superposition entre la surface inférieure plus haute de composant $r$ et la surface supérieurde composant $k$. On propose la hauteur de la surface supérieure  de composant $k$ est $h_k$. La distance entre la surface plus haute et la surface plus base de composant $r$ est $\Delta h_r$
        Si il existe la superposition on va stocker $h_k-\Delta h_r$  dans `z_record_list`.
    - si l'indice de la surface inférieure de composant $k$ est 2 ou 5, dans ce cas composant $k$ a seulement une surface supérieurs.
        - Premièrement, on doit détecter si'il existe la superposition entre la surface inférieure plus base de composant $r$ et la surface supérieure de composant $k$. Si il existe la superposition on va stocker la hauteur de cette surface supérieure plus haute de composant $k$ dans `z_record_list`.
        - Deuxièmement, on doit détecter s'il existe la superposition entre la surface inférieure plus haute de composant $r$ et la surface supérieurde composant $k$. On propose la hauteur de la surface supérieure  de composant $k$ est $h_k$. La distance entre la surface plus haute et la surface plus base de composant $r$ est $\Delta h_r$
        Si il existe la superposition on va stocker $h_k-\Delta h_r$  dans `z_record_list`.

#### 2.4 Etapes suivants
D'après les cas qu'on a introduits, on peut obtenir une liste `z_record_list`, puis on va utiliser la valeur maximale de cette liste pour changer la hauteur de composant . On propose le hauteur de centre de composant $r$ est $H_r$, la valeur maximale de `z_record_list` est $H_m$, la hauteur actuel de composant $r$ est $\Delta H$, elle égale à la distance entre la surface supérieure  plus haute et la surface inférieure  de composant .  Si z_record_list est vide, alors $H_m = 0$

$$ H_r \rightarrow H_m + \frac{\Delta H}{2}$$

#### 2.5 Resultat
Pour un placement ci-dessous,
**1-er exemple**
<div align="center">
<image src="./figs/BL-gravity-before.png" style="display:block;width:500px;"></image>

<b>Figure 2.3 1-er placement, avant l'ajout de la gravité</b>

<image src="./figs/BL-gravity-after.png" style="display:block;width:500px;"></image>
<b>Figure 2.4 1-er placement, après l'ajout de la gravité</b>
</div>

**2-ème exemple**
<div align="center">
<image src="./figs/BL-gravity-before2.png" style="display:block;width:500px;"></image>

<b>Figure 2.5 2-ième placement, avant l'ajout de la gravité</b>

<image src="./figs/BL-gravity-after2.png" style="display:block;width:500px;"></image>
<b>Figure 2.6 2-ième placement, après l'ajout de la gravité</b>
</div>



### 3. Ce que je vais faire
Je vais commencer à considérer les poids des composants et les poids qu'ils peuvent supporter. Mon idée est de diviser le composant de L-forme en deux boîtes, une avantage est qu'on peut utiliser l'algorithme qu'on a proposé. Mais il est encore difficile à réaliser.

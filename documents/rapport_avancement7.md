## Rapport d'avancement 7
> Author: CAI Pengfei
> Encadrant: Emilie Poirson
> Date:1 Juillet, 2019

### 1. Introduction
La semaine dernier, j'ai eassayé d'ajouté l'ordre de déchargement pour notre code. J'ai aussi mis certaines améliorations.

### 2. Ce que l'on a fait
Pour optimiser l'ordre de déchargement. Premièrement, on doit obtenir l'ordre de déchargement lorsqu'un placement. Puis on doit estimer l'ordre de déchargement qu'on a obtenue. En fin, il faut optimiser cette ordre.

#### 2.1 Expression d'ordre de déchargement idéal
Dans la vraie vie, on sait que on doit livrer des colis aux certaines entreprises. Donc les colis devait être divisés par les entreprises. En effet, l'ordre de déchargement est l'ordre d'entreprises. L'ordre des colis pour une même entreprise n'a pas de sens. Pour exprimer plus clairement, on a un example.

<image src="./figs/ideal_dechargement_order.png" style="display:block;width:100px;"></image>

Pour 8 composants(0,1,2...7), on doit les livrer aux 4 entreprises "A,B,C". Pour cette example, on doit livrer composants `[3, 4, 5]` à l'entreprise `A`. Puis `[1, 2]` à l'entreprise `B`, etc. Mais pour une même entreprise, l'ordre n'a pas de sens comme on doit livrer tous les composants de cette entreprise. C'est-à-dire, `[3, 4, 5]` est même que `[3, 5, 4]`.  Mais on ne peut pas changer l'ordre des entreprises. L'ordre des entreprises est important. Pour notre example on choisit l'ordre des entreprises comme `[A, B, C]`

Donc on peut exprimer notre l'ordre de déchargement ideal comme:
`[[3, 4, 5], [1, 2], [0, 6, 7]]`
Remarquez qu'on a trié des sous listes du petit au grand, mais c'est équivalent au ordre
`[[4, 5, 3], [2, 1], [0, 7, 6]]`

 l'optimisation.
#### 2.1 Obtenir l'ordre de déchargement actuel d'un placement
Le principe pour obtenir l'ordre de déchargement actuel est simple. D'abord on doit choisir une direction pour déterminer dans quel sens qu'on peut décharger des colis.  On a plusieurs choix:
- `-x` signifie que on doit d'abord décharger des colis au côté droit.
- `+x` et `x` signifie que on doit décharger des colis au côté gauche.
- `+y` et `y` signifie que on doit d'abord décharger des colis au côté avant.
- `-y` signifie que on doit d'abord décharger des colis au côté arrière
- `-z` signifie que on doit d'abord décharger des colis le plus haut

Pour notre exemple, on choisit la direction `-x`. Puis on mis des colis en ordre d'après les coordonnés de leur côté droit pour obtenir un ordre de déchargement non traité.

<image src="./figs/8-item.png" style="display:block;width:400px;"></image>

Pour un placement ci-dessus, on peut obtenir un ordre de déchargement non traité: `[3, 4, 5, 1, 2, 6, 7, 0]`. Puis on va tester itérativement: pour un colis, si il existe une autre colis sur lequel, on ne peut pas le décharger, puis on va tester le colis suivant. Si il n'existe aucun colis sur lequel, on peut le décharger. On supprime ce colis de cet ordre de déchargement non traité et on recommence cette teste itérative.

Pour le placement ci-dessus, pour le colis 3, on peut voir il y a colis 5 sur lequel, donc on va tester colis 4. Colis 5 est aussi sur colis 4. on va tester le colis suivant: colis 5. Aucun colis sur colis 5, donc on peut le décharger. Puis on supprime ce colis de l'ordre de déchargement non traité. Et on recommence ce processus. Finalement, on peut obtenir un ordre de déchargement actuel:
`[5, 7, 6, 3, 4, 1, 2, 0]`



#### 2.3 Calculer la distance entre ordre de déchargement idéal et ordre actuel.
D'abord on doit diviser l'ordre de déchargement actuel pour obtenir une forme similaire à l'ordre de déchargement idéal. Donc on a l'ordre de déchargement actuel`[[5, 7, 6], [3, 4], [1, 2, 0]]`. L'ordre de déchargement idéal est `[[3, 4, 5], [1, 2], [0, 6, 7]]`. Pour chaque colis de l'ordre de  déchargement idéal, on va chercher son position  de l'ordre de déchargement actuel.


En effet, on calcule la distance pour un colis de deux partie, une partie est distance des entreprise. Une autre partie est calculer d'après la position de ce colis dans son sous-liste.
$$
dist =
\begin{cases}
dist_{entreprise} + \frac {idx}{len}, & \text{if colis est livré à l'entreprise plus arrière} \\
dist_{entreprise} + \frac {len -1 - idx}{len}, & \text{if colis est livré à l'entreprise plus avant}
\end{cases}
$$

- $dist_{entreprise}$ signifie la distance des entreprise. Par exemple si on doit livrer ce colis à l'entreprise A mais on le livre à l'entreprise C, la distance des entreprise est donc 2. si on doit livrer ce colis à l'entreprise B mais on le livre à l'entreprise A, la distance des entreprise est donc 1.

- $idx$ : son indice de son sous liste. par exemple pour sous liste `[3, 4, 5]`, idx de colis 3 est donc 0.
- $len$ : le longeur de son sous liste. pour colis 3, son sous liste est `[3, 4, 5]` donc $len = 3$


Par exemple: pour le colis 3, on doit livrer ce colis à l'entreprise A mais on le livre à l'entreprise B, donc on doit choisir le premier formule.
$$
\begin{array}{rcl}
dist_{colis3} & = & dist_{entreprise} + \frac {idx}{len} \\
& = & 1 + \frac {0}{2} \\
& = & 1
\end{array}
$$

pour le colis 7, on doit livrer ce colis à l'entreprise C mais on le livre à l'entreprise A, donc on doit choisir le deuxième formule.
$$
\begin{array}{rcl}
dist_{colis7} & = & dist_{entreprise} + \frac {len - 1 - idx}{len} \\
& = & 2 + \frac {3 - 1 - 1}{3} \\
& = & 2.3333
\end{array}
$$
#### 2.4 Optimisation
Pendant chaque itération, après la sélection par des contraintes, on peut obtenir une population dont 250 individus. Puis on utlise la sélection de tournoi pour choisir 220 individus qui ont plus petit distance de déchargement. Puis on utilise la sélection par Pareto pour conserver 200 individus.

#### 2.5 Ajout du perturbation
Lorsque nombre d'itération peut être divsié par 100, on va augmenté le taux de mutation à 0.01 pour évité le résultat tombé à un optimale locale.

## 3 Autre mis à jours
On a rassembler toutes les paramètres et les mettre à un endroit. Maintenant c'est plus pratique pour customizer certaines paramètres.

<image src="./figs/parameters.png" style="display:block;width:600px;"></image>

De plus, en considérant que le processus d'optimisation est lentement, on a ajouté des codes qui permet de conitnuer le processus d'optimisation. Il faut seulement activer `--continue_training`, puis choisir le npy fichier qui stocke la population ou entrer le dossier où le fichier se situé.

Comme chaque fois, on trouve la sélection des contraintes prend trop de temps notament à la première itération. Pour accélérer ce processus, on peut directement choisir un npy fichier pour sauter le processus à la première itération.
l faut seulement activer `--use_pop_init_npy`, puis choisir le npy fichier.

## 4. Analyse des résultats

<image src="./figs/chart_order_dechargement.png" style="display:block;width:600px;"></image>



<image src="./figs/result-order-dechargement.png" style="display:block;width:600px;"></image>

On peut voir le processus est convergent. Après prèsque 500 itération, le résultat devient stable.

## 5. Schema
<image src="./figs/flow.png" style="display:block;width:350px;"></image>

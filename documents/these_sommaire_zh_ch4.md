# 第4章 methodes de placement propose
本章介绍了为了解决这些问题而提出的placement方法，以及不同algo de separation的不同变体。

## 4.1 Introduction
文献中都是针对特定问题使用特定方法。但是大多数方法不允许集成新的约束或用于特定的集合形状。
<p style="color:white;background-color:green">因此我们提出了一种通用方法来处理placemnet最大化问题</p>
该方法应该既能处理C&P问题又能处理pb d'agencement。并能够考虑所有类型的目标和约束。

我们采用了une methode de placement relaxe以灵活的适用不同的问题。因此 **优化变量就是les variables de positionnement des composants.**
选择这种methodes de placement relaxe的动机是
- 该方法允许在建模问题时集成特定的约束，从而保证满足这些约束。composant对齐或者是一些composant的定位需要满足一些特殊的约束时可以轻的通过methodes relaxe进行管理。

- 即使一些约束条件没有完全满足，该方法也会提出解决方案。在一些过度约束（surcontraint）的问题时，设计者可以轻松地放宽某些约束以便找到一个或多个可行的方案。


- 该方法允许concepteur轻松地与解决方案进行交互并且提出他感兴趣的解决方案。

## 解决pb de placement的方案
以模块化进行阐述，首先是该方法的一般原理，然后该方法的不同版本。

###  4.2.1 原理
pb de placement和 局部最小值问题非常相似。当问题使用真是变量时，搜索空间将会变得非常大。为了探索更有效的搜索空间的方法，将使用全局优化算法。全局优化算法将会随机的进行探索，他讲控制composant集合的variables de positionnement和他们的属性从而提出有效的解决方案。

composant的variables de position是连续的，组建的朝向是离散的（0 90 180 270度）或根据composant的自由度是连续的（180度自由旋转）

methode de placement relaxe的主要困难在于如何满足这些约束。通过使用分离算法（algo de separation）使得解决这种全局优化问题变得可行。该算法的目的是在当地移动这些composant以便尽可能的遵从这些约束。使用全局优化算法是解决methodes de placement的经典方法，很多多进化算法被提出。**但是将进化算法与分离算法的耦合是新的。**

<p style="color:white;background-color:green;font-weight:bold">总而言之，全局优化算法负责高效的探索espace de conception，分离算法负责是全局优化器提出的解决方案尽可能的满足约束</p>

#### 4.2.1.1 几何表示和碰撞检测
在2d和3d中，composant的表示方法有很多种。**几何表示的主要目的是便于几何计算尤其是碰撞检测的运算**。碰撞问题至今都是解决methodes de placemnt的难点之一。碰撞检测算法的速度对于解决pb de placement至关重要。

碰撞检测算法可分为3个级别
- niveau 1: 只检测碰撞
- niveau 2: 量化碰撞
- niveau 3: 使用碰撞信息分离重叠的composant。

目前已经有许多解决方案被提出
对于第一种,最明显的是计算交叉区域的面积（2d）或交叉区域的体积（3d）。然而这种方法计算量太大，通常无法实现。为了克服这一困难，已经有几种替代方案被提出。

<p style="color:white;background-color:green;">例如我们下面介绍的这个profondeur de penetration。</p>

我们的目的是选择适合每个问题的结合并且我们可用于实现分离算法。在2d中这种选择取决于composant的自由度，特别是composant的能否自由旋转。在3d中，当composant具有复杂的几何形状时，我们使用球体composant以有效的检测碰撞。

#### 4.2.1.2 分离算法原理
分离算法的目的是使全局优化器提出的解决方案尽可能的可接受（admissible），从不可接受的解决方案中，该算法通过局部区域的移动来最小化不满足的约束条件。其目标是通过最小化移动以便尽可能的不要干扰初始解决方案。


该方法是全局的当所有composant同时移动以满足约束条件时。
许多文献中也提出了一些启发式的分离算法，但是他们逐个对composant进行移动并不能保证deplacemnt minimum.

<p style="color:black;background-color:pink">我们想要最小化的函数是两个函数的和，第一个表示不遵守每对composant之间的非重叠约束，第二个表示不遵守每个composant的appartenance约束。 如果pb de placement 有m个composant，那么该函数表示为:</p>

$$F(\boldsymbol{v}) = w_{pen} \sum_{i=1}^{m-1} \sum_{j=i+1}^{m} f_{ij}(\boldsymbol{v}) + w_{pro} \sum_{i=1}^{m} g_i(\boldsymbol{v})$$

其中
- $\boldsymbol{v}$是m个composant的 vecteur position. 它包含一个vecteur de translation $\boldsymbol{x}$和一个vecteur d'orientation $\boldsymbol{o}$.
- 系数$w_{pen}$和$w_{pro}$均为positif($  \geq 0$)。用于调节两个函数的影响程度。pen意为穿透（penetration）。pro意为侵入（protrusion）。如无特殊指明那么$w_{pen}=w_{pro}=1$
- $f_{ij}(\boldsymbol{v})$为composant$C_i$和$C_j$不遵守不重合约束的数值，如果两个不重合那么该项为0。
- $g_i(\boldsymbol{v})$表示composant $C_i$在contenant的程度，如果$C_i$完全在contenant内部，那么该项为0.
- $f_{ij}$和$g_i$具体表达形式取决于composant的集合形状以及他们的自由度。

这些函数都是非线性的。

最小化方法：quasi-Newton BFGS.
停止迭代依据：1. 最大迭代次数， 2. norme du gradient 精度 3. norme du gradient阈值 低于此阈值不再足够判断目标值的改善。算法细节详见附件B.2

通过对F函数分析梯度可以避免使用数值微分。函数F及其梯度详见4.3. 图4.9， 4.20， 4.29分别以图形方式表示有分离算法的不同变体完成的工作。

#### 4.2.1.3 多目标遗传算法

选用的是全局优化器是Omni-Optimizer.基于NSGA-II适用于解决单目标和多目标约束问题。

此外，operateurs de selection restreinte在croisement之前选择最优最接近的个体，从而找到多模态的解决方案。 本遗传算法对实数使用使用传统的 operateur de croisement continu(SBX)以及的、mutaion polynomiale。这些遗传算子的功能详见annexe B。对于离散型变量则使用operateurs de croisement a deux points 以及 mutation par inversion de bit。这种变量当朝向是离散时将会被应用。

对于对composant的compacite有较高要求的算子，该遗传算法需要很多次迭代后才能收敛。为了克服这个缺点，我们引入了一个新的算子，该算子能够改变两个compsant之间的距离，这种方法能够避免优化器快速的收敛至局部最优值（我们要的是全局最优值）。和交叉变异算子相同，该算子也有一个给定的概率


#### 4.2.1.4 提出的placemnt算法
算法如图4.1所示， 整体框架与一般的遗传算法非常类似。
![](./figs/algo_placement.png)

初始种群以随机的方式生成，或者设计者可以提供一组解决方案，或者是通过启发式放置得到的解决方案。自评估每个解决方案之前，约束条件都会被判断，如果不满足约束，algo de separation 将会修改解决方案使得解决方案可以接受。如果分离算法返回的函数值为0 那么该解决方案是可以接受的（admissible）

该值用于遗传算法的一个约束
- 如果该值为0， 那么contraintes de placement被满足。
- 相反，如果修改后的解决方案仍然non admissible那么重复此操作，直至评估完全部种群
。如果满足一个停止标准，则算法停止。否则执行selection de tournoi, 交叉变异和mutation以产生新一代种群。
- 停止标准可以使最大迭代书，une stagnation de l'evolution de l'hypervolume 或者是其他用户自定义的标准。


### 4.2.2 pb de placement 2d建模（朝向离散）
在大多数2d问题中，composant的朝向都是离散的，也就是说在用户指定的几个朝向集合中进行选择。
大多数情况下，朝向是$(0^\circ, 90^\circ, 180^\circ, 270^\circ )$中的一种，这些朝向通常有compo的几何形状来决定，但是此处提供的算法中，同样考虑了自由旋转的问题（任意度数）。

所有的composant都是多边形，不是这种形式的也可由多边形近似而成。composant的variable de position是连续的，variables d'orientation是离散的，composant $C_i$的变量被包含在向量$\boldsymbol{v_i}$里，$\boldsymbol{v_i} = (\boldsymbol{x_i}, \theta_i) = (x_i, y_i, \theta_i)$。 其中$\theta_i$从用户给定的朝向集合中选择。


composant通过参考点（如重心）来计算。多边形的位置可以通过旋转和平移的组合来进行计算，计算过程如下
$$\binom{P_{ij,x}^{n}}{P_{ij,y}^{n}} = \boldsymbol{c}_{ij}(\boldsymbol{v}_{i}) = \binom{x_i}{y_i} + \begin{bmatrix}
cos(\theta_i) & -sin(\theta_i) \\
sin(\theta_i) & cos(\theta_i)
\end{bmatrix} \binom{P_{ij,x}}{P_{ij,y}}$$



- $P_{ij,x}$表示的是多边形$P_i$上第j个点的x坐标
- 变化后的坐标表示为$P_{ij,x}^{n}$。

如图4.2所示,我们先讲物体绕着其重心旋转，而后在将物体平移到所需的位置。
![](./figs/transformation_2d.png)


---

<p style="color:white;background-color:green;">正如之前所说计算两个composant之间的重合面积计算量大，另一种方法于是被提出，如非重叠多边形(No-Fit Polygon)，和内贴多边形(Inner-Fit Polygon)。如图4.3所示：</p>

![](./figs/no-fit-poly-inner-fit-poly.png)

$P_1$位置固定，$P_2$位置通过参考点来确定。参考点所在位置是途中的黑线，非重叠多边形$NFP(P_1, P_2)$可以仅仅通过P2的参考点的坐标变知道多边形$P_1$和多边形$P_2$是否重合。如果$P_2$参考点位于NFP以内，那么，这两个多边形一定有重合部分。对于内贴多边形IFP同理。

尽管NFP和IFP的计算很昂贵，并且需要对每个朝向的多边形都进行计算，但是使用此方法有两个优点。
- 可以通过对多边形的点的归属来简单测验collision.复杂度仅为多边形的边数。

- 另一方面， test d'appartenance 可以得到一些对分离算法有用的信息（如profondeur和penetration）从而用于在数值上评估两个composant的重叠程度。

多边形计算使用了minkowski和， 该方法适用于polygone convexe，对于non convexe的需要进行分解，详见 annexe B.4


## 4.2.3 pb de placement 2d建模（朝向连续）
此时NFP和IFP表现不是很好。由于种种原因，我们要更改composant的集合表示。我们朝着圆圈组合的表示方法迈进。由于分离算法收敛的，这些圆圈组合以axe median的方式进行构造。[Att95, TV04].

![](./figs/axe-median.png)


### 4.2.4 pb de placement 3d建模
三维问题建模是对二维问题建模的推广，精确的几何形状不允许快速的碰撞检测，并且不适用与分离算法。于是我们选择使用球形组合以表示3d composant，这样就可以轻松检测composant的交叉点，还可以利用collision来实现重叠composant的分离，球形分解技术详见相关分离算法。

一般情况下，变量是6个，3个用于计算平移，3个用来计算朝向。我们选择z-x-z欧拉角来表示朝向。关于欧拉角的旋转矩阵详见附录B.5


如果所有的composant和contenant都是长方体(parallelepipede rectangle)有必要对composant进行转化，在这种情况下，分离算法详见4.3.1. 普通情况下，算法则是4.3.3.2和4.3.3.3

### 4.2.5 问题初始化
遗传算法的种群初始化直接影响到算法的收敛速度，并且对于解决方案的diversite genotypique起着重要的作用。另外第五章表明，找到一组可行的解决方案可能很困难，为了克服这些缺点，我们建议使用可行的解决方案初始化全局算法的总体。

我们首先想到的应该是，从不同的优化方案中生成一个可行的解决方案以初始化种群。然而这需要先执行所提出的方案，以保存识别的可行解决方案。缺点是这种方案计算量大，切很难识别哪一种是可行的解决方案。

第二种方法是使用专用的方法。在2d中我们提出了一种methode de placement legal,该方法可以生成满足非重叠和appartenance约束的方法，这是一种适用于复杂几何形状composant的启发式方法（<span style="color:violet;">Bottom-Left-Fill</span>）。该方法保证了composant的放置顺序同时满足了placement的约束要求。在表示composant的引入顺序时采用了随机排列（permutation aleatoire）. 朝向也是随机选择。对于要放置的每个composant，他的polygone d'appartenance 将会被计算，他的左下角的点也会被标记。在这一点上将会放置composant，重复该过程直到放置完所有组件或者容器中没有更多的空间。

为了多样化解决方案，启发式放置的方向可以改变，例如可以从haut a droite开始，最后将随机优化算法应用在composant引用顺序（排列）上可以用于处理难以初始化的清醒。

<span style="color:white;background-color:green">第二种方法好呀</span>，所有composant相互接触，因此非常适合处理有forte compacite的问题。然而该方法不能满足特定的放置约束如对其等要求。

图4.5,表示的是Left-Bottom-Fill，从最左边开始然后从最下边开始。
![](./figs/left-bottom-fill.png)

<span style="color:white;background-color:violet">对于3d问题，选择了基于Tiwari等人提出的voxel的Bottom-Left-Fill被用于初始化。</span>


## 4.3 分离算法
分离算法被用于满足放置约束（contraintes de non-chevauchement et contraintes d'appartenance)。一般想法是讲所有的约束放置在一个相同的连续可微分函数中，然后利用algo d'optimisationd deterministe BFGS 用于最小化不符合该约束。根据composant集合表示不同，该算法也会变化。

### 4.3.1 对于长方体的algo de seperation en translation

对于所有组件和容器均为长方体的情况下，专门搞了个特定的分离算法。目前存在许多长方体或矩形的放置方法，但是他们都没有考虑特定的一些约束（如composant对齐）。这里介绍的算法就考虑了这些约束。

我们首先要找出表示不满足这些约束的函数F，从而有效的分离长方体组件集合。

为此开发并比较了不同的函数以便选择最好的一个。两个量是确定这些不满足约束函数的基础
- des intervalles d'intersection de segment
- la profondeur de penetration
这两个量将会首先在1d案例中呈现，然后推广至更高的维度。

然后这些fonction elementaire 将会组合成不同的fonctions test 第四段将会介绍几种test从而决定哪个fonction de minimisation 最为有效。 第五段讲将会考虑rotaion。最后介绍了如何扩展一些新的约束。


#### 4.3.1.1 cas 1d

首先，我们考虑一组可以在轴上平移的m条线段。我们希望他们两两互不重合。

参考点：线段中点，考虑到更高维度的离散旋转，这种选择乍看起来计算量大，事实上是更为明智的算法，理由将在下一节给出。


假设$\boldsymbol{x}$是m条线段的位置坐标所组成的向量，而$\boldsymbol{b}$是各条线段的长度所组成的向量,$\boldsymbol{x} \in \mathbb{R}^m, \boldsymbol{b} \in \mathbb{R}^m$，我们开始计算线段i和线段j互不重合的约束。他们的位置变量分别为$x_i$, $x_j$，如果存在点既属于线段i又属于线段j，则两者相重合。函数$\gamma_{ij}$表示的是线段i和线段j的重合区间宽度。

$\gamma_{ij}(\boldsymbol{x}) = \max \{0, \min\{x_i + \frac{b_i}{2}, x_j + \frac{b_j}{2}\} - \max \{x_i - \frac{b_i}{2}, x_j - \frac{b_j}{2}\}\}$

穿深(profondeur de penetration)$\delta_{ij}$表示的是线段ij，通过平移即可分离两条线段的最短长度。
$\delta_{ij} = \max \{0, \frac{b_i}{2}+ \frac{b_j}{2}-|x_i - x_j|\}$.
这两个函数都是lineaire par morceaux, 如果线段i和线段j完全没有相交， 那么这两个的函数值均为0。这两个函数最大的不同之处在于他们的梯度。如图所示：

![](./figs/gradient_1d.png)
关键就在当一个线段完全处于另一个线段之中，此时$\gamma_{ij}$不再变化，其值为较小的线段的长度，这将会导致优化算法的过早收敛，但是穿深$\delta_{ij}$仍可以变化，因为它是将两线段不重合所移动的最小长度。且穿深不受参考点选择的影响。
梯度数学表达式为：
![](./figs/gradient_eq_1d.png)



实际算法中，我们使用了二分之一线段长来避免每次都除以2.


当composant是线段，长方形或者是长方体是，contraintes d'appartenance可以通过以下两个方面考虑
第一种是基于惩罚函数，第二种是使用能够处理边界变量的优化器。边界可以约束composant保持在容器中，但是对于任意形状的contenant则无能为力。此外长方形或长方体的正交旋转在每次旋转时需要边界变量的更新，所以我们选择使用惩罚函数来管理contraintes d'appartenance.

为了对线段i属于contenant的隶属约束(contraintes d'appartenance)进行建模，我们引入两个新的函数$\bar{\gamma_i}$和$\bar{\delta_i}$作为之前提到的两个函数的补充。

contenant的长度为$c$，其最左边端点位于原点。所有线段有一部分长度位于contenant里面，函数$\bar{\gamma_i}$表示的是线段i位于容器外部分的长度，函数$\bar{\delta_i}$表示的是线段i通过平移可以移动到容器内部的最小长度。

![](./figs/gamma_bar-delta_bar-1d.png)
当线段完全位于contenant内部时，$\bar{\gamma_i}$和$\bar{\delta_i}$均为0，但是当线段完全位于contenant之外时$\bar{\gamma_i}$为常数，而$\bar{\delta_i}$则继续变化。

![](./figs/gradient2_1d.png)
![](./figs/gradient_eq_2.png)

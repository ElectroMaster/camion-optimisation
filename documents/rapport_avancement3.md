## Rapport d'avancement
> Author: CAI
> Date:  mai, 2019

### 1. Introduction
J'ai éssaie plusieur façon pour utiliser compactivité dans l'algorithme.
### 2. Algorithme
#### 2.1 Expression de la compactivité
On utilise le rapport d'espace vide par rapport au plus petit contenant virtuel qui peut contenir tous les composant.

$$r = 1 -  \frac{\sum_{i=1}^{m}V_{i}-V_{intersection}}{V_{c}} $$
ou $v_c$ signifie le plus petit contenant qui peut contenir tous les composants.

- $V_i = \prod_{k = 1}^{d} b_{ik}$
- $V_{intersection} = \sum_{i=1}^{m-1} \sum_{j=i+1}^{m}\Gamma_{ij}$

- longeur d'un côté du contenant virtuel:
$x_{ck} = \max \limits_{i \in \{1,...,m\}} x_{ik}  - \min \limits_{i \in \{1,...,m\}} x_{ik}$ donc on a
$$V_c = \prod_{k = 1}^{d} x_{ck} = \prod_{k = 1}^{d} (\max \limits_{i \in \{1,...,m\}} x_{ik}  - \min \limits_{i \in \{1,...,m\}} x_{ik})$$


donc on a

$$r = 1 - \frac{\sum_{i=1}^{m} \prod_{k = 1}^{d} b_{ik} - \sum_{i=1}^{m-1} \sum_{j=i+1}^{m}\Gamma_{ij}}{\prod_{k = 1}^{d} (\max \limits_{i \in \{1,...,m\}} x_{ik}  - \min \limits_{i \in \{1,...,m\}} x_{ik})}$$

#### 2.2 Choix de l'expression de degré d'adaptation d'un individus $i$.

##### 2.2.1 Premier essai
On utilise seulement les contraintes  non-chevauchement et appartenance, c'est-à-dire des valeurs de la fonctions $F$. D'après la sélection par tournoi. On peut obtenir l'expression:
$$\mathcal{D}_i = \frac{argmax(\{F_1, ..., F_{200}\}) - F_i}{\sum_{k=1}^{200}(argmax(\{F_1, ..., F_{200}\}) - F_k)}$$

![](./figs/result-F.png)


##### 2.2.2 Deuxième éssai
On utilise les valeurs de la fonction $F$ et le rapport d'espace vide $r$. On essaie la production de $F$ et $r$.
Donc on a
$$
\begin{array}{lcl}
T_i & = & F_i \times r_i \\
\mathcal{D}_i & = & \frac{argmax(\{T_1, ..., T_{200}\}) - T_i}{\sum_{k=1}^{200}(argmax(\{T_1, ..., T_{200}\}) - T_k)}
\end{array}
$$
![](./figs/result-FxVide.png)

On trouve que cette solution est plus compacte aprè 1000 itérations.




##### 2.2.3 Troisième éssai



$$\begin{array}{lcl}
\mathcal{D}_{i,part1} & = & \frac{argmax(\{F_1, ..., F_{200}\}) - F_i}{\sum_{k=1}^{200}(argmax(\{F_1, ..., F_{200}\}) - F_k)} \\
\mathcal{D}_{i,part2}  & = & \frac{argmax(\{r_1, ..., r_{200}\}) - r_i}{\sum_{k=1}^{200}(argmax(\{r_1, ..., r_{200}\}) - r_k)} \\
\mathcal{D}_{i} & = & \frac{\mathcal{D}_{i,part1} + \mathcal{D}_{i,part2}}{2}
\end{array}$$
![](./figs/result-F+Vide.png)

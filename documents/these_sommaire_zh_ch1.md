## 大纲
第一章介绍问题分类，经过分析，我们将problem d'agencement 重新命名为problems de placement。并且这个问题游客分为两个子问题：
- les problèmes de découpe et de conditionnement
- les problèmes d’agencement.

第二章介绍了多目标优化问题的解决方案，包括多目标优化理论和多目标优化算法。最后介绍了用于验证结果所需的各种多目标分析工具。

第三章介绍了一种agencement的方法，其中几何composant之间没有直接的互相干扰。该方法被用于船舱的布局示例。我们将之建模为组合优化问题，该方案基于原始的方法以确保符合contraintes。在此基础上我们提出了一种广义的多目标优化方法。

第四章介绍了一种解决pb placement的普适方法。该方法同时使用了algo evolutionnaire 和 algo de separation。前者用于全局搜索，后者用于保证几何约束条件的最大满足。尽管前者和问题无关但喝着泽宇composant的几何形状直接相关。因此我们展示了问题的建模和algo de separation的几个辩题。几个例子可以使人们看到algo de separation的步骤。

第五章则介绍的是我们测试了提出的方法在数个问题上的应用。第一个例子针对特定的某个问题与其他方法进行了比较。接下来是两个介绍多目标agencement的例子。他们可以证明我们提出问题的有点。多种研究用于独立地分析解决方法各个成分的作用。对于变量空间和目标的多样性分析是值得关注的问题。通过第二章提出的评价指标进行不同方法的评价。

## 第一章
### 1.1 简介
Les problèmes d’agencement géométrique et fonctionnel下文称之为problèmes de place-
ment由很多子问题组成，包括les problèmes de découpe et de conditionnement （英文：C&P cutting & pacing pb）et les problèmes d’agencement.（layout pb）


<p style="color:#C71585">optimisation de chargement 属于pb de decoupe et conditionnement ,目标是maximiser le chargement .</p>

### 1.2 pb de placement 定义

#### 1.2.1 基本概念定义
在找出pb de placement的数学定义前，一些基本概念需要定义。
- **composant**: 通常是我们需要放置的具有几何形状的物理物体，可能是constant也可能是variables的，通常来说是不变的。
  - attributs
- **contenant**: 容器，用于盛放composant。体积固定或者可变。体积可变时，通常是求解最小化容器体积，如pb de decoupe。问题中也可能有多个contenant，例如求解最小数量的contenant用于盛放composant。

- **inconnus**:
  - variables de positionnement
  - attributs de composants(部分未知的)

- **contraintes**
  - 至少有composant不互相重叠（non-chevauchement）这一要求。
  - 大多数情况下，composant应该在contenant里面
- **objectifs**：也就是评价标准，通常以数字衡量。涉及到美学的标准时例外。
#### 1.2.2 pb de placement 定义
<p style="color:		#BA55D3">Étant donné un ensemble de composants ainsi qu’un ensemble de contenants, un problème de placement consiste à trouver l’ensemble des variables de positionnement des composants afin de minimiser un ensemble d’objectifs, tout en respectant des contraintes.</p>

### 1.3 pb de placement 特点

#### 1.3.1 technique de placement
- methodes de placement legal
- methodes de placement relaxe
#### 1.3.2 变量表示
变量有几种类型,continues, discretes, binaires或者是permutation(排列)
- 排列型：用长为m的id序列表示摆放顺序，则有m!个可能的解。某论文采用从下至上从左至右的排序。
- 离散型：可以用于定位composant在网格上的放置位置，通常应用于几何形状为长方体或立方体（或由多个立方体组成的区块）的composant的表示。朝向也可以用离散型数据表示
- binaire: 是否属于某容器或某composant内
- 实数型：positionnemnt relatif ou absolu。对于某些相对位置约束条件来说，positionnement relatif更适合，如两个composant的对齐。

#### 1.3.3 placement的目标

trois types:
- individuel: placer ce somposant le plus a droite
- globaux: appliquer sur l'ensemble des composant
- interaction: fait intervenir une relation entre different composants.

####  1.3.4 contraintes
3 types:
- 限制自由度的
- 不同优化参数的组合
- 目标的组合
autres
- 限制位置或朝向的
- 限制composant接近程度的
- 限制domaine的（不太理解）
#### 1.3.5 对不重合或属于不属于的评估
计算量大。对于m个composant需要$\frac{m^2}{2} + \frac{m}{2}$次测试来检测重合。

理想的方法是使用methode de placement legal使得一开始就自动满足互不重合约束。但也得确保所有的组件均可放置。

在不能使用这种方法情况下，要想确保所有的composant在容器内不并且没有一对composant重叠，测试需要根据composant的几何表示选择不同的方式来执行这些测试。
- 对于规则几何(rectangles, cercles, spheres)可以使用formulation analytiques 来评估约束。
- 对于复杂几何形状物体。已经开发出了几种不同的策略，
    - 在2d中注入非重叠多边形的几何工具可用于以较低成本检测组件之间的重叠 。
    - 3d中，通常相似的几何形状容易快速的检测出碰撞，
    voxel(英语： contraction anglophone de volumetric pixels), octree八叉树（英语 contraction angophone de octant tree）容易检测composant之间的碰撞，详见第四章。

### 1.4 pb de decoupe et de conditionnement
formulations verbales et mathématique sont simples mais la combinatoire rend ces pbs difficile a resoudre.

#### 1.4.1 定义
对于每一个C&P pb 其背后的主要思想都是compacite: 即在容器中盛放的composants应尽可能的多，contenant的数量或体积应该尽可能的小。 因此每个解决方案都试图从这些特性出发，这也是和后面的layout pb最主要的区别。

由两种问题组成
- pb de decision:
  contenant能否装下所有的composant
- pb de minimisation / maximisation
  容器内部能够装多少个composant

pb de decision通常帮助解决pb de minimisation / maximisation

#### 1.4.2 C&P pb 分类
**Dyckhoff [Dyc90].**

1. Dimension du problème
  - 1 Problème unidimensionnel
  - 2 Problème bidimensionnel
  - 3 Problème tridimensionnel
  - N Problème multi-dimensionnel avec N > 3
2. Type d’affectation
  - B Tous les contenants et une sélection des composants
  - V Une sélection des contenants et tous les composants
3. Nature des contenants
  - O Un seul contenant
  - I Plusieurs contenants identiques
  - D Plusieurs contenants différents
4. Nature des composants
  - F Quelques composants
  - M De nombreux composants très différents entre eux
  - R De nombreux composants

**Wäscher et al. [WHS07]**
![](./figs/whs07.png)


### 1.4.3 Pb de decoupe
#### 1.4.3.1 pb de decoupe unidimensionnel
找到可包含给定composant集合的最小容器数目

#### 1.4.3.2 pb de decoupe bidimensionnel
与一维的pb de decoupe类似，只不过这次是composant是二维的长方形（高度统一）。

### 1.4.4 Pb de sac a dos
#### 1.4.4.1 Pb de sac a dos unidimensionnel
给定composant集合，每个composant有权重还有价值，目标最大化价值同时保证不超过权重最大值。解决方案是动态变成。

#### 1.4.4.2 Pb de sac a dos multi-dimensionnel
Le problème de sac à dos peut être décliné en 2D et en 3D. 在2d情况下，只考虑平行于坐标系轴的长方形，或者是3d情况下，只考虑平行于坐标轴的长方体的时候，是可以的。
- type chargement de type sac a dos
- type minimiser les dimension de contenant

### 1.4.5 pb de bin packing 2d
找出尺寸一致的最小数量的长方形contenant用于容纳一系列的长方形composant。composant的棱边与contenant的棱边平行，我们称为，rangement orthogonal。

Lodi et al.提出的pb bin packing分类：

- 2BP|OG : les composants sont orientés (O), le découpage guillotine (G) est requis ;
- 2BP|RG : les composants peuvent subir une rotation de 90 ◦ (R) et le découpage guillotine (G)
est requis ;
- 2BP|OF : les composants sont orientés (O) et le découpage est libre (F) ;
- 2BP|RF : les composants peuvent subir une rotation de 90 ◦ (R) et le découpage est libre (F) ;

针对各种问题，许多研究者提出了各种解决方案,以下省略。


### 1.4.6 pb de chargement de palettes
2D pb de chargement de palettes 指的是将些许不同形状的长方形composant防止最小数目的形状大小均相同的长方形palette里。

### 1.4.7 pb de decoupe de formes irregulieres
将一堆2d复杂形状的composant放置与contenant里，使得dimension minimale。目标是尽量减少切割组件产生的损失。该问题主要在纺织业和金属业中遇到。当contenant的形状是长方形是，我们又将其称为pb de decoupe en bande(英文strip packing pb) 又被分类为 2D-ODP (two-dimensional irregular open dimension problem) selon la typologie de Wäscher [WHS07]。此类问题还有多种变体，如当组件都相同时或pb de decoupe periodique.

针对各种问题，许多研究者提出了各种解决方案,以下省略。

### 1.4.8 任意几何形状的3d物体的包装问题（pb de conditionnement）
英文：3d packing，放置尽可能多的3d不规则composant于不规则容器内。

应用，激光快速原型。

针对各种问题，许多研究者提出了各种解决方案，作者认为
Bottom-Left-Back Fill最厉害

## 1.5 pb d'agencement
该问题又被定义为pb de placement. 主要用于寻找composant在contenant内部空间的位置。

![](./figs/pb_agencement.png)

很多C&P解决方法不再适用于此类问题，但是很多C&P问题可以看成是pb d'agencemnt的特殊情况。因此很多解决pb d'agencement的问题对于C&P问题同样适用。

尽管该问题是multi-objectifs的，但大多数方法是将其转换成mono-objectif non contrainte 的。本论文的一大目的就是将此类问题建模城multi-objectifs的。

经典的pb d'agencement涉及到本地布局，大规模芯片集成，工程问题中的3d 布局等。

### 1.5.1 pb d'agencement 2d
#### 1.5.1.1 pb d'agencemnt de locaux
又被称为 pb d'agencement d'ateliers.(英文：facility layout pb， FLP)

在大部分此类问题中，composant主要是和长方形且棱与坐标轴平行。Ces problèmes sont caractérisés par les
interactions entre composants, principalement exprimées comme des minimisations de flux. composant事实上也可以不是长方形，但这样做有优点也有缺点。

#### 1.5.1.2 pb d'integration a grande echelle
大规模集成（英文 very large scale integration, VLSI）放置长方形composant从而最小化一个或数个函数。在此类问题中composant又被称为module.
目标
- la minimisation des longueurs de connexions
- la minimisation de la chaleur dissipée par les composants
- ...

### 1.5.2 pb d'agencement 3d
pb d'agencement 3d主要位于工程类问题中，composant几何形状较为复杂，目标函数公式也较为复杂，因此为了满足目标和约束，计算时间也较长，但在大多数情况下，几何形状的要求并没有特别的严格，可以使用八叉树技术来近似组件的几何形状。

该类问题的解决需要掌握目标函数公式，几何因素影响，以及优化方法。尽管许多研究人员提出了各种解决方案，但都是mono-objectif的。因此本文将提出一个多目标的。

1.6 pb C&P 和 pb d'agencement 比较
![](./figs/comparasion.png)

1.7 建议
对于pb d'agencement,objectifs和contrainte决定了问题的本质。从给定的问题出发
- 根据给定的几何，目标和约束选择放置方案。对于具体的特殊约束问题（如acessibilite）只能考虑methode de placement relaxe
- 选择一组适应问题的优化变量，在建模过程中集成最大数量的约束。
如果问题的紧凑型很重要，则可能难以识别可行的初始的解决方案，使用放置启发法找到一组可行的解决方案是明智的。

组建的稽核表示取决于组建的几何形状和自由度，

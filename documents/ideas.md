# Sujet: optimisation d'agencement pour le chargement de vehicules.
>Author: CAI Pengfei

>Date: 2019/04/09
## 1. Problem d'agencement
**Objets considéré**:
- Contenant: e.g. camion.
    - caractéristiques: dimension fixe.
- Composantes: e.g. colis.
    - caractéristiques: normalement dimension fixe.

**But**:
- Maximiser le chargement
- Faciliter le chargement et dechargement

**Description**:
- Etant donné une liste de colis et un camion, on doit trouver les positionements des colis afin de minimiser des fonctions objectives.

**Cas particuliers**:
- Intersection des colis.
- Dimensions des certains colis sont variables.

## 2. Définition des objets
### variables
- Point siginificatif: $ (x_i, y_i, z_i)$, on le définit pour déterminer la position du colis
- 3d rotation $({\alpha}_i, {\beta}_i, {\gamma}_i)$
- **Donc** pour un objet 3d, les variables nécéssaire sont $ (x_i,y_i,z_i, {\alpha}_i, {\beta}_i, {\gamma}_i)$
### paramètres fixes
- (**Très souvent**) Parallélépipède rectangle: longeur, largeur, hauteur.
- Ball: rayon $r$
- Sphéroïde: longeur d'axe long $a$, longeur d'axe court $b$
- Pyramide: longeur, largeur. point sommet.
- Forme irréguliers: le considérer comme un Parallélépipède rectangle.

## 3. Représentation des objets
- Identification: numéro de colis - ID
    - D'après ID on peut identifier un colis ainsi que les paramètres fixes duquel.
- positionnement: point siginificatif $ (x_i, y_i, z_i)$
- Orientation d'agencement: $({\alpha}_i, {\beta}_i, {\gamma}_i)$

## 4. Contraintes
- Non chevauchement = non superposition
- TODO:

## 5. Fonctions objectives

### Questions utiles
- Est-il possible de placer tous les colis dans un camion
- Combien de camions sont nécessaires
- Combien de ces colis peut-on placer à l’intérieur
de un camion?
- Si les colis sont divisés en lots (client A, B, C), comment minimiser le temps de livraison
- Est-ce qu'il faut considérer les matériaux des colis.
- Les questions au-dessus peut-être ont différents poids de gravité.
### donc
- <p style="color:red">Compacité - comment l'exprimer?</p>
- Coût de livraison, il faut considérer la position du porte et l'ordre de dechargement
- Stablité, il faut considérer les matérieux
### expression mathématique
- Compacité:
    $$ f_{compact}(\boldsymbol x) = f_{compact}(x_1, x_2, ..., x_i, ..., x_n)$$
- Coût de livraison, les colis qu'on veut livrer premièrement doit être proche de la porte. donc si loin alors le coût doit être plus élevé.
    $$ f_{livraison}(\boldsymbol x) = f_{livraison}(x_1, x_2, ..., x_i, ..., x_n)$$
- Stabilité, le plus fragile matériel et le plus bas on le met, on aura la plus haute pénalisation

    $$ f_{stab}(\boldsymbol x) = f_{stab}(x_1, x_2, ..., x_i, ..., x_n)$$
    où $x_i$ signifie un coli de numéro i.
- **En conclusion**, c'est un optimisation multi-objectif.
    $$\min \limits_{\boldsymbol x}f(\boldsymbol x) = \min \limits_{\boldsymbol x} (f_{compact}(\boldsymbol x), f_{livraison}(\boldsymbol x), f_{stab}(\boldsymbol x), ..., f_{?}(\boldsymbol x))$$


## 6. Références
[1] Guillaume Jacquenot. Méthode générique pour l’optimisation d’agencement géométrique et fonctionnel. Mécanique [physics.med-ph]. Ecole Centrale de Nantes (ECN), 2010. Français. <tel-00468463>
